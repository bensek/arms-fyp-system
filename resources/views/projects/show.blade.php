@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">Project #{{$project->id}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('list_projects', $year_id)}}">Projects</a>
                    </li>
                     <li class="breadcrumb-item active">Project #{{$project->id}}
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title pb-2"></h4>
                                <div class="heading-elements row">
                                    @role('supervisor')
                                    @if($project->supervisor_id == Auth::id())
                                        <a href="{{route('edit_project', ['year_id' => $year_id, 'id' => $project->id])}}" class="btn btn-info  btn-min-width box-shadow mr-1 mb-1  btn-sm"><i class="la la-edit font-medium-3"></i>Edit</a>
                                        <a href="#" onclick="deleteProject({{$project->id}})" class="btn  btn-danger  btn-min-width box-shadow mr-1 mb-1  btn-sm"><i class="la la-trash font-medium-3"></i>Delete
                                        </a>
                                    @endif
                                    @endrole
                                    @role('coordinator')
                                        <a href="{{route('edit_project', ['year_id' => $year_id, 'id' => $project->id])}}" class="btn btn-info  btn-min-width box-shadow mr-1 mb-1  btn-sm"><i class="la la-edit font-medium-3"></i>Edit</a>
                                        <a href="#" onclick="deleteProject({{$project->id}})" class="btn  btn-danger  btn-min-width box-shadow mr-1 mb-1  btn-sm"><i class="la la-trash font-medium-3"></i>Delete
                                        </a>
                                    @endrole
                                </div>
                            </div>

                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                	@if(Session::has('message'))
                                	<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
									@endif

                                    <div class="px-2 mb-4">
                                        
        <h1><strong>{{$project->title}}</strong></h1>
        <hr>
        <div class="row">
            <div class="col-3">
                <h5><i class="ft-user" style="font-size: 18px;"></i> {{$project->lecturer}}</h5>
            </div>
            <div class="col-3">
                <h5><i class="ft-calendar" style="font-size: 18px;"></i> {{$project->created_at->isoFormat('dddd Do MMMM YYYY')}}</h5>
            </div>
              <div class="col-6">
                <h5><i class="ft-grid" style="font-size: 18px;"></i> 
                   
                    @php $count = count($project->categories) @endphp
                    @for($i = 0; $i < $count; $i++)
                        {{$project->categories[$i]->name}} 

                        @if($i != $count-1),@endif
                    @endfor
                </h5>
            </div>
            
        </div>


        <p class="card-text text-justify mt-2">{!! $project->description !!}}</p>

         <h5 class="mt-4"><i class="ft-edit-2" style="font-size: 18px;"></i> <strong>Comments </strong>({{count($comments)}})</h5>
        <hr>
        @foreach($comments as $comment)
        <div class="mb-3">
            
            <div class="row">
            <div class="col-6">
                        <h6><em><strong>{{$comment->user->fullnames()}}</strong></em></h6>

            </div>
            <div class="col-6 text-right">
                {{$comment->created_at->isoFormat('ddd, Do MMM YY')}}   
            </div>
            
            </div>
            <div class="row">
                <div class="col-8">
                    <p>{{$comment->text}}</p>

                </div>
                <div class="col-4 text-right">
                    @if($comment->user_id == Auth::id())
                        <a href="{{route('edit_comment', $comment->id)}}">Edit</a> | <a href="{{route('delete_comment', $comment->id)}}">Delete</a>
                    @endif
                </div>
                
            </div>
            

        </div>
        

        @endforeach

        <form method="POST" action="{{route('comment_project')}}">
            @csrf
            <div class="row">
                <div class="col-6">
                    <input type="hidden" name="project_id" value="{{$project->id}}">
                      <h5 class="">Add comment</h5>
                      <fieldset class="form-group">
                        <textarea name="text" class="form-control" rows="5" placeholder="Type..."></textarea>
                    </fieldset>
                    <button class="btn btn-primary">Comment</button>
                </div>
                
            </div>
          
        </form>









                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
    <script >
        function deleteProject(id){
            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this project!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#6610f2',
                cancelButtonColor: '#607D8B',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false
            }).then((result) => {
                if(result.value){
                    $.ajax({
                        url: '/projects/delete/'+id,
                        type: "DELETE",
                        data:{ _token: "{{csrf_token()}}" }
                    }).done(function() {

                        swal({
                            title: "Deleted",
                            text: "Project has been successfully deleted",
                            type: "success",
                            confirmButtonColor: '#6610f2',

                        }).then(function() {
                            location.href = '/dashboard';
                        });
                    });

                }


            });

        }
    </script>

@endsection
