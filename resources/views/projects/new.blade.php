@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('list_projects', $year_id)}}">Projects</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{session('title')}}</h4>
                                    <div class="heading-elements">

                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
@if(Session::has('message'))
                                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        <form class="form needs-validation" method="post" action="{{route('store_project')}}" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="current_year_id" value="{{$year_id}}">
                                            <div class="form-body">
                                                <div class="row">
                                                        <input type="hidden" name="project_id" value="{{$project->id}}">
                                                <div class="form-group col-12">
                                                    <label for="schoolName">Project Title<span class="text-danger">*</span></label>
                                                    <input type="text" id="schoolName" class="form-control" placeholder="Add a title" name="title" required value="{{$project->title}}">
                                                </div>
                                                </div>
                                                <div class="row">
                                                      <div class="form-group col-6">
                                                        <label>Select Academic Year<span class="text-danger">*</span></label>
                                                        <select class="select2 form-control" name="year_id" required="">
                                                            <option value="">Select</option>
                                                            @foreach($years as $year)
                                                            <option value="{{$year->id}}">{{$year->name}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>

                                                    <div class="form-group col-6">
                                                        <label for="schoolName">Submitted By<span class="text-danger"></span></label>
                                                        <input type="text" id="schoolName" class="form-control" placeholder="Add lecturer" name="lecturer" required  value="{{Auth::user()->fullnames()}}">
                                                    </div>
                                                   
                                                </div>
                                                    <div class="row">
                                                        <div class="form-group col-6">
                                                            <label for="category">Categories<span class="text-danger">*</span></label><br>

                                                            <select class="select2 form-control select2-size-sm" id="category" name="category_ids[]" required="" multiple="">
                                                                <option disabled="" value="">Select category</option>
                                                                @foreach($categories as $cat)
                                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-6">
                                                            <label for="schoolName">Attach Image<small class="text-muted"> (Optional)</small></label>
                                                            <input type="file" class="form-control" name="thumbnail">

                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                    <div class="form-group col-12">
                                                    <label for="description">Project Description<span class="text-danger">*</span></label>
                                                    <textarea rows="100" id="description" class="form-control summernote" placeholder="Add description" name="description" required>{{$project->description}}</textarea>
                                                </div>
                                                    </div>

                                                </div>

                                            <div class="form-actions">
                                                <a href="{{route('list_projects', $year_id)}}" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>


                </section>

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $('#category').select2();
        $('#description').summernote({
            height:300
        });
    });
</script>


@endsection
