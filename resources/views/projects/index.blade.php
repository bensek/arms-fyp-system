@extends('layouts.master')

@section('content')


<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Projects
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        @if($current_year == null)
                                            All Projects
                                        @else
                                            {{$current_year->description}} Projects
                                        @endif
                                    </h4>

                                    <div class="heading-elements">
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{route('list_projects',0)}}" class="btn btn-outline-info btn-year text-white btn-md @if($current_year == null) active @endif">All Years</a>
                                            @foreach($years as $year)
                                                    <a href="{{route('list_projects',$year->id)}}" class="btn btn-outline-info btn-year text-white btn-md @if($current_year != null && $current_year->id == $year->id) active @endif">{{$year->name}}</a>
                                            @endforeach
                                        </div>

                                        @if($current_year == null)
                                             <a href="{{route('new_project', 0)}}" class="btn btn-success btn-min-width box-shadow ml-4 white"><i class="la la-plus font-medium-3"></i>New</a>

                                        @else
                                             <a href="{{route('new_project', $current_year->id)}}" class="btn btn-success btn-min-width box-shadow ml-4 white"><i class="la la-plus font-medium-3"></i>New</a>

                                        @endif
                                        

                                      

                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                    	@if(Session::has('message'))
                                    	<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                            <table id="datatable" class="table table-striped table-bordered bootstrap-3 dataTable">
                                            <thead>
                                                <tr>
                                                    <td>TITLE</td>
                                                    <td>LECTURER</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($projects as $p)
                                                <tr>
                                                    <th style="width: 300px">
                                                          @if($current_year == null)
                                             <a href="{{route('show_project', ['id' => $p->id, 'year_id' => 0])}}" class="text-dark">{{$p->title}}</a>

                                        @else
                                             <a href="{{route('show_project', ['id' => $p->id, 'year_id' => $current_year->id])}}" class="text-dark">{{$p->title}}</a>


                                        @endif
                                        
                                                       
                                                    </th>
                                                    <th>{{$p->lecturer}}</th>

                                                </tr>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>

                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </section>
                <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
            	jQuery(document).ready(function(){
					$("#datatable").DataTable({
						columnDefs: [{ orderable: false }],
                        "pageLength": 50
                    });
            	});

            	function deleteProject(id){
            		Swal.fire({
				        title: 'Are you sure?',
				        text: 'You will not be able to recover this project!',
				        type: 'warning',
				        showCancelButton: true,
				        confirmButtonColor: '#6610f2',
				        cancelButtonColor: '#607D8B',
				        confirmButtonText: 'Yes, delete it!',
				        closeOnConfirm: false
				    }).then((result) => {
				    	if(result.value){
				    		$.ajax({
					            url: '/projects/delete/'+id,
					            type: "DELETE",
					            data:{ _token: "{{csrf_token()}}" }
					        }).done(function() {

					            swal({
					                title: "Deleted",
					                text: "Project has been successfully deleted",
					                type: "success",
					                confirmButtonColor: '#6610f2',

					            }).then(function() {
					                location.href = '/projects';
					            });
					        });

				    	}


				    });

            	}

            </script>


@endsection
