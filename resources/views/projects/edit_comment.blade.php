@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('list_projects')}}">Projects</a>
                    </li>
                     <li class="breadcrumb-item active">
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-6">
                        <div class="card">
                           


                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif

                                    <div class="px-2 mb-4">

                                         <form method="POST" action="{{route('update_comment')}}">
            @csrf
            <div class="row">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$comment->id}}">
                      <h5 class="">Edit comment</h5>
                      <fieldset class="form-group">
                        <textarea name="text" class="form-control" rows="5" placeholder="Type...">{{$comment->text}}</textarea>
                    </fieldset>
                    <button class="btn btn-primary">Update</button>
                </div>
                
            </div>
          
        </form>
                                        
    
       






                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
 
@endsection
