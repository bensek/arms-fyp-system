@extends('layouts.master')

@section('content')

<div class="content-header row">
</div>
<div class="content-body">

    <section id="minimal-statistics">
     </section>
                
 <!-- Revenue, Hit Rate & Deals -->
                <div class="row">
                    <!-- <div class="col-lg-12 col-md-12">                        
                        
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Submitted Proposals</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><button type="button" class="btn btn-glow btn-round btn-bg-gradient-x-red-pink">More</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body p-0 pb-0">
                                    <div class="chartist">
                                        <div id="project-stats" class="height-400 areaGradientShadow1"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                  <!--   <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Proposals Statistics</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                   
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="height-300">
                                            <canvas id="simple-doughnut-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div> -->
                </div>
                <!--/ Revenue, Hit Rate & Deals -->
                <!-- Chat and Recent Projects -->
                <!-- <div class="row match-height">
                   
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <h5 class="card-title text-bold-700 my-2">Recent Proposals</h5>
                        <div class="card">
                            <div class="card-content">
                                <div id="recent-projects" class="media-list position-relative">
                                    <div class="table-responsive">
                                        <table class="table table-padded table-xl mb-0" id="recent-project-table">
                                            <thead>
                                                <tr>
                                                    <th class="border-top-0">Project Name</th>
                                                    <th class="border-top-0">Assigned to</th>
                                                    <th class="border-top-0">Deadline</th>
                                                    <th class="border-top-0">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-truncate align-middle">
                                                        <a href="#">X Admin</a>
                                                    </td>
                                                    <td class="text-truncate">
                                                        <ul class="list-unstyled users-list m-0">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="John Doe" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-19.png" alt="Avatar">
                                                            </li>
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Katherine Nichols" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-18.png" alt="Avatar">
                                                            </li>
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Joseph Weaver" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-17.png" alt="Avatar">
                                                            </li>
                                                            <li class="avatar avatar-sm">
                                                                <span class="badge badge-info">+2 more</span>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="text-truncate pb-0">
                                                        <span>15th July, 2018</span>
                                                        <p class="font-small-2 text-muted"> 1 day left</p>
                                                    </td>
                                                    <td>
                                                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                                            <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate align-middle">
                                                        <a href="#">Analytics UI</a>
                                                    </td>
                                                    <td class="text-truncate">
                                                        <ul class="list-unstyled users-list m-0">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="John Doe" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-17.png" alt="Avatar">
                                                            </li>
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Katherine Nichols" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-14.png" alt="Avatar">
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="text-truncate pb-0">
                                                        <span>26th May, 2018</span>
                                                        <p class="font-small-2 text-muted danger"> behind</p>
                                                    </td>
                                                    <td>
                                                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate align-middle">
                                                        <a href="#">Traveltrip</a>
                                                    </td>
                                                    <td class="text-truncate">
                                                        <ul class="list-unstyled users-list m-0">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="John Doe" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-19.png" alt="Avatar">
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="text-truncate pb-0">
                                                        <span>23rd May, 2018</span>
                                                        <p class="font-small-2 text-muted"> in 11 Days</p>
                                                    </td>
                                                    <td>
                                                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                                            <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate align-middle">
                                                        <a href="#">Apex Angular</a>
                                                    </td>
                                                    <td class="text-truncate">
                                                        <ul class="list-unstyled users-list m-0">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="John Doe" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-19.png" alt="Avatar">
                                                            </li>
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Katherine Nichols" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-18.png" alt="Avatar">
                                                            </li>
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Joseph Weaver" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-17.png" alt="Avatar">
                                                            </li>
                                                            <li class="avatar avatar-sm">
                                                                <span class="badge badge-info">+1 more</span>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="text-truncate pb-0">
                                                        <span>13th May, 2018</span>
                                                        <p class="font-small-2 text-muted"> 1 month</p>
                                                    </td>
                                                    <td>
                                                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                                            <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate align-middle">
                                                        <a href="#">Chameleon Admin</a>
                                                    </td>
                                                    <td class="text-truncate">
                                                        <ul class="list-unstyled users-list m-0">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="John Doe" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-11.png" alt="Avatar">
                                                            </li>
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Katherine Nichols" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="/images/portrait/small/avatar-s-12.png" alt="Avatar">
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="text-truncate pb-0">
                                                        <span>18th July, 2018</span>
                                                        <p class="font-small-2 text-muted danger"> behind</p>
                                                    </td>
                                                    <td>
                                                        <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                                            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!--/ Products sell and New Orders -->
</div>


@endsection