@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{session('title')}}</h4>
                                    <div class="heading-elements">
                                      
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        @if(Session::has('message'))
                                        <div class="alert {{ Session::get('alert-class', 'alert-danger') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        
                                        <form class="form needs-validation" method="post" action="{{route('reset_password_new')}}">
                                            @csrf
                                            <div class="form-body">

                                                <div class="form-group row col-6">
                                                    <label for="schoolName">Email Address<span class="text-danger">*</span></label>
                                                    <input type="text" id="schoolName" class="form-control" placeholder="Enter user's email address" name="email" required value="">
                                                </div>

                                                 <div class="form-group row col-6">
                                                    <label for="schoolName">New Password<span class="text-danger">*</span></label>
                                                    <input type="text" id="schoolName" class="form-control" placeholder="Enter the new password and keep it safely" name="password" required value="{{$random}}">
                                                    <small>Copy this password somewhere before resetting. </small>
                                                </div>
                                                
                                            </div>

                                            <div class="form-actions">
                                               
                                                <button type="submit" class="btn btn-primary">
                                                    Reset
                                                </button>
                                            </div>
                                        </form>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){});
</script>


@endsection