<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="An online system for CEDAT students that allows them to submit their final year project proposals, presentations and reports">
    <meta name="author" content="ThemeSelect">
    <title>{{session('title')}} | {{ config('app.name')}}</title>
    <link rel="apple-touch-icon" href="/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- <link rel="stylesheet" type="text/css" href="/vendors/css/ui/perfect-scrollbar.min.css"> -->


    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/chat-application.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/dashboard-analytics.css">
    <link rel="stylesheet" type="text/css" href="/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugins/forms/wizard.css">

    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <!-- END: Custom CSS-->

    <!-- Other libraries -->
    <link rel="stylesheet" type="text/css" href="/css/plugins/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/extensions/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/tables/extensions/buttons.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/advanced-cards.css">
    <link rel="stylesheet" href="/js/summernote/summernote-bs4.css"/>



    <!-- jQuery Library -->
    <script src="/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>

    <style type="text/css">

          #invalid-feedback{
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #fa626b;
         }

        .header-navbar {
            background-color: #23A74C;
        }
        html body .content .content-wrapper .content-wrapper-before {
            background-color: transparent;
        }
        .main-menu .main-menu-content {
            background-color: #23A74C !important;
        }
         .navbar-header{
            background-color: #23A74C;
        }

        body.vertical-layout.vertical-menu.menu-expanded .main-menu.menu-light .navigation > li > a > i {
             background: #23A74C;
        }
        .main-menu.menu-light .navigation li a {
            color: #fff;
        }
        body.vertical-layout.vertical-menu.menu-expanded .main-menu {
            background-color: #23A74C;
        }
        body.vertical-layout.vertical-menu.menu-expanded .main-menu {
             background-color: #23A74C;
        }
        html body .content .content-wrapper .content-header-title {
            color: #202020;
        }
        .breadcrumb .breadcrumb-item a {
            color: #202020;
        }
        .breadcrumb .breadcrumb-item.active {
            color: #202020;
        }
        .breadcrumb-item + .breadcrumb-item::before {
            color: #202020;
        }

        .btn-year {
            color: #28afd0 !important;
        }
        .btn-year.active {
            color: #fff !important;
        }

    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    @php 
    $year = App\AcademicYear::where('is_current',1)->first(); 
    @endphp

    @php 
    $programmes = App\Programme::where('department_id', Auth::user()->department_id)->get();
    @endphp

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse show" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                       <!--  <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li> -->
                         <li class="nav-item"><h3 class="nav-link" style="margin-top:14px;">
                         <strong>FOURTH YEAR PROJECT SYSTEM</strong></h3></li>

                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="nav-item pt-1 mr-1" style="margin-top: 16px">
                            <h5 class="text-white">{{Auth::user()->fullnames()}}</h5>
                      @if(Auth::user()->verified == 0)      
                     <span class="badge badge-danger">Account requires approval</span>
                     @endif

                    </li>
                     <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <span class="avatar avatar-online"><img src="/images/placeholder.png" alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right">
                                    <a class="dropdown-item" href="{{route('account')}}"><i class="ft-user"></i> Account</a> 
                                    <div class="dropdown-divider"></div>


                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                           <i class="ft-power"></i>{{ __(' Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </div>
                            </div>
                        </li>   

                    </ul>
                </div>

            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="/images/backgrounds/02.jpg">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item text-center">
                    <a class="navbar-brand text-center">
                    <img style="height: 80px;" alt="logo" src="/images/mak_logo.png" />
                    <!-- <h5 class="brand-text">FOURTH YEAR PROJECTS SYSTEM</h5> -->
                    </a>
                </li>

                <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
            </ul>

        </div>
        <!-- <div class="navigation-background"></div> -->
        <div class="main-menu-content" style="padding-top: 30px;">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                @role('coordinator')
                <li class=" nav-item"><a href="/dashboard"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="ft-layers"></i><span class="menu-title" data-i18n="">Proposals</span></a>
                    <ul class="menu-content">
                        @foreach($programmes as $programme)
                            <li><a class="menu-item" href="{{route('list_proposals', ['programme_id'=>$programme->id, 'year_id'=> $year->id])}}">{{$programme->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                @endrole
                 @role('supervisor')
                <li class=" nav-item"><a href="{{route('supervisor_dashboard')}}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="{{ route('supervisor_proposals')}}"><i class="ft-layers"></i><span class="menu-title" data-i18n="">Proposals</span></a>
                </li>
                @endrole
                @role('coordinator|supervisor')
                <li class="nav-item"><a href="#"><i class="ft-monitor"></i><span class="menu-title" data-i18n="">Presentations</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('list_presentations', ['type'=>'mid_term','year_id' => $year->id])}}">Mid-Term Presentations</a>
                        </li>
                        <li><a class="menu-item" href="{{route('list_presentations', ['type'=>'final','year_id' => $year->id])}}">Final Presentations</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item"><a href="#"><i class="ft-file-text"></i><span class="menu-title" data-i18n="">Reports</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('list_reports', ['type'=> 'project_proposal', 'year_id' => $year->id])}}">Project Proposal Reports</a>
                        </li>
                        <li><a class="menu-item" href="{{route('list_reports', ['type'=> 'project_report', 'year_id' => $year->id])}}">Final Project Reports</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item"><a href="#"><i class="ft-calendar"></i><span class="menu-title" data-i18n="">Panels</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('list_panels', 'mid_term')}}">Mid-Term</a>
                        </li>
                        <li><a class="menu-item" href="{{route('list_panels', 'final')}}">Final Projects</a>
                        </li>
                      <!--   <li><a class="menu-item" href="{{route('list_panels', 'supplementary')}}">Supplementary</a>
                        </li> -->
                    </ul>
                </li>
                 <li class=" nav-item"><a href="#"><i class="ft-check-square"></i><span class="menu-title" data-i18n="">Grades</span></a>
                    <ul class="menu-content">
                        @role('coordinator|supervisor')
                        @foreach($programmes as $programme)
                            <li><a class="menu-item" href="{{route('list_grades', ['id'=>$programme->id, 'year_id'=>$year->id])}}">{{$programme->code}}</a>
                            </li>
                        @endforeach
                        @endrole
                        @role('admin')
                        @foreach(App\Programme::all() as $programme)
                            <li><a class="menu-item" href="{{route('list_grades', ['id'=>$programme->id, 'year_id'=>$year->id])}}">{{$programme->code}}</a>
                            </li>
                        @endforeach
                        @endrole
                    </ul>
                </li>
                @endrole
                @role('coordinator|supervisor')
                  <li class=" nav-item"><a href="{{route('list_projects', ['year_id' => $year->id])}}"><i class="ft-cpu"></i><span class="menu-title" data-i18n="">Projects List</span></a>

                </li>
                @endrole
                @role('coordinator')
                <li class=" nav-item"><a href="{{route('list_announcements')}}"><i class="ft-alert-octagon"></i><span class="menu-title" data-i18n="">Announcements</span></a>
                </li>
                 <li class=" nav-item"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="">Students</span></a>
                    <ul class="menu-content">
                        @foreach($programmes as $programme)
                            <li><a class="menu-item" href="{{route('list_students', ['id'=>$programme->id, 'name'=>$programme->name, 'year_id' => $year->id])}}">{{$programme->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
               
                <li class="nav-item"><a href="#"><i class="ft-user-plus"></i><span class="menu-title" data-i18n="">Supervisors</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('list_supervisors')}}">List of Supervisors</a>
                        </li>
                        <li><a class="menu-item" href="{{route('list_approvals')}}">Approve Accounts</a>
                        </li>
                    </ul>
                </li>

                 <li class="nav-item"><a href="#"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('list_academic_years')}}">Academic Years</a>
                        </li>
                        <li><a class="menu-item" href="{{route('list_categories')}}">Categories</a>
                        </li>
                        <li><a class="menu-item" href="{{route('reset_password')}}">Reset passwords</a>
                        </li>
                    </ul>
                </li>
                @endrole
                @role('admin')
                <li class=" nav-item"><a href="{{route('admin_dashboard')}}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
                </li>
                    <li class=" nav-item"><a href="{{ route('list_coordinators')}}"><i class="ft-grid"></i><span class="menu-title" data-i18n="">Coordinators</span></a>
                </li>
                 </li>
                <li class=" nav-item"><a href="{{ route('list_logs')}}"><i class="ft-list"></i><span class="menu-title" data-i18n="">Grade Logs</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="{{route('list_programmes')}}">Programmes</a>
                        </li>
                        <li><a class="menu-item" href="{{route('list_departments')}}">Departments</a>
                        </li>
                        <li class="@if(Request::is('schools/')) active @endif"><a class="menu-item" href="{{route('list_schools')}}">Schools</a>
                        </li>
                    </ul>
                </li>
                @endrole
                @role('supervisor|coordinator')
                 </li>
                    <li class=" nav-item"><a href="{{ route('index.manual')}}" target="_blank"><i class="ft-info"></i><span class="menu-title" data-i18n="">Help Center</span></a>
                </li>
                @endrole

            </ul>

        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            @yield('content')


        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2020 &copy; Copyright ARMS Project</span>

        </div>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- <script src="/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
    <script src="/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
    --> <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/js/core/app-menu.js" type="text/javascript"></script>
    <script src="/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- <script src="/js/scripts/pages/dashboard-analytics.js" type="text/javascript"></script> -->
    <!-- END: Page JS-->

    <script src="/js/scripts/extensions/toastr.js" type="text/javascript"></script>
    <script src="/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script src="/vendors/js/extensions/sweetalert2.all.js" type="text/javascript"></script>
    <script src="/vendors/js/tables/jquery.dataTables.min.js"></script>
    <script src="/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="/vendors/js/forms/select/select2.full.min.js"></script>
    <!-- <script src="/js/scripts/cards/card-advanced.js" type="text/javascript"></script> -->
    <!-- <script src="/js/scripts/charts/chartjs/pie-doughnut/pie-simple.js" type="text/javascript"></script>
    <script src="/js/scripts/charts/chartjs/pie-doughnut/doughnut-simple.js" type="text/javascript"></script> -->
    <script type="text/javascript" src="/vendors/js/tables/buttons.flash.min.js"></script>
    <script type="text/javascript" src="/vendors/js/tables/jszip.min.js"></script>
    <script type="text/javascript" src="/vendors/js/tables/pdfmake.min.js"></script>
    <script type="text/javascript" src="/vendors/js/tables/vfs_fonts.js"></script>
    <script type="text/javascript" src="/vendors/js/tables/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/vendors/js/tables/buttons.print.min.js"></script>
    <script src="/js/scripts/forms/custom-file-input.js"></script>
    <script src="/js/summernote//summernote-bs4.min.js"></script>
    <script src="/js/summernote/summernote-data.js"></script>

    <script src="/vendors/js/ui/jquery.sticky.js"></script>
    <script src="/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
    <script src="/js/scripts/forms/form-repeater.js"></script>


</body>
<!-- END: Body-->

</html>
