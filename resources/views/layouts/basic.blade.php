<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>{{session('title')}} | {{ config('app.name')}}</title>
    <link rel="apple-touch-icon" href="/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/vendors/css/ui/perfect-scrollbar.min.css">


    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/chat-application.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/dashboard-analytics.css">
    <!-- END: Page CSS-->


    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/login-register.css">
    <!-- BEGIN: Custom CSS-->

    <!-- END: Custom CSS-->

    <!-- Other libraries -->
    <link rel="stylesheet" type="text/css" href="/css/plugins/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/extensions/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/forms/selects/select2.min.css">

    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <!-- jQuery Library -->
    <script src="/js/core/libraries/jquery.min.js" type="text/javascript"></script>
     <script src="/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>

     <style type="text/css">
         #login-bg{
            background: url('/images/login.jpg'); 
            height: 100%;  
            background-position: center;
            background-repeat: no-repeat; 
            background-size: cover;
         }

         #logo-small{
            height: 50px;
         }

         #invalid-feedback{
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #fa626b;
         }
     </style>


</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 1-column  bg-full-screen-image blank-page blank-page" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">
    <!-- BEGIN: Content-->
   
            @yield('content')

             <!-- BEGIN: Footer-->
 
    <!-- END: Footer-->

    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/js/core/app-menu.js" type="text/javascript"></script>
    <script src="/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->


    <script src="/js/scripts/extensions/toastr.js" type="text/javascript"></script>
    <script src="/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script src="/vendors/js/extensions/sweetalert2.all.js" type="text/javascript"></script>
    <script src="/vendors/js/tables/jquery.dataTables.min.js"></script>
    <script src="/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <script src="/js/scripts/forms/form-login-register.js" type="text/javascript"></script>




</body>
<!-- END: Body-->

</html>