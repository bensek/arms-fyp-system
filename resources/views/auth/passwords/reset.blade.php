@extends('layouts.basic')

@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body" id="login-bg">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                                <div class="card-header border-0">
                                    <!-- <div class="text-center mb-1">
                                        <img src="/images/logo/logo.png" alt="branding logo" id="logo-small">
                                    </div> -->
                                    <div class="font-large-1  text-center text-success">
                                        Reset Password
                                    </div>
                                </div>
                                <div class="card-content">

                                    <div class="card-body">
                                        <form method="POST" action="{{ route('password.update') }}">
                                         @csrf
                                          <input type="hidden" name="token" value="{{ $token }}">
                                          <div class="col-12">
                                            <label>Email Address</label>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                                 @error('email')
                                                    <span id="invalid-feedback">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </fieldset>
                                        </div>
                                         <div class="col-12">
                                            <label>New Password</label>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                 <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                                  @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </fieldset>
                                        </div>
                                         <div class="col-12">
                                            <label>Confirm Password</label>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                                  
                                            </fieldset>
                                        </div>

                                        <div class="form-group text-center">
                                            <button type="submit" class="btn round btn-block btn-success col-12 mt-2">Reset Password</button>
                                        </div>


                                        </form>
                                    </div>
                        

                                
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection