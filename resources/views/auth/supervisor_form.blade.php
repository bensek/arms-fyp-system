@extends('layouts.basic')

@section('content')
 <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body" id="login-bg">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-6 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <!-- <div class="text-center mb-1">
                                        <img src="/images/logo/logo.png" alt="branding logo" id="logo-small">
                                    </div> -->
                                    <div class="font-large-1  text-center text-primary">
                                        Supervisor Registration
                                    </div>
                                </div>
                                <div class="card-content">

                                    <div class="card-body">
                                        <form class="form-horizontal" action="/register_supervisor" method= "post" novalidate>
                                            @csrf
                                        	<input type="hidden" name="role" value="4">
                                        	<div class="row">
                                                <div class="col-4">
                                                    <label>First Name<span class="text-danger">*</span></label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Andrew"  autocomplete="off" name="first_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('first_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Last Name<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Mugisha"  name="last_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('last_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Other Name</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" placeholder="e.g. Walter" autocomplete="off" name="other_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>

                                                    </fieldset>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>Title<span class="text-danger">*</span></label>                                               
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" placeholder="e.g Dr, Prof, Mr" name="title">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                        @error('title')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                    
                                                </div>
                                                <div class="col-4">
                                                    <label>Department<span class="text-danger">*</span></label> 
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                         <select class="select2 form-control" id="department" name="department">
                                                            <option disabled="" selected="" value="">Select Department</option>
                                                            @foreach($departments as $department)
                                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                                            @endforeach
                                                            
                                                        </select>
                                                        <div class="form-control-position">
                                                            <i class="ft-wind"></i>
                                                        </div>
                                                        @error('department')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Office Location<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" placeholder="e.g Room 304 Old building" name="office">
                                                        <div class="form-control-position">
                                                            <i class="ft-navigation"></i>
                                                        </div>
                                                          @error('office')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                     <label>Email Address<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="email" class="form-control" placeholder="e.g. andrew@gmail.com"  name="email">
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>  
                                                </div>
                                                <div class="col-4">
                                                    <label>Password<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" placeholder="Must be atleast 8 characters"  autocomplete="off" name="password">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                        @error('password')
                                                            <span id="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Confirm Password<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" placeholder="Re-enter your password"  name="password_confirmation">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn round btn-block btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Register</button>
                                            </div>

                                        </form>
                                    </div>
                                    <p class="card-subtitle text-muted text-center font-small-3 mx-2 my-1">
                                        <span>Already a member ?
                                            <a href="/login" class="card-link">Login</a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection