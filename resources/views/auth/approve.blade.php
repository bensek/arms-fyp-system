@extends('layouts.basic')

@section('content')
    <!-- BEGIN: Content-->

<div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body" id="login-bg">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3 m-0">
                                <div class="card-header border-0">
                                    <div class="text-center mb-1">
                                        <img src="/images/account_declined.png" alt="branding logo">
                                    </div>
                                    <div class="font-large-1  text-center" style="color:#E82A39">
                                        Your Account Is Not Yet Approved
                                    </div>
                                </div>
                                <div class="card-body text-center pt-0">
                                    <p>To access the system you must be have your account approved by the Admin. For any inquiries please contact <strong>Seremba Edward</strong> via Email: <a href="mailto:serembae@gmail.com"> serembae@gmail.com</a>
                                    </p>
                                   <!--  <div class="mt-2">
                                        <i class="la la-unlock-alt spinner font-large-2 danger"></i>
                                    </div> -->
                                </div>
                                <div class="text-center pt-2 pb-2">
                                   <a class="btn btn-success" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                   {{ __(' Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>




    <!-- END: Content-->

@endsection