@extends('layouts.basic')

@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body" id="login-bg">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                            
                                <div class="card-content">


                                    <div class="card-body">
                                        <div class="text-center mb-0">
                                        <img src="/images/mak_logo.png" style="height: 100px">
                                    </div>
                                    <!-- <div class="font-large-1  text-center">
                                        Makerere University
                                    </div> -->
                                    <div class="font-medium-3  text-center mt-0 mx-4">
                                       <b> Final Year Projects Submission and Review System</b>
                                    </div>
                                    @if(Session::has('message'))
                                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
                                    @endif
                                    <h5 class="text-success my-1 text-center">LOGIN</h5>


                                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                         @csrf
                                         <div>
                                             <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control" id="email" placeholder="Email" required name="email">
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                            </fieldset>
                                            @error('email')
                                                <span id="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                             
                                         </div>
                                         <div>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control" id="password" placeholder="Password" required name="password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                                 @error('password')
                                                    <span id="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </fieldset>
                                           
                                        </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12 text-center text-sm-left">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label class="form-check-label" for="remember">
                                                            {{ __('Remember Me') }}
                                                        </label>
                                                    </div>
                                                </div>
                                               <!--  <div class="col-md-6 col-12 float-sm-left text-center text-sm-right"><a href="{{ route('password.request') }}" class="card-link text-dark">Forgot Password?</a></div> -->
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn round btn-block btn-success col-12 mr-1 mb-1">Login</button>
                                            </div>

                                        </form>
                                    </div>

                                    <!-- <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-1 my-1"><span>Don't Have An Account?</span></p> -->
                                     <div class="text-center">
                                    <h6 class="overline-title overline-title-sap"><span>REGISTER AS A</span></h6>
                                </div>
                                    <div class="text-center mb-2 mt-2">
                                        <a href="/register/student" class="btn btn-outline-danger round btn-min-width mr-1 mb-1 ">STUDENT</a>
                                        <a href="/register/supervisor" class="btn btn-outline-danger round btn-min-width mr-1 mb-1">SUPERVISOR</a>
                                    
                                    </div>

                                
                                    
                              

                                
                                </div>

                                
                            </div>


                        </div>

                    </div>

                </section>


              
        </div>
    </footer>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection