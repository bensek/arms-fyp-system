@extends('layouts.basic')

@section('content')
  <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body" id="login-bg">
            <section class="flexbox-container">
            <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                    <div class="card-header border-0">
                        <!-- <div class="text-center mb-1">
                            <img src="/images/logo/logo.png" alt="branding logo" id="logo-small">
                        </div> -->
                        <div class="font-large-1  text-center text-primary">
                            Verify Your Email Address
                        </div>
                    </div>
                    <div class="card-content">

                        <div class="card-body">

                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif

                        {{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }},
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                        </form>
                        <div class="text-center mt-2">
                            <a class="btn btn-danger round" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                         
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
</div>
</div>
@endsection
