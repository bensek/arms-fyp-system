@extends('layouts.basic')

@section('content')
 <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body" id="login-bg">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-6 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <!-- <div class="text-center mb-1">
                                        <img src="/images/logo/logo.png" alt="branding logo" id="logo-small">
                                    </div> -->
                                    <div class="font-large-1  text-center text-success">
                                        Student Registration
                                    </div>
{{--                                    <div class="font-medium-1  text-center text-primary">--}}
{{--                                        For Academic Year {{$year->name}}--}}
{{--                                    </div>--}}
                                </div>
                                <div class="card-content">

                                    <div class="card-body">
                                        <form class="form-horizontal" method="POST" action="/register_student">
                                            @csrf
                                        	<input type="hidden" name="role" value="3">
                                            <div class="row">
                                                <div class="col-6">
                                                    <label>First Name<span class="text-danger">*</span></label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Andrew"  autocomplete="off" name="first_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('first_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-6">
                                                    <label>Last Name<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Mugisha"  name="last_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('last_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <label>Other Name</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Walter" autocomplete="off" name="other_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>

                                                    </fieldset>
                                                </div>

                                            <div class="col-6">
                                                    <label>Programme<span class="text-danger">*</span></label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                         <select class="select2 form-control" id="programme" name="programme">
                                                                <option disabled="" selected="" value="">Select Programme</option>
                                                                @foreach($programmes as $programme)
                                                                <option value="{{$programme->id}}">{{$programme->name}}</option>
                                                                @endforeach

                                                            </select>
                                                        <div class="form-control-position">
                                                            <i class="ft-wind"></i>
                                                        </div>
                                                         @error('programme')
                                                                    <span id="invalid-feedback">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-6">
                                                    <label>Academic Year<span class="text-danger">*</span></label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <select class="select2 form-control" name="academic_year">
                                                            <option disabled="" selected="" value="">Select</option>
                                                            @foreach($years as $year)
                                                                <option value="{{$year->id}}">{{$year->name}}</option>
                                                            @endforeach

                                                        </select>
                                                        <div class="form-control-position">
                                                            <i class="ft-wind"></i>
                                                        </div>
                                                        @error('academic_year')
                                                        <span id="invalid-feedback">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>

                                                <div class="col-6">
                                                    <label>Registration Number<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. 17/U/1453/PS"  autocomplete="off" name="registration_number">
                                                        <div class="form-control-position">
                                                            <i class="ft-file"></i>
                                                        </div>
                                                        @error('registration_number')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                            </div>
                                                <div class="row">
                                                <div class="col-6">
                                                    <label>Student Number<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. 217002343"  name="student_number">
                                                        <div class="form-control-position">
                                                            <i class="ft-file"></i>
                                                        </div>
                                                        @error('student_number')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                        		<div class="col-6">
                                                     <label>Email Address<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="email" class="form-control" id="user-email" placeholder="e.g. andrew@gmail.com"  name="email">
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                        		</div>
                                                </div>
                                            <div class="row">
                                        		<div class="col-6">
                                                    <label>Password<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" id="user-name" placeholder="Must be atleast 8 characters"  autocomplete="off" name="password">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                        @error('password')
                                                            <span id="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                        		</div>
                                                <div class="col-6">
                                                    <label>Confirm Password<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" id="user-name" placeholder="Re-enter your password"  name="password_confirmation">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                        	</div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn round btn-block btn-success col-12 mr-1 mb-1 mt-2">Register</button>
                                            </div>

                                        </form>
                                    </div>
                                    <p class="card-subtitle text-muted text-center font-small-3 mx-1 my-1">
                                        <span>Already a member ?
                                            <a href="/login" class="card-link text-success">Login</a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
