@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">
    {{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                  @role('coordinator')
                    <ol class="breadcrumb">

                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('list_supervisors')}}">
                       Supervisors
                    </a>
                    </li>
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                </ol>
                @endrole
                @role('admin')
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin_dashboard')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('list_coordinators')}}">
                       Coordinators
                    </a>
                    </li>
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>

                    </ol>
                @endrole
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                @role('coordinator')
                                    Create A Supervisor's Account
                                @else
                                    Create A Coordinator's Account
                                @endrole
                                </h4>
                               <!--  <div class="heading-elements">
                                   <a href="/supervisors/new" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-plus font-medium-3"></i>New</a>
                                </div> -->
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif

                                    <div class="card-body">
                                        <form class="form-horizontal" action="{{route('store_supervisor')}}" method= "post" novalidate>
                                            @csrf
                                            <input type="hidden" name="role" value="4">
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>First Name<span class="text-danger">*</span></label>

                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Andrew"  autocomplete="off" name="first_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('first_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Last Name<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="e.g. Mugisha"  name="last_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                         @error('last_name')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Other Name</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" placeholder="e.g. Walter" autocomplete="off" name="other_name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>

                                                    </fieldset>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <label>Title<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" placeholder="e.g Dr, Prof, Mr" name="title">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                        @error('title')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>

                                                </div>
                                                <div class="col-4">
                                                    <label>Department<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                         <select class="select2 form-control" id="department" name="department">
                                                            <option disabled="" selected="" value="">Select Department</option>
                                                            @foreach($departments as $department)
                                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                                            @endforeach

                                                        </select>
                                                        <div class="form-control-position">
                                                            <i class="ft-wind"></i>
                                                        </div>
                                                        @error('department')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Office Location<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" placeholder="e.g Room 304 Old building" name="office" required="">
                                                        <div class="form-control-position">
                                                            <i class="ft-navigation"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                     <label>Email Address<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="email" class="form-control" placeholder="e.g. andrew@gmail.com"  name="email">
                                                        <div class="form-control-position">
                                                            <i class="ft-mail"></i>
                                                        </div>
                                                        @error('email')
                                                            <span id="invalid-feedback">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Password<span class="text-danger">*</span></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" placeholder="Must be atleast 8 characters"  autocomplete="off" name="password">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                        @error('password')
                                                            <span id="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </fieldset>
                                                </div>
                                                <div class="col-4">
                                                    <label>Confirm Password<span class="text-danger">*</span></label>
                                                     <fieldset class="form-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" placeholder="Re-enter your password"  name="password_confirmation">
                                                        <div class="form-control-position">
                                                            <i class="ft-lock"></i>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                          <!--   @role('admin')
                                            <div class="row">
                                                <div class="custom-control custom-checkbox ml-2">
                                                    <input type="checkbox" name="is_coordinator" class="form-control custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">
                                                        Is A Project Coordinator
                                                    </label>
                                                </div>

                                            </div>
                                            @endrole -->
                                             <div class="form-actions">
                                                <a href="{{route('list_supervisors')}}" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>

                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

@endsection
