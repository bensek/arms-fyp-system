@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    @role('coordinator')
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                    @else
                    <li class="breadcrumb-item"><a href="{{route('admin_dashboard')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                    @endrole
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Approve User Accounts</h4>
                               
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                	@if(Session::has('message'))
                                	<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
									@endif

                                    <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                        	<div class="row">
                                        		<div class="col-sm-12">

                                        	<table id="datatable" class="table table-bordered">
                                            <thead>
                                            	<tr>
                                                	<td>#</td>
                                                	<td>NAME</td>
                                                    <td>EMAIL</td>
                                                    <td>DEPARTMENT</td>
                                                    <td>ACTIONS</td>
                                            	</tr>
                                            </thead>
                                            <tbody>
                                            	@php $counter = 1 @endphp
                                                @foreach($users as $user)
                                                    <tr>
                                                        <th>{{$counter++}}</th>
                                                        <th>
                                                            @if($user->hasRole('supervisor'))
    {{$user->staff_bio->title}} {{$user->lname}} {{$user->fname}} {{$user->other_name}}  
                                                            @else

                    {{$user->lname}} {{$user->fname}} {{$user->other_name}}  
                                                            @endif



                                                           
                                                        </th>
                                                        
                                                        <th>{{$user->email}}</th>
                                                        <th>{{$user->department->name}}</th>
                                                        <th>

                                                            <a href="{{route('approve_supervisor',$user->id)}}" class="btn btn-icon btn-info btn-sm">Approve <i class="ft-check"></i></a>
                                                          


                                                        </th>

                                                    </tr>
                                                    @endforeach

                                    		</tbody>

                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
            	jQuery(document).ready(function(){
					$("#datatable").DataTable({
                      
					});
            	});
            </script>


@endsection
