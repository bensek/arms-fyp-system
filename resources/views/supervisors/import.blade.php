@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    @role('coordinator')
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="/supervisors">Supervisors</a>
                    </li>
                    <li class="breadcrumb-item active">Import
                    </li>
                    @else
                    <li class="breadcrumb-item"><a href="{{route('admin_dashboard')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Coordinators
                    </li>
                    @endrole
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Import CSV and Excel lists of Supervisors</h4>
                               
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif
    

        <form action="{{ route('import_supervisors') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <fieldset class="form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" required="">
                                                <label class="custom-file-label" for="inputGroupFile01">Choose File</label>
                                            </div>
                                        </fieldset>
           
            <button class="btn btn-primary my-2 " type="submit">Import data</button>
        </form>
</div>
</div>
</div>
</div>
</div>
</section>
</div>

@endsection