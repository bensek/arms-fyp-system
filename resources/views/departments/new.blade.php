@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/admin/departments">Departments</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{session('title')}}</h4>
                                    <div class="heading-elements">
                                      
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form needs-validation" method="post" action="/admin/departments">
                                            @csrf
                                            <div class="form-body">
                                            <input type="hidden" name="id" value="{{$department->id}}">    
                                            <div class="col-6">                        
                                                
                                                <div class="form-group">
                                                    <label>School<span class="text-danger">*</span></label>
                                                    <select class="select2 form-control" id="school" name="school_id" required="" >
                                                        <option disabled="" selected="" value="">Select school</option>
                                                        @foreach($schools as $school)
                                                            <option value="{{$school->id}}" {{$school->id == $department->school_id ? 'selected' : ''}}>{{$school->name}}</option>
                                                        @endforeach
                                                        
                                                    </select>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label for="deptName">Department Name<span class="text-danger">*</span></label>
                                                    <input type="text" id="deptName" class="form-control" placeholder="Enter the department name" name="name" required value="{{$department->name}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="hod">Head of Department</label>
                                                    <input type="text" id="hod" class="form-control" placeholder="Enter the head of deparment (Optional)" name="head_of_department" value="{{$department->head_of_department}}">
                                                </div>
                                            </div>
                                                
                                            </div>

                                            <div class="form-actions">
                                                <a href="/admin/departments" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $('#school').select2();
    });
</script>


@endsection