@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('list_panels', $panel->type)}}">Panels</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-8">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{session('title')}}</h4>
                                    <div class="heading-elements">

                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">

                                        <form class="form needs-validation" method="post" action="{{route('update_panels')}}" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="panel_id" value="{{$panel->id}}">
                                            <div class="form-body">
                                                <div class="row">
                                                      
                                                <div class="form-group col-12">
                                                    <label for="name">Panel Name<span class="text-danger">*</span></label>
                                                    <input type="text" id="name" class="form-control" placeholder="Add a name" name="name" required value="{{$panel->name}}">
                                                </div>
                                                <div class="col-12">
                                                       <fieldset class="form-group position-relative">
                                                                            <label for="type">Type of Presentation</label>
                                            <select class="form-control" id="type" required="" name="type">
                                                <option selected="">Select</option>

                                                <option value="mid_term" @if($panel->type == 'mid_term') selected @endif> Mid-term Oral Presentations</option>
                                                <option value="final"  @if($panel->type == 'final') selected @endif>Final Oral Presentations</option>
                                                <option value="supplementary"  @if($panel->type == 'supplementary') selected @endif>Supplementary Presentations</option>
                                            </select>
                                        </fieldset>
                                                    
                                                </div>
                                                        <div class="form-group col-12">
                                                            <label for="supervisors">Add Supervisors<span class="text-danger">*</span></label><br>

                                                            <select class="select2 form-control select2-size-sm" id="supervisors" name="supervisors[]" required="" multiple="">
                                                                <option disabled="" value="">Select</option>
                                                                @foreach($supervisors as $s)
                                                                    @if(in_array($s->id, $lecturers))
                                                                    <option value="{{$s->id}}" selected="">{{$s->fullnames()}}</option>
                                                                    @else
                                                                    <option value="{{$s->id}}">{{$s->fullnames()}}</option>
                                                                    @endif

                                                                @endforeach
                                                            </select>
                                                        </div>

                                                     

                                                    </div>
                                                  
                                                </div>

                                            <div class="form-actions">
                                                <a href="{{route('list_panels', $panel->type)}}" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Update
                                                </button>
                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>


                </section>

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $('#supervisors').select2();
        $('#description').summernote({
            height:300
        });
    });
</script>


@endsection
