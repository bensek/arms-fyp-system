@extends('layouts.master')

@section('content')


<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Panels
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">List of panels</h4>
                                    <div class="heading-elements">
                                        @role('coordinator')
                                       <a href="{{route('new_panels', $type)}}" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-plus font-medium-3"></i>New</a>
                                       @endrole
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                    	@if(Session::has('message'))
                                    	<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                            <table id="datatable" class="table table-striped table-bordered bootstrap-3 dataTable">
                                            <thead>
                                                <tr>
                                                    <td>PANEL</td>
                                                    <td>LECTURERS</td>
                                                    <td>LOCATION</td>
                                                    <td>ACTIONS</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($panels as $panel)
                                                <tr>
                                                    <th>{{$panel->name}}</th>
                                                    <th>
                                                        <ul>
                                                            @foreach($panel->lecturers as $s)
                                                                <li>{{$s->fullnames()}}
                                                                    @if($panel->chair_id == $s->id)
                                                                     (Chair)
                                                                    @endif
                                                                </li>

                                                            @endforeach
                                                            
                                                        </ul>
                                                    </th>
                                                    <th>{{$panel->location}}</th>
                                                    <th>
                                                        @role('coordinator')
                                                        <a href="{{route('show_panels', [$type, $panel->id])}}" class="btn btn-icon btn-success btn-sm"><i class="ft-eye"></i> View</a>
                                                        <a href="{{route('edit_panels', [$type, $panel->id])}}" class="btn btn-icon btn-info btn-sm"><i class="ft-edit"></i></a>
                                                            <a href="#" onclick="deletePanel({{$panel->id}})" class="btn btn-icon btn-danger btn-sm"><i class="ft-trash-2"></i>
                                                            </a>
                                                            @else
                                                             <a href="{{route('show_panels', [$type, $panel->id])}}" class="btn btn-icon btn-success btn-sm"><i class="ft-eye"></i> View</a>
                                                             @endif


                                                    </th>
                                                </tr>

                                                @endforeach
                                              
                                            </tbody>

                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>

                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </section>
                <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
            	jQuery(document).ready(function(){
					$("#datatable").DataTable({
						columnDefs: [{ orderable: false }],
                        "pageLength": 50,
                         
                    });
            	});

            	function deletePanel(id){
            		Swal.fire({
				        title: 'Are you sure?',
				        text: 'You will not be able to recover this panel!',
				        type: 'warning',
				        showCancelButton: true,
				        confirmButtonColor: '#6610f2',
				        cancelButtonColor: '#607D8B',
				        confirmButtonText: 'Yes, delete it!',
				        closeOnConfirm: false
				    }).then((result) => {
				    	if(result.value){
				    		$.ajax({
					            url: '/panels/delete/'+id,
					            type: "DELETE",
					            data:{ _token: "{{csrf_token()}}" }
					        }).done(function() {

					            swal({
					                title: "Deleted",
					                text: "Panel has been successfully deleted",
					                type: "success",
					                confirmButtonColor: '#6610f2',

					            }).then(function() {
					                location.href = '/panels/'+ "{{$type}}";
					            });
					        });

				    	}


				    });

            	}

            </script>


@endsection
