@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('list_panels', $panel->type)}}">Panels</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                 <div class="card-header">
                                    <h4 class="card-title">Timetable</h4>
                                    <div class="heading-elements">
                                        @role('coordinator')
<a href="{{route('allocate_panels', $panel->id)}}" class="btn btn-success btn-sm">
                                                    <i class="la la-plus"></i> Add Students
                                                </a>
                                                @endrole
                                    </div>

                               
                                </div>

                                <div class="card-content">

                                    <div class="card-body">
                                          @if(Session::has('message'))
                                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
                                        @endif
                                        <div class="table-responsive">
                                            <table id="datatable" class="table table-striped table-bordered bootstrap-3 dataTable">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Time</th>
                                                    <th>Student Names</th>
                                                    <th>Student No</th>
                                                    <th>Title</th>
                                                    <th>Programme</th>
                                                    <th>Main & Co-Supervisor</th>
                                                    <!-- <th>Slides</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $counter = 1 @endphp
                                                @foreach($students as $std)
                                                <tr>
                                                    <td>{{$counter++}}</td>
                                                    <td>{{$std->time}}</td>
                                    <td>
                                        {{$std->presentation1_id}}
                                    @if($std->proposal->is_individual_project)
                                    <span>{{$std->proposal->name1}}</span><br>
                                    @else
                                    <span>{{$std->proposal->name1}}  <br>{{$std->proposal->name2}}</span><br>
                                    @endif
                                    </td>
                                                    <td>
                                    @if($std->proposal->is_individual_project)
                                    <span>{{$std->proposal->reg_no1}}</span><br>
                                    @else
                                    <span>{{$std->proposal->reg_no1}} <br>{{$std->proposal->reg_no2}}</span><br>
                                    @endif</td>
                                                    <td>{{$std->proposal->title}}</td>
                                                    <td>{{$std->proposal->programme->code}}</td>
                                                    <td>
                                                         @if($std->proposal->cosupervisor_name == null)
                                    <span>{{$std->proposal->supervisor_name}}</span><br>
                                    @else
                                    <span>{{$std->proposal->supervisor_name}}  <br>{{$std->proposal->cosupervisor_name}}</span><br>
                                    @endif
                                                        
                                                    </td>
                                                   <!--  <td>
                                                    <form action="{{route('download')}}" method="GET">
                                                        @csrf

                                        @if($panel->type=='mid_term')
                                            <input type="hidden" name="path" value='{{$std->presentation1}}'>
                                        @elseif($panel->type=='final')
                                            <input type="hidden" name="path" value='{{$std->presentation2->document}}'>
                                        @else 
                                           <input type="hidden" name="path" value='{{$std->presentation1->document}}'>
                                        @endif
                                                       
                                                        <input type="submit" class="btn btn-info btn-sm" value="Download">
                                                    </form>
                                                    </td> -->
                                                </tr>
                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                        
                                     

                                    </div>
                                </div>
                            </div>
                        </div>


                </section>

</div>

 <script type="text/javascript">
        jQuery(document).ready(function(){
                    $("#datatable").DataTable({
                        columnDefs: [{ orderable: false, targets: [6] }],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf'
                        ]
                    });
                });
    </script>



@endsection
