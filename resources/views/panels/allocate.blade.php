@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('list_panels', $panel->type)}}">Panels</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">

      <section id="configuration">
                    <div class="row">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$panel->name}} [
                                        @if($panel->type=='mid_term')Mid-Term Presentations 
                                        @elseif($panel->type=='final')Final Presentations
                                        @else Supplementary Presentations @endif
                                        ]

                                    </h4>
                                    <div class="heading-elements">

                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">

                                        <form class="form needs-validation" method="post" action="{{route('allocate_students')}}" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="panel_id" value="{{$panel->id}}">
                                            <div class="form-body">
                                                <div class="row">
                                                <div class="form-group col-12">
                                                    <label for="name">Select Time<span class="text-danger">*</span></label>
                                                   <select class="form-control" name="time" required="">
                                                    <option value="">Select</option>
                                                    @foreach($time as $t)
                                                    <option value="{{$t}}">{{$t}}</option>
                                                    @endforeach

                                                       
                                                   </select>
                                                </div>
                                                      
                                               
                                                <div class="form-group col-12">
                                                    <label for="students">Students<span class="text-danger">*</span></label><br>

                                                    <select class="select2 form-control select2-size-sm" id="students" name="proposal_id" required="">
                                                        <option  value="">Search</option>
                                                        @foreach($proposals as $proposal)
                                                            <option value="{{$proposal->id}}">
                                    @if($proposal->is_individual_project)
                                    <span class="text-primary">{{$proposal->name1}}</span><br>
                                    @else
                                    <span class="text-primary">{{$proposal->name1}} - {{$proposal->name2}}</span><br>
                                    @endif 
                                    <!-- ({{$proposal->title}})  -->

                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                              
                                                <div class="form-group col-12">
                                                    <label for="student_nos">Student Nos<span class="text-danger">*</span></label>
                                                    <input type="text" id="student_nos" class="form-control" name="student_nos" required>
                                                </div>
                                                  <div class="form-group col-12">
                                                    <label for="title">Project Title<span class="text-danger">*</span></label>
                                                    <input type="text" id="title" class="form-control" name="title" required>
                                                </div>
                                                 <div class="form-group col-12">
                                                    <label for="supervisors">Main Supervisor & Co-Supervisor<span class="text-danger">*</span></label>
                                                    <input type="text" id="supervisors" class="form-control" name="supervisors" required>
                                                </div>

                                                     

                                                    </div>
                                                  
                                                </div>

                                            <div class="form-actions">
                                                <a href="{{route('list_panels', $panel->type)}}" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>
                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>


                </section>

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $('#students').select2();
        $('#description').summernote({
            height:300
        });

          $("select#students").change(function(){
            var proposal_id = $(this).children("option:selected").val();
            console.log("Proposal Id -> " + proposal_id);

            $.ajax({
                url:"/get_proposal/"+proposal_id,
                success:function(data){
                    console.log(data)
                    if(data.name2 == null){
                         $("#student_nos").val(data.name1)
                     }else{
                         $("#student_nos").val(data.reg_no1 + ' - ' + data.reg_no2)
                     }
                    $("#title").val(data.title)
                    if(data.cosupervisor_name == null){
                        $("#supervisors").val(data.supervisor_name)

                    }else{
                        $("#supervisors").val(data.supervisor_name + ' - ' + data.cosupervisor_name)

                    }
                }
            });


        });
    });
</script>


@endsection
