@extends('student_portal.basic')

@section('content')
                <!-- Revenue, Hit Rate & Deals -->
                <div class="row">
                    <div class="col-lg-8 col-md-12">

                        <div class="">
                            @if($proposal)

                            <div class="card">

                                    <div class="card-header bg-black">
                                        <h4 class="white">My Proposal</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li>
                                                    <a class="btn btn-sm btn-white danger box-shadow-1 round btn-min-width">{{$proposal->status->name}} <i class="ft-activity pl-1"></i></a>
                                                </li>
                                                <li></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content  bg-black">
                                        <div class="card-body">
                                            <div class="mx-2">

                                                <div class="">
                                                    <a href="{{route('proposal_details', ['id'=>$proposal->id])}}">
                                                    <h4 class="text-uppercase text-white"><strong>{{$proposal->title}}</strong></h4>
                                                    </a>
                                                    <hr>
                                                    <small class="text-bold-600 mt-2 text-white"> Submitted On:
                                                        <span class="text-white">{{$proposal->dateTime}}</span>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </div>



                        @else

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body pt-0 pb-1 bg-black">
                                    <div class="text-center text-primary p-3">
                                        <h1 class="text-white"><strong>No Proposal</strong></h1>
                                    <h6 class=" mt-1 text-white">You have not yet submitted any proposal. Click the button below to fill the form.</h6>
                                    <a href="{{route('submit_proposal', 0)}}" class="btn btn-outline-danger round mt-1">Submit</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endif

                        </div>

                         @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-class', 'alert-success') }} alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
                @endif
                
                        <div class="card">

                            <div class="card-header p-1">
                                <h4 class=" pt-1 text-success"><strong>Browse Projects</strong></h4>

                            </div>
                             <div class="card-content collapse show px-2">
                                    @foreach($categories as $cat)
                                    <a href="{{route('category_projects', $cat->id)}}">
                                    <div class="badge border-black black badge-border badge-category">{{$cat->name}}</div>
                                    </a>
                                 
                                    @endforeach
                               
                             </div>

                             <h4 class="mt-4 px-2 text-success"><strong>Latest Projects</strong></h4>

                             <div class="row p-2">
                                
                               
                                    @foreach($projects as $project)
                                    <div class="col-sm-12 col-md-12">
                                        <h4>{{$project->title}}</h4>
                                        <p><em>By {{$project->lecturer}}</em></p>
                                        <p class="text-justify">{!! $project->description !!}</p>
                                        <a href="{{route('display_project', $project->id)}}" class="mr-1 mb-1 text-success">Read More</a>
                                        <hr>

                                    </div>         
                                    @endforeach
                                
                                                                        
                                </div>

                             
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">



                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                             <h4 class="text-success"><strong>Notice Board</strong></h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a href="{{route('get_announcements')}}" class="btn btn-glow btn-round btn-bg-gradient-x-red-pink btn-md">More</a></li>
                                    </ul>
                                </div>
                            </div>
                                <div class="card-content">

                                    <ul class="list-group list-group-flush">
                                        @if(count($announcements) > 0)
                                        @foreach($announcements as $an)
                                        <li class="list-group-item">
                                            <a href="{{ route('display_announcement', ['id'=>$an->id]) }}">
                                            <span class="font-medium-1 text-muted">{{$an->title}} </span> <br>
                                            <small class="text-primary"><em>{{$an->date}}</em></small>

                                        </a>


                                        </li>
                                        @endforeach
                                            @else
                                                <div class="text-center" style="height: 200px"> <span class="mt-4">No announcements available</span></div>
                                            @endif

                                    </ul>
                                </div>
                            </div>


                        </div>



                    </div>
                    </div>

                <script type="text/javascript">
                    jQuery(document).ready(function(){
                        $("#datatable").DataTable({
                            columnDefs: [{ orderable: false }],

                        });
                    });
                </script>
@endsection
