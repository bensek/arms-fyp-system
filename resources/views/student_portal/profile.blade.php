@extends('student_portal.basic')

@section('content')

<div id="user-profile">
                   
                    <div class="row">
                        <div class="col-xl-3 col-lg-5 col-md-12">
                            <div class="card">
                                <div class="card-header pb-0">
                                    <div class="card-title-wrap bar-primary">
                                        <div class="card-title text-primary">Student Information</div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body p-0 pt-0 pb-1">
                                        <ul>
                                            <li>
                                                <strong>First Name:</strong>
                                                {{$user->fname}}
                                            </li>
                                            <li>
                                                <strong>Last Name:</strong>
                                                {{$user->lname}}
                                            </li>
                                            <li>
                                                <strong>Other Names:</strong>
                                                {{$user->other_name}}
                                            </li>
                                            <li>
                                                <strong>Email:</strong>
                                                {{$user->email}}
                                            </li>
                                           
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="card">
                                <div class="card-header pb-0">
                                    <div class="card-title-wrap bar-primary">
                                        <div class="card-title text-primary">Bio Data</div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body p-0 pt-0 pb-1">
                                        <ul>
                                           
                                            <li>
                                                <strong>Student Number:</strong>
                                                {{$user->bio_data->student_no}}
                                            </li>
                                            <li>
                                                <strong>Registration Number:</strong>
                                                {{$user->bio_data->registration_no}}
                                            </li>
                                            <li>
                                                <strong>Programme:</strong>
                                                {{$user->programme->name}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div>
                                <a href="{{route('edit_student_profile')}}" class="btn btn-success btn-min-width box-shadow-2 mr-1 mb-1 block text-white"><i class="ft-edit"></i> Edit Profile</a>
                            </div>
                            
                        </div>
                        <div class="col-xl-9 col-lg-7 col-md-12">
                            <!--Project Timeline div starts-->
                            <div id="timeline">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title-wrap bar-primary">
                                            <div class="card-title text-primary">Recent Proposals</div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                         @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-class', 'alert-success') }} alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
                @endif
                                        <div class="card-block">
                                            <div class="timeline">

                                            	@foreach($proposals as $proposal)
                                            	<div class="px-2 mb-4">
                                                <a href="{{route('proposal_details', ['id'=>$proposal->id])}}"><h3><strong>{{$proposal->title}}</strong></h3></a>
                                    			

                                                <hr>
                                                <h6 class="text-bold-600 mt-2"> Status
                                                    <span class="badge badge-pill badge-{{$proposal->status->class}} text-white">{{$proposal->status->name}}</span>
                                                </h6>
                                                <h6 class="text-bold-600 mt-2"> Submitted On:
                                                    <span class="text-danger">{{$proposal->dateTime}}</span>
                                                </h6>
                                                  <h6 class="text-bold-600 mt-2"> Project Partner:
                                                    <span class="">{{$proposal->name2}}</span>
                                                </h6>
                                                <h6 class="text-bold-600 mt-2"> Supervisors:
                                                    <span class="">@if($proposal->supervisor_name != "") 1. {{$proposal->superviser_name}}  @else N/A @endif @if($proposal->cosupervisor_name != ""), 2. {{$proposal->cosupervisor_name}} @endif</span>
                                                </h6>
                                                <h6 class="text-bold-600 mt-2">Problem Statement
                                                </h6>
                                                <p class="text-justify">{{$proposal->problem_statement}}</p>
                                                <a href="{{route('proposal_details', ['id'=>$proposal->id])}}">See details</a>


                                            </div>
                                                @endforeach

                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Project Timeline div ends-->
                        </div>
                    </div>
                </div>
@endsection