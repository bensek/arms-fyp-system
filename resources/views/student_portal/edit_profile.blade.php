@extends('student_portal.basic')

@section('content')
<div class="row justify-content-center">
<div class="col-xl-8 col-lg-8 col-md-8">
<div class="card">
        <div class="card-header">
            <h4 class="card-title text-primary" id="basic-layout-form">Edit Profile Information</h4>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('update_student_profile')}}">
                    @csrf
                    <input type="hidden" name="role" value="3">
                    <div class="row">
                        <div class="col-6">
                            <label>First Name</label>

                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-name" value="{{$user->fname}}"  autocomplete="off" name="first_name">
                                <div class="form-control-position">
                                    <i class="ft-user"></i>
                                </div>
                                 @error('first_name')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                        <div class="col-6">
                            <label>Last Name</label>
                             <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-name" value="{{$user->lname}}"  name="last_name">
                                <div class="form-control-position">
                                    <i class="ft-user"></i>
                                </div>
                                 @error('last_name')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                       
                        
                    </div>

                    <div class="row">
                         <div class="col-6">
                            <label>Other Name</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-name" value="{{$user->other_name}}" autocomplete="off" name="other_name">
                                <div class="form-control-position">
                                    <i class="ft-user"></i>
                                </div>

                            </fieldset>
                        </div>
                        <div class="col-6">
                             <label>Email Address</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="email" class="form-control" id="user-email" value="{{$user->email}}" name="email">
                                <div class="form-control-position">
                                    <i class="ft-mail"></i>
                                </div>
                                @error('email')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>  
                        </div>

                        
                    </div>
                    <div class="row">
                     
                        <div class="col-6">
                            <label>Registration Number</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-name" value="{{$user->bio_data->registration_no}}" autocomplete="off" name="registration_number">
                                <div class="form-control-position">
                                    <i class="ft-file"></i>
                                </div>
                                @error('registration_number')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                        <div class="col-6">
                            <label>Student Number</label>
                             <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-name" value="{{$user->bio_data->student_no}}"  name="student_number">
                                <div class="form-control-position">
                                    <i class="ft-file"></i>
                                </div>
                                @error('student_number')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                    </div>
                    <div class="form-actions text-center">
                        <a href="{{route('get_profile')}}" class="btn btn-danger mr-1 white">
                            <i class="ft-x"></i> Cancel
                        </a>
                        <button type="submit" class="btn btn-success">
                            <i class="la la-check"></i> Update
                        </button>
                    </div>
                    
                </form>
                                                
               
            </div>
        </div>
    </div>
</div>

<div class="col-xl-4 col-lg-4 col-md-4">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-primary" id="basic-layout-form">Change Password</h4>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                @if($errors->any())
               
                <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                     <h6 class="text-white">{{$errors->first()}}</h6>
                </div>
                @endif
                <form method="POST" action="{{ route('change_password') }}">
                @csrf
                <div class="col-12">
                                            <label>Old Password</label>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                 <input id="password" type="password" class="form-control" name="oldpassword" required autocomplete="new-password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                                
                                            </fieldset>
                                        </div>
                <div class="col-12">
                                            <label>New Password</label>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                 <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                            </fieldset>
                                        </div>
                                         <div class="col-12">
                                            <label>Confirm Password</label>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                                  
                                            </fieldset>
                                        </div>

                                         <div class="form-actions text-center">
                        
                        <button type="submit" class="btn btn-success btn-block">
                            <i class="la la-check"></i> Change Password
                        </button>
                    </div>
                </form>
                
            </div>
        </div>

    </div>

</div>
</div>


@endsection