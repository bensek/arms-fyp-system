@extends('student_portal.basic')

@section('content')
<div class="row justify-content-center">
    <div class="col-10">
        <div class="card">
    <div class="card-body">
        <h1><strong>{{$project->title}}</strong></h1>
        <hr>
        <div class="row">
            <div class="col-3">
                <h5><i class="ft-user" style="font-size: 18px;"></i> {{$project->lecturer}}</h5>
            </div>
            <div class="col-3">
                <h5><i class="ft-calendar" style="font-size: 18px;"></i> {{$project->created_at->isoFormat('dddd Do MMMM YYYY')}}</h5>
            </div>
              <div class="col-6">
                <h5><i class="ft-grid" style="font-size: 18px;"></i> 
                   
                    @php $count = count($project->categories) @endphp
                    @for($i = 0; $i < $count; $i++)
                        {{$project->categories[$i]->name}} 

                        @if($i != $count-1),@endif
                    @endfor
                </h5>
            </div>
            
        </div>

      
        <p class="card-text text-justify mt-2">{!! $project->description !!}}</p>

         <h5 class="mt-4"><i class="ft-edit-2" style="font-size: 18px;"></i> <strong>Comments </strong>({{count($comments)}})</h5>
        <hr>
        @foreach($comments as $comment)
        <div class="mb-3">
            
            <div class="row">
            <div class="col-6">
                        <h6><em><strong>{{$comment->user->fullnames()}}</strong></em></h6>

            </div>
            <div class="col-6 text-right">
                {{$comment->created_at->isoFormat('ddd, Do MMM YY')}}   
            </div>
            
            </div>
            <div class="row">
                <div class="col-8">
                    <p>{{$comment->text}}</p>

                </div>
                <div class="col-4 text-right">
                    @if($comment->user_id == Auth::id())
                        <a href="{{route('edit_comment', $comment->id)}}">Edit</a> | <a href="{{route('delete_comment', $comment->id)}}">Delete</a>
                    @endif
                </div>
                
            </div>
            

        

        </div>
        

        @endforeach

        <form method="POST" action="{{route('comment_project')}}">
            @csrf
            <div class="row">
                <div class="col-6">
                    <input type="hidden" name="project_id" value="{{$project->id}}">
                      <h5 class="">Add comment</h5>
                      <fieldset class="form-group">
                        <textarea name="text" class="form-control" rows="3" placeholder="Type..."></textarea>
                    </fieldset>
                    <button class="btn btn-primary">Comment</button>
                </div>
                
            </div>
          
        </form>

    </div>
</div>
    </div>

</div>

@endsection
