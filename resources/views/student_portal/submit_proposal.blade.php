@extends('student_portal.basic')

@section('content')
<div class="row justify-content-center">
<div class="card col-10">
        <div class="card-header">
            <h2 class="text-primary" id="basic-layout-form">Submission of ECE Fourth Year Proposals</h2>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                <div class="card-text">
                    <p>Project ideas should be submitted by two students. Submissions of a project idea by one or by more than two students will only be considered if accompanied by recommendation from the proposed supervisor of the project idea. Proposals should not exceed 500KB in size.
                    </p>
                    <p>
					Please note that this is the first step of the Proposal Submission Process. After completing this form, you need to email a copy of your proposal to the Project Coordinators as guided.
                    </p>
                </div>
                <form class="form" action="{{route('store_proposal')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="project_id" value="{{$project->id}}">
                    <div class="form-body">
                        <input type="hidden" name="id" value="{{$proposal->id}}" >
                        <input type="hidden" name="name2" id="name2">

                    	 <div class="form-group">
                    	 	<h4 class="form-section text-primary">
                            <i class="ft-book primary"></i> Programme</h4>
                            <label for="companyName">Identify Your Study Program</label><br><br>
                            @foreach($programmes as $p)

                                <div class="form-check form-group">
                                    <input class="form-check-input" type="radio" name="programme_id" id="inlineRadio{{$p->id}}" value="{{$p->id}}" {{ ($std_programme_id == $p->id) ? "checked" : ""}}>
                                    <label class="form-check-label" for="inlineRadio{{$p->id}}">{{$p->name}}</label>
                                </div>
                            @endforeach

                        </div>
                         <div class="form-group">
                            <label>Select Academic Year</label>
                            <select class="select2 form-control col-6" name="year_id" required="">
                                <option value="">Select</option>
                                @foreach($years as $year)
                                <option value="{{$year->id}}" @if($year->is_current == 1) selected @endif>{{$year->name}}</option>
                                @endforeach

                            </select>
                        </div>

                      	<h4 class="form-section text-primary">
                        <i class="ft-user primary"></i> Student One Details</h4>
                        <div class="form-group">
                            <label for="companyName">Student Full Names</label>
                            <input type="text" class="form-control" name="name1" value="{{$user->fullnames()}}">
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="companyinput5">Registration Number</label>
                                    <input type="text" id="reg_no1" class="form-control" name="reg_no1" value="{{$user->bio_data->registration_no}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="companyinput1">Email</label>
                                    <input type="email" id="email1" class="form-control" name="email1" value="{{$user->email}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-check mb-2">
                            <input class="form-check-input" type="checkbox" value="" id="checkbox" name="is_individual_project">
                            <label class="form-check-label" for="checkbox">
                                Check this box if you are one student working on this project
                            </label>
                        </div>

                        <!-- Start of student 2 container -->
                        <div id="student2-container">
                        <h4 class="form-section text-primary">
                        <i class="ft-user primary"></i> Student Two Details</h4>
                        <div class="form-group">
                            <label for="companyName">Student Full Names</label>
                             <select class="select2 form-control" id="student2" name="std_id2">
                                <option disabled="" selected="" value="">Select</option>
                                @foreach($students as $student)
                                @if(Auth()->id() != $student->id)
                                    <option value="{{$student->id}}">{{$student->fullnames()}}</option>
                                @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="companyinput5">Registration Number</label>
                                    <input type="text" id="reg_no2" class="form-control" name="reg_no2">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="companyinput1">Email</label>
                                    <input type="email" id="email2" class="form-control" name="email2">
                                </div>
                            </div>
                        </div>
                        </div>
                        <!-- End of student 2 container -->

                        <h4 class="form-section text-primary">
                            <i class="ft-flag primary"></i> Project Details</h4>
                        <div class="form-group">
                            <label >Project Title</label>
                            <input type="text" class="form-control" name="title" required="" value="{{$project->title}}">
                        </div>

                        <div class="form-group">
                            <label>Problem Statement<span class="text-muted"> (Not to exceed 300 words)</span></label>
                            <textarea rows="5" class="form-control" name="problem_statement" required=""></textarea>
                        </div>
                        <div class="form-group">
                            <label for="companyName">Have you consulted with a lecturer or other expert/resource person in preparation of your proposal *</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="lecturer" id="lecturer1" value="1" required="" @if($project->id != null) checked @endif>
                                <label class="form-check-label" for="lecturer1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="lecturer" id="lecturer0" value="0">
                                <label class="form-check-label" for="lecturer0">No</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="companyName">Lecturer Name (if any)</label>
                            <input type="text" id="companyName" class="form-control" name="lecturer_name" value="{{$project->lecturer}}">
                        </div>

                      <h4 class="form-section text-primary">
                        <i class="ft-file-text primary"></i> Upload Proposal Document</h4>
                        <div class="form-group">
                         <input type="file" id="proposal_doc" class="form-control" name="proposal_doc" required="">

                        </div>


                        <div class="form-group">
                            <label for="comments"> Additional Comments</label>
                            <textarea id="comments" rows="3" class="form-control" name="comments"></textarea>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="button" class="btn btn-danger mr-1">
                            <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">
                            <i class="la la-check-square-o"></i> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        $('#student2').select2();

        $('#checkbox').val(false);
        $('#checkbox').change(function() {
            if(this.checked) {
                $(this).val(true);
                $('#student2-container').hide();
            }else{
                $(this).val(false);
                $('#student2-container').show();
            }
        });

        $("select#student2").change(function(){
            var student2_id = $(this).children("option:selected").val();
            console.log("Student 2 -> " + student2_id);

            $.ajax({
                url:"/get_student_by_id/"+student2_id,
                success:function(data){
                    console.log(data)
                    $("#name2").val(data.fullname)
                    $("#email2").val(data.student.email)
                    $("#reg_no2").val(data.bio_data.registration_no)
                }
            });


        });
    });


</script>

@endsection
