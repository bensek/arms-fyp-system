@extends('student_portal.basic')

@section('content')
<div class="row justify-content-center">
<div class="col-xl-8 col-lg-8 col-md-8">
<div class="card">
    
        <div class="card-header">
            <h4 class="card-title text-primary" id="basic-layout-form">Proposal Information</h4>
            @if($proposal->status->name == "Approved")
            <div class="heading-elements">
              <button data-target="#addPresentation" data-toggle="modal" class="btn btn-success btn-min-width mr-1 mb-1">Submit Presentation</button>
              <button data-target="#addReport" data-toggle="modal" class="btn btn-success btn-min-width mr-1 mb-1">Upload Report</button>
            </div>
            @endif
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-class', 'alert-success') }} alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
                @endif
                @if($errors->any())
               
                <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                     <h6 class="text-white">{{$errors->first()}}</h6>
                </div>
              @endif
                <h5><strong>Project Title:  {{$proposal->title}}</strong></h5>

                <hr>
                <h6 class="text-bold-600 mt-2"> Status
                    <span class="badge badge-pill badge-{{$proposal->status->class}} text-white">{{$proposal->status->name}}</span>
                </h6>
                <h6 class="text-bold-600 mt-2"> Submitted On:
                    <span class="text-danger">{{$proposal->dateTime}}</span>
                </h6>
                @if($proposal->is_individual_project == 0)
                  <h6 class="text-bold-600 mt-2"> Project Partner:
                    <span class="">{{$proposal->name2}}</span>
                </h6>
                @endif
                <h6 class="text-bold-600 mt-2">Problem Statement
                </h6>
                <p class="text-justify">{{$proposal->problem_statement}}</p>
                  <form action="{{route('download')}}" method="GET">
                @csrf
                <input type="hidden" name="path" value='{{$proposal->document}}'>
                <button type="submit" class="btn btn-outline-primary btn-min-width mr-1 mb-1">
                                <i class="la la-download"></i> Download Attachment
                </button>
                </form>
            </div>
        </div>
    </div>

<div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Presentations</h4>
                                <a class="heading-elements-toggle">
                                    <i class="fa fa-ellipsis-v font-medium-3"></i>
                                </a>
                              
                            </div>
                            <div class="card-body pl-0 pr-0">
                               <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Date Submitted</th>
                                                    <th>Notes</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($presentations as $pr)
                                                <tr>
                                                    <td>{{$pr->title}} <br>
                                                         <small class="text-danger">
                                                            @if($pr->type == 'mid_term')
                                                                Mid term presentation
                                                            @endif
                                                            @if($pr->type == 'final')
                                                                Final presentation
                                                            @endif
                                                        </small>
                                                    </td>
                                                    <td>{{$pr->created_at->isoFormat('dddd Do MMMM YYYY')}}</td>
                                                    <td>{{$pr->notes}}</td>
                                                    <td>
                                                        <div class="row">
                                                        <form action="{{route('download')}}" method="GET">
                                                        @csrf
                                                        <input type="hidden" name="path" value='{{$pr->document}}'>
                                                        <button type="submit" class="mr-1 mb-1 btn btn-outline-success btn-sm" data-toggle="tooltip" data-original-title="Download Slides">
                                                                <i class="ft-download"></i>
                                                        </button>
                                                        </form>

                                                        @if($pr->user_id == Auth::id())
                                                           
                                                        <form action="{{route('delete_presentation')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$pr->id}}">
                                                             <button type="submit" class="mr-1 mb-1 btn btn-outline-danger btn-sm" data-toggle="tooltip" data-original-title="Delete Presentation"><i class="ft-trash"></i> </button>
                                                        </form>
                                                        @endif
                                                        </div>
                                                      
                                                        
                                                    </td>
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>



<div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Reports</h4>
                                <a class="heading-elements-toggle">
                                    <i class="fa fa-ellipsis-v font-medium-3"></i>
                                </a>
                              
                            </div>
                            <div class="card-body pl-0 pr-0">
                               <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Date Submitted</th>
                                                    <th>Notes</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($reports as $report)
                                                <tr>
                                                    <td>{{$report->title}}<br>
                                                         <small class="text-danger">
                                                           @if($report->type == 'project_report')
                                                           Final Project Report
                                                           @else
                                                           Full Project Proposal Report
                                                           @endif
                                                        </small >
                                                    </td>
                                                    
                                                    <td>{{$report->created_at->isoFormat('dddd Do MMMM YYYY')}}</td>
                                                    <td>{{$report->notes}}</td>
                                                    <td>
                                                        <div class="row">
                                                              <form action="{{route('download')}}" method="GET">
                                                        @csrf
                                                        <input type="hidden" name="path" value='{{$report->document}}'>
                                                        <button type="submit" class="mr-1 mb-1 btn btn-outline-success btn-sm" data-toggle="tooltip" data-original-title="Download Report">
                                                                <i class="ft-download"></i>
                                                        </button>
                                                        </form>
                                                        @if($report->user_id == Auth::id())
                                                        <form action="{{route('delete_report')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$report->id}}">
                                                             <button type="submit" class="mr-1 mb-1 btn btn-outline-danger btn-sm" data-toggle="tooltip" data-original-title="Delete Report"><i class="ft-trash"></i> </button>
                                                        </form>
                                                        @endif
                                                        </div>
                                                      
                                                        
                                                    </td>
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                         

                        <button type="button" class="mr-1 mb-1 btn btn-outline-danger"data-toggle="modal" data-target="#confirmDelete">
                                                        Delete Proposal
                        </button>

                       
</div>

<div class="col-xl-4 col-lg-4 col-md-4">

   <div class="card">
        
        <div class="card-content collapse show">
            <div class="card-body">

                <p class="text-bold-600 text-primary">Main Supervisor</p>
                @if($proposal->supervisor_name != "")
                <h6 class="mt-1"> Name:
                    <span class="text-dark">{{$proposal->supervisor_name}}</span>
                </h6>
                <h6 class="mt-1"> Email:
                    <span class="text-dark">{{$proposal->supervisor_email}}</span>
                </h6>

                @else

                <h6 class="mt-1"> Name:
                                            <span class="text-dark">N/A</span>
                                        </h6>
                                        <h6 class="mt-1"> Email:
                                            <span class="text-dark">N/A</span>
                                        </h6>

                @endif
                <hr>

                <p class="text-bold-600 text-primary">Co-Supervisor</p>


                 @if($proposal->cosupervisor_name != "")
                <h6 class="mt-1"> Name:
                    <span class="text-dark">{{$proposal->cosupervisor_name}}</span>
                </h6>
                <h6 class="mt-1"> Email:
                    <span class="text-dark">{{$proposal->cosupervisor_email}}</span>
                </h6>

                @else

                <h6 class="mt-1"> Name:
                    <span class="text-dark">N/A</span>
                </h6>
                <h6 class="mt-1"> Email:
                    <span class="text-dark">N/A</span>
                </h6>

                @endif
                                           
                                            
            </div>
        
        </div>
    </div>


    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-primary" id="basic-layout-form">Proposal Notifications</h4>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                @if(count($notices) != 0)

                <div class="list-group">
                    @foreach($notices as $notice)                 
                        <div class="list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="text-bold-600">{{$notice->notification}}</h5>
                                <!-- <small class="text-muted">3 days ago</small> -->
                            </div>
                            <p class="text-muted">{{$notice->comment}}</p>
                            <small class="text-info">{{$notice->date}}</small>
                        </div>
                        @endforeach
                                            
            </div>
            @else
            <div class="text-center" style="height: 100px">
                No notifications available!!!
            </div>

            @endif
        </div>
    </div>
</div>



</div>
</div>


<div class="modal fade text-left" id="addPresentation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-modal="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h3 class="modal-title" id="myModalLabel35"> Submit Presentation</h3>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                </div>
                                                                <form method="POST" action="{{route('submit_presentation')}}" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <input type="hidden" name="proposal_id" value="{{$proposal->id}}">
                                                                    <div class="modal-body">
                                                                        <fieldset class="form-group position-relative">
                                                                            <label for="type">Type of Presentation</label>
                                            <select class="form-control" id="type" required="" name="type">
                                                <option selected="">Select</option>
                                                <option value="mid_term">Mid-term Oral Presentation</option>
                                                <option value="final">Final Oral Presentation</option>
                                            </select>
                                        </fieldset>
                                                                        <fieldset class="form-group floating-label-form-group">
                                                                            <label for="title">Title of Presentation</label>
                                                                            <input type="text" class="form-control" id="title" required=""name="title">
                                                                        </fieldset>
                                                                        <fieldset class="form-group floating-label-form-group">
                                                                            <label for="doc">Upload your presentation slides</label>
                                                                           <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="document" name="document" required="">
                                                <label class="custom-file-label" for="document">Choose file</label>
                                            </div>
                                                                        </fieldset>
                                                                        <fieldset class="form-group floating-label-form-group">
                                                                            <label for="title1">Brief Notes</label>
                                                                            <textarea class="form-control" id="title1" rows="3" placeholder="Optional" name="notes"></textarea>
                                                                        </fieldset>
                                                                    </div>
                                            <div class="modal-footer">
                                                <input type="reset" class="btn btn-secondary " data-dismiss="modal" value="close">
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>



<div class="modal fade text-left" id="addReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-modal="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h3 class="modal-title" id="myModalLabel35"> Upload Report</h3>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                </div>
                                                                <form method="POST" action="{{route('upload_report')}}" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <input type="hidden" name="proposal_id" value="{{$proposal->id}}">
                                                                    <div class="modal-body">
                                                                        <fieldset class="form-group position-relative">
                                                                            <label for="type">Type of Report</label>
                                            <select class="form-control" id="type" required="" name="type">
                                                <option selected="" disabled="">Select</option>
                                                <option value="project_proposal">Full Project Proposal Report</option>
                                                <option value="project_report">Final Project Report</option>
                                            </select>
                                        </fieldset>
                                                                        <fieldset class="form-group floating-label-form-group">
                                                                            <label for="title">Title of Report</label>
                                                                            <input type="text" class="form-control" id="title" required=""name="title">
                                                                        </fieldset>
                                                                        <fieldset class="form-group floating-label-form-group">
                                                                            <label for="doc">Upload your report</label>
                                                                           <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="document" name="document" required="">
                                                <label class="custom-file-label" for="document">Choose file</label>
                                            </div>
                                                                        </fieldset>
                                                                        <fieldset class="form-group floating-label-form-group">
                                                                            <label for="title1">Brief Notes</label>
                                                                            <textarea class="form-control" id="title1" rows="3" placeholder="Optional" name="notes"></textarea>
                                                                        </fieldset>
                                                                    </div>
                                            <div class="modal-footer">
                                                <input type="reset" class="btn btn-secondary " data-dismiss="modal" value="close">
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


<div class="modal fade text-left" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel1" aria-modal="true" style="padding-right: 17px;">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="basicModalLabel1">Delete Proposal</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>All information e.g presentations, reports, etc shall be deleted for this proposal. Are you sure you want to continue to delete this Proposal? </p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Cancel</button>
                                                                      <form action="{{route('delete_proposal', $proposal->id)}}" method="post">
                                                            @csrf
                                                             <button type="submit" class="btn btn-danger"> Yes, delete</button>
                                                        </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
@endsection