@extends('student_portal.basic')

@section('content')
<div class="row justify-content-center">
<div class="col-xl-4 col-lg-4 col-md-4">
    
</div>

<div class="col-xl-4 col-lg-4 col-md-4">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-primary" id="basic-layout-form">{{session('title')}}</h4>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                @if($errors->any())
               
                <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                     <h6 class="text-white">{{$errors->first()}}</h6>
                </div>
                @endif
               <form method="POST" action="{{route('update_comment')}}">
            @csrf
            <div class="row">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$comment->id}}">
                      <h5 class="">Edit comment</h5>
                      <fieldset class="form-group">
                        <textarea name="text" class="form-control" rows="3" placeholder="Type...">{{$comment->text}}</textarea>
                    </fieldset>
                    <button class="btn btn-primary">Update</button>
                </div>
                
            </div>
          
        </form>
                
            </div>
        </div>

    </div>

</div>
<div class="col-xl-4 col-lg-4 col-md-4">
</div>
</div>


@endsection