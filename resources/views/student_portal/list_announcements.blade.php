@extends('student_portal.basic')

@section('content')
<div class="row justify-content-center">
<div class="card col-12">
        <div class="card-header">
            <h2 class="text-primary" id="basic-layout-form">Notice Board</h2>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">



<div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
        @foreach($announcements as $i => $a)
        <a class="list-group-item list-group-item-action @if($selected == $a->id) active @endif" id="list-{{$a->id}}-list" data-toggle="list" href="#list-{{$a->id}}" role="tab" aria-controls="prince">
            <span class="font-medium-2"><strong>{{$a->title}} </strong></span> <br>
            <span class="font-small-3 ">{{$a->date}}</span>
        </a>
        @endforeach
    </div>
  </div>
  <div class="col-6">
    <div class="tab-content" id="nav-tabContent">
        @php $counter = 1 @endphp
        @foreach($announcements as $i => $a)
            @if($i == 0)
            <div class="tab-pane active" id="list-{{$a->id}}" role="tabpanel" aria-labelledby="list-{{$a->id}}-list">
                 {!! $a->description !!}
            </div>
            @else
            <div class="tab-pane " id="list-{{$a->id}}" role="tabpanel" aria-labelledby="list-{{$a->id}}-list">
                 {!! $a->description !!}
            </div>

            @endif
        @endforeach
    </div>
  </div>
</div>


            </div>
        </div>
    </div>

</div>

@endsection
