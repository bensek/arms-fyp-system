@extends('student_portal.basic')

@section('content')
                <!-- Revenue, Hit Rate & Deals -->
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="card">
                            <div class="card-header p-1">
                                <h3 class="text-center pt-2 text-danger">Projects List @if($year){{$year->name}}@endif</h3>

                            </div>
                             <div class="card-content collapse show">
                                 <div class="table-responsive">
                                     <table id="datatable" class="table table-striped">
                                         <thead>
                                         <tr>
                                             <th scope="col">#</th>
                                             <th scope="col">PROJECT TITLE</th>
                                             <th scope="col">LECTURER</th>
                                         </tr>
                                         </thead>
                                         <tbody>
                                         @php $counter = 1 @endphp
                                         @foreach($projects as $p)

                                         <tr>
                                             <td scope="row">{{$counter++}}</td>
                                             <td>
                                                 <div class="col-9 pl-0">
                                                     <a href="{{route('display_project',['id'=>$p->id])}}"><h4><strong>{{$p->title}}</strong></h4></a>
                                                     <div class="d-flex align-items-center">
                                                         @foreach($p->categories as $cat)
                                                             <div class="pl-0 align-middle">
                                                                 <p class="text-primary font-small-3"> <i class="ft-chevron-right"></i> {{$cat->name}}</p>
                                                             </div>
                                                         @endforeach

                                                     </div>
                                                 </div>
                                             </td>

                                             <td>{{$p->lecturer}}</td>
                                         </tr>
                                         @endforeach
                                         </tbody>
                                     </table>
                                 </div>

                             </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">

                        <div class="col-12">
                            @if($proposal)

                            <div class="card bg-gradient-directional-danger">
                                    <div class="card-header bg-hexagons-danger">
                                        <h4 class="card-title white">My Proposal</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li>
                                                    <a class="btn btn-sm btn-white danger box-shadow-1 round btn-min-width" href="#" target="_blank">{{$proposal->status->name}} <i class="ft-activity pl-1"></i></a>
                                                </li>
                                                <li></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content   bg-hexagons-danger">
                                        <div class="card-body">
                                            <div class="mx-2">

                                                <div class="text-center">
                                                    <a href="{{route('proposal_details', ['id'=>$proposal->id])}}">
                                                    <h4 class="text-uppercase"><strong>{{$proposal->title}}</strong></h4>
                                                    </a>
                                                    <hr>
                                                    <small class="text-bold-600 mt-2 text-white"> Submitted On:
                                                        <span class="text-white">{{$proposal->dateTime}}</span>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </div>



                        @else

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body pt-0 pb-1">
                                    <div class="text-center text-primary p-3">
                                        <h3 class="text-primary"><strong>No Proposal</strong></h3>
                                    <h6 class="text-primary mt-1">You have not yet submitted any proposal. Click the button below to fill the form.</h6>
                                    <a href="{{route('submit_proposal')}}" class="btn btn-outline-warning round mt-1">Submit</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endif

                        </div>


                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                <h4 class="card-title text-danger">Notice Board</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a href="{{route('get_announcements')}}" class="btn btn-glow btn-round btn-bg-gradient-x-red-pink btn-md">More</a></li>
                                    </ul>
                                </div>
                            </div>
                                <div class="card-content">

                                    <ul class="list-group list-group-flush">
                                        @if(count($announcements) > 0)
                                        @foreach($announcements as $an)
                                        <li class="list-group-item">
                                            <a href="{{ route('display_announcement', ['id'=>$an->id]) }}">
                                            <span class="font-medium-1 text-muted">{{$an->title}} </span> <br>
                                            <small class="text-primary"><em>{{$an->date}}</em></small>

                                        </a>


                                        </li>
                                        @endforeach
                                            @else
                                                <div class="text-center" style="height: 200px"> <span class="mt-4">No announcements available</span></div>
                                            @endif

                                    </ul>
                                </div>
                            </div>


                        </div>



                    </div>
                    </div>

                <script type="text/javascript">
                    jQuery(document).ready(function(){
                        $("#datatable").DataTable({
                            columnDefs: [{ orderable: false }],

                        });
                    });
                </script>
@endsection
