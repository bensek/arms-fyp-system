@extends('student_portal.basic')

@section('content')
                <!-- Revenue, Hit Rate & Deals -->
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-header p-1">
                                <h3 class="text-center pt-2 text-success">{{session('title')}} for {{$year->name}}</h3>

                            </div>
                             <div class="card-content collapse show">


                                 <div class="table-responsive">
                                     <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 ">
                                            <div class="row">
                                                <div class="col-12">
                                     <table id="datatable" class="table table-striped">
                                         <thead>
                                         <tr>
                                             <th scope="col">PROJECT TITLE</th>
                                             <th scope="col">LECTURER</th>
                                             <th></th>
                                         </tr>
                                         </thead>
                                         <tbody>
                                         @php $counter = 1 @endphp
                                         @foreach($projects as $p)

                                         <tr>
                                             <td>
                                                 <div class="col-9 pl-0">
                                                     <a href="{{route('display_project',['id'=>$p->id])}}"><h4><strong>{{$p->title}}</strong></h4></a>
                                                     <div class="d-flex align-items-center">
                                                         @foreach($p->categories as $cat)
                                                             <div class="pl-0 align-middle">
                                                                 <p class="text-primary font-small-3"> <i class="ft-chevron-right"></i> {{$cat->name}}</p>
                                                             </div>
                                                         @endforeach

                                                     </div>
                                                    
                                             </td>

                                             <td>{{$p->lecturer}}</td>
                                             <td>  <a href="{{route('display_project', $p->id)}}" class="mr-1 mb-1 btn btn-outline-success btn-sm">View <i class="ft-eye"></i> </a><a href="{{route('submit_proposal', $p->id)}}" class="mr-1 mb-1 btn btn-success btn-sm">Apply <i class="ft-arrow-right"></i> </a></td>
                                         </tr>
                                         @endforeach
                                         </tbody>
                                     </table>
                                 </div>
                             </div>
                         </div>
                                 </div>

                             </div>
                        </div>
                    </div>
                    </div>

                <script type="text/javascript">
                    jQuery(document).ready(function(){
                        $("#datatable").DataTable({
                            columnDefs: [{ orderable: false }],
                            "bPaginate": false,
                            "bLengthChange": false,
                            "bFilter": true,
                            "bInfo": false,
                            "bAutoWidth": false,
                        });
                    });


                </script>
@endsection
