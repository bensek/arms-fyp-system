@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('list_academic_years')}}">Academic Years</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{session('title')}}</h4>
                                    <div class="heading-elements">
                                      
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form needs-validation" method="post" action="{{route('store_academic_year')}}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="form-body">
                                                <input type="hidden" name="id" value="{{$year->id}}">                                             
                                                <div class="form-group col-12">
                                                    <label for="schoolName">Name<span class="text-danger">*</span></label>
                                                    <input type="text" id="schoolName" class="form-control" placeholder="e.g 2020/2021" name="name" required value="{{$year->name}}">
                                                    <small class="text-muted">The name is displayed anywhere to represent the current academic year</small>
                                                </div>
                                                
                                            </div>

                                            <div class="form-body row">
                                                <div class="form-group col-6">
                                                    <label for="schoolName">Start Date<span class="text-danger">*</span></label>
                                                    <input type="date" id="schoolName" class="form-control" placeholder="e.g 2020/2021" name="start_date" required value="{{$year->start_date}}">
                                                </div>
                                                 <div class="form-group col-6 pr-2">
                                                    <label for="schoolName">End Date<span class="text-danger">*</span></label>
                                                    <input type="date" id="schoolName" class="form-control" name="end_date" required value="{{$year->end_date}}">
                                                </div>
                                                
                                            </div>
                                             <div class="form-group">
                                                <label for="companyName">Is Current Academic Year?<span class="text-danger">*</span><br><small class="text-muted">If Yes, any current year will be disabled</small>
                                                </label>
                                                                                                 <br>
                                                    <div class=" form-check form-group form-check-inline">
                                                        <input class="form-check-input form-check-inline" type="radio" name="is_current" id="inlineRadio1" value="1" {{ ($year->is_current == 1) ? "checked" : ""}}>
                                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                    </div>

                                                    <div class="form-check form-group form-check-inline">
                                                        <input class="form-check-input" type="radio" name="is_current" id="inlineRadio2" value="0" {{ ($year->is_current == 0) ? "checked" : ""}}>
                                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                             <div class="col-sm-12 col-md-6">
                                                <div class="form-group col-12">
                                                    <label for="schoolName">Deadline for Submissions<span class="text-danger"></span></label>
                                                    <input type="date" id="schoolName" class="form-control"name="deadline" value="{{$year->deadline}}">
                                                    <small class="text-muted">This is the main deadline for submitting proposals</small>
                                                </div>
                                                <div class="form-group col-12">
                                                    <label for="schoolName">Late Submission Deadline<span class="text-danger"></span></label>
                                                    <input type="date" id="schoolName" class="form-control"name="late_deadline" value="{{$year->late_deadline}}">
                                                    <small class="text-muted">This is the deadline for late submissions</small>
                                                </div>
                                            </div>
                                                
                                            

                                                                                                 
                                    </div>



                                            <div class="form-actions">
                                                <a href="{{route('list_academic_years')}}" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){});
</script>


@endsection