@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Academic Years
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">List of Academic Years</h4>
                                    <div class="heading-elements">
                                       <a href="{{route('new_academic_year')}}" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-plus font-medium-3"></i>New</a> 
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                    	@if(Session::has('message'))
                                    	<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
										@endif
                                        
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            	<div class="row">
                                            		<div class="col-sm-12">

                                            	<table id="datatable" class="table table-bordered">
                                                <thead>
                                                	<tr>
	                                                	<td>#</td>
	                                                	<td>NAME</td>
	                                                	<td>DATE RANGE</td>
                                                        <td>STATUS</td>
                                                        <td>DEADLINE</td>
                                                        <td>LATE SUBMISSIONS</td>
	                                                	<td>ACTIONS</td>
                                                	</tr>
                                                </thead>
                                                <tbody>
                                                	@php $counter = 1 @endphp
                                                	@foreach($years as $year)
	                                                <tr>
	                                                	<th>{{$counter++}}</th>
	                                                	<th>{{$year->name}}</th>
	                                                	<th>{{$year->start_date}} to {{$year->end_date}}</th>
                                                        <th>
                                                            @if($year->is_current ==1)
                                                            <div class="badge badge-primary">Current</div>
                                                            @else
                                                            <div class="badge badge-light">Disabled</div>
                                                            @endif
                                                        </th>
                                                        <th>{{$year->deadline}}</th>
                                                        <th>{{$year->late_deadline}}</th>
	                                                	<th>
	                                                		<a href="/academic_years/edit/{{$year->id}}" class="btn btn-icon btn-info btn-sm"><i class="ft-edit"></i></a>
	                                                		<a href="#" onclick="deleteYear({{$year->id}})" class="btn btn-icon btn-danger btn-sm"><i class="ft-trash-2"></i>
	                                                		</a>
	                                                		

	                                                	</th>

	                                                	
	                                                </tr>
	                                                @endforeach 
                                        		</tbody>
                                               
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
            	jQuery(document).ready(function(){
					$("#datatable").DataTable({
						columnDefs: [{ orderable: false, targets: [3] }],
					});
            	});

            	function deleteYear(id){
            		Swal.fire({
				        title: 'Are you sure?',
				        text: 'You will not be able to recover this academic year!',
				        type: 'warning',
				        showCancelButton: true,
				        confirmButtonColor: '#6610f2',
				        cancelButtonColor: '#607D8B',
				        confirmButtonText: 'Yes, delete it!',
				        closeOnConfirm: false
				    }).then((result) => {
				    	if(result.value){
				    		$.ajax({
					            url: '/academic_years/delete/'+id,
					            type: "DELETE",
					            data:{ _token: "{{csrf_token()}}" }
					        }).done(function() {

					            swal({
					                title: "Deleted", 
					                text: "Academic year has been successfully deleted", 
					                type: "success",
					                confirmButtonColor: '#6610f2',

					            }).then(function() {
					                location.href = '/academic_years';
					            });
					        });

				    	}

				       
				    });

            	}
            	
            </script>


@endsection