<h1>Email Verification</h1>
  
Please verify your email by clicking the link below:  <br>

<a href="{{ route('user.verify', $details['token']) }}">Verify Email</a>