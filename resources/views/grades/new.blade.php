@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('list_grades', ['id'=>$student->programme_id, 'year_id' => $year_id])}}">Grades</a>
                    </li>
                     <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif
                                    
                                    <form>
                                     <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="ft-user"></i> Student Information</h4>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Full Names</label>
                                                           
                                                            <input type="text" class="form-control"value="{{$student->fullnames()}}" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control"value="{{$student->email}}" disabled="">
                                                        </div>
                                                    </div>
                                                     <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Registration No</label>
                                                            <input type="text" class="form-control"value="{{$student->bio_data->registration_no}}" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Student No</label>
                                                            <input type="text" class="form-control"value="{{$student->bio_data->student_no}}" disabled="">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                               
                                            </div>
                                        </form>



                                    <form class="form" action="{{route('store_grade')}}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{$student->id}}" name="student_id" >
                                        <input type="hidden" name="grade_id" value="{{$grade->id}}">
                                            <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="ft-clipboard"></i> Add Marks</h4>
                                                    <p>Enter marks for each section under 100%</p>
                                                    <div class="row mt-2">

                                                        <div class="col-md-6">
                                                            <div class="table-responsive">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                            <tr>
                                                <th><h4><strong>Item</strong><h4></th>
                                                <th><h4><strong>Marks(100%)</strong></h4></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><h4>Mid-term Presentation</h4></td>
                                                <td><h4>{{$grade->midterm_presentation}}</h4></td>
                                            </tr>
                                             <tr>
                                                <td><h4>Project Proposal Report</h4></td>
                                                <td><h4>{{$grade->proposal_report}}</h4></td>
                                            </tr>
                                             <tr>
                                                <td><h4>Final Presentation</h4></td>
                                                <td><h4>{{$grade->final_presentation}}</h4></td>
                                            </tr>
                                             <tr>
                                                <td><h4>Final Project Report</h4></td>
                                                <td><h4>{{$grade->final_report}}</h4></td>
                                            </tr>
                                             <tr>
                                                <td><h4 class="text-success"><strong>Average Total</strong></h4></td>
                                                <td><h2 class="text-success"><strong>{{$grade->total}}<strong></strong></h2></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                                            
                                                        </div>

                                                         <div class="col-md-6">
                                                            <div class="row">
                                                        @role('coordinator|admin')
                                                        <div class="form-group col-md-6">
                                                            <label><h4>Mid-term Presentation</h4></label>
                                                            <input type="number" min="1" max="100" class="form-control input-xl" name="midterm_presentation" value="{{$grade->midterm_presentation}}">
                                                        <p>12% of total</p>
                                                        </div>
                                                        @endrole
                                                        <div class="form-group col-md-6">

                                                        <label><h4>Project Proposal Report</h4></label>

                                                        <input type="number" min="1" max="100" class="form-control input-xl" name="proposal_report" value="{{$grade->proposal_report}}">
                                                        <p>8% of total</p>
                                                        </div>

                                                        @role('coordinator|admin')

                                                         <div class="form-group col-md-6">
                                                            <label><h4>Final Presentation</h4></label>
                                                            <input type="number" min="1" max="100" class="form-control input-xl" name="final_presentation" value="{{$grade->final_presentation}}">
                                                        <p>48% of total</p>
                                                        </div>
                                                        @endrole

                                                           <div class="form-group col-md-6">
                                                            <label><h4>Final Project Report</h4></label>
                                                            <input type="number" min="1" max="100" class="form-control input-xl" name="final_report" value="{{$grade->final_report}}">
                                                        <p>32% of total</p>
                                                        </div>                                                                
                                                            </div>
                                                             
                                                            
                                                       
                                                      
                                                        

                                                         <div class="form-actions">
                                               
                                                <button type="submit" class="btn btn-success btn-block">
                                                    <i class="la la-check-square-o"></i> Grade
                                                </button>
                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                               
                                            </div>

                                           
                                        </form>   


                                                           
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

@endsection