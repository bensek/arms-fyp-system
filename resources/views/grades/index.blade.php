@extends('layouts.master')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">{{session('title')}}</h3>
        </div>
        <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
                <div class="breadcrumb-wrapper mr-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active">{{session('title')}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                           <div class="card-header">
                           <!-- <h4 class="card-title"></h4> -->
                           <!--  <a href="{{route('send_email')}}" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white btn-sm"><i class="la la-plus font-medium-3"></i> Import Grades</a>  -->

                            <div class="heading-elements">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    @foreach($years as $year)

                                    <a href="{{route('list_grades', ['id'=>$programme->id, 'year_id'=> $year->id])}}" class="btn btn-outline-info btn-year text-white btn-md @if($current_year->id == $year->id) active @endif">{{$year->name}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif


                                <div class="table-responsive">
                                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <table id="datatable" class="table table-bordered">
                                                     <thead>
                                                <tr>
                                                    <th>Student</th>
                                                    <th>Reg No</th>
                                                    <th >Proposal <br>Report <br>(8%)</th>
                                                    <th>Mid-Term <br>Presentation <br>(12%)</th>
                                                    <th>Final <br>Report <br>(32%)</th>
                                                    <th>Final <br>Presentation<br>(48%)</th>
                                                    <th>TOTAL <br>(100%)</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($students as $student)
                                                <tr >
                                                <th>{{$student->fullnames()}}</th>
                                                <th>{{$student->bio_data->registration_no}}</th>
                                               <td>@if(!is_null($student->grade)){{$student->grade->proposal_report}}@endif</td>
                                                <td>@if(!is_null($student->grade)){{$student->grade->midterm_presentation}}@endif</td>
                                                <td>@if(!is_null($student->grade)){{$student->grade->final_report}}@endif</td>
                                                <td>@if(!is_null($student->grade)){{$student->grade->final_presentation}}@endif</td>
                                                <td>
                                                    @if(!is_null($student->grade))
                                                    {{$student->grade->total}}
                                                    @endif
                                                </td>
                                                <th> <a href="{{route('grade_student', ['id'=>$student->id, 'year_id'=>$current_year->id])}}" class="btn btn-info mr-1 mb-1 btn-sm">Grade</a></th>

                                                </tr>
                                                @endforeach

                                            </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </section>
        <!--/ Zero configuration table -->

    </div>

    <script type="text/javascript">
        jQuery(document).ready(function(){
                    $("#datatable").DataTable({
                        columnDefs: [{ orderable: false, targets: [6] }, 

                        ],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf'
                        ]
                    });
                });
    </script>


@endsection
