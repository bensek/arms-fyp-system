@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('list_logs')}}">Grades</a>
                    </li>
                     <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif
                                    
                                    <form>
                                     <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="ft-user"></i> Student Information</h4>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Full Names</label>
                                                           
                                                            <input type="text" class="form-control"value="{{$grade->student->fullnames()}}" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control"value="{{$grade->student->email}}" disabled="">
                                                        </div>
                                                    </div>
                                                     <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Registration No</label>
                                                            <input type="text" class="form-control"value="{{$grade->student->bio_data->registration_no}}" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Total Marks</label>
                                                            <input type="text" class="form-control"value="{{$grade->total}}" disabled="">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                               
                                            </div>
                                        </form>

 
                                <h4 class="mb-4"><strong>Logs</strong></h4>


                                <div class="table-responsive">
                                        <table class="table">
                                           
                                            <tbody>
                                            @foreach($grade->audits as $a)
                                            <tr>
                                                    <td>{{$a->created_at->isoFormat('Do, MMMM Y H:m')}} <br>
                                                        <span class="badge badge-secondary"> {{$a->event}} </span> <br>
                                                        Ip Address: {{$a->ip_address}} <br>
                                                        User: {{$a->user->fullnames()}}

                                                    </td>
                                                    <td><span class="text-danger">Old: {{json_encode($a->old_values)}}</span><br>
                                                    <span class="text-success">New: {{json_encode($a->new_values)}}</span>
                                                </td>
                                                   
                                            </tr>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

@endsection