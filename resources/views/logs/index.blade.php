@extends('layouts.master')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">{{session('title')}}</h3>
        </div>
        <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
                <div class="breadcrumb-wrapper mr-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active">{{session('title')}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                           <div class="card-header">
                           <h4 class="card-title">List of All Grades</h4>

                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
                                    @endif


                                <div class="table-responsive">
                                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <table id="datatable" class="table table-bordered">
                                                     <thead>
                                                <tr>
                                                    <th>Student</th>
                                                    <th>Total Marks</th>
                                                    <th>Programme</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($grades as $g)
                                                <tr >
                                                <td>{{$g->student->fullnames()}}</td>
                                                
                                                <td>
                                                    {{$g->total}}
                                                </td>
                                                <td>{{$g->student->programme->name}}</td>
                                                <td> <a href="{{route('show_logs', $g->id)}}" class="btn btn-info mr-1 mb-1 btn-sm">View Logs</a></td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </section>
        <!--/ Zero configuration table -->

    </div>

    <script type="text/javascript">
        jQuery(document).ready(function(){
                    $("#datatable").DataTable({
                        columnDefs: [{ orderable: false, targets: [3] }],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf'
                        ]
                    });
                });
    </script>


@endsection
