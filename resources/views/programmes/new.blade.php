@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/programmes">Programmes</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{session('title')}}</h4>
                                    <div class="heading-elements">
                                      
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form needs-validation" method="post" action="/admin/programmes">
                                            @csrf
                                            <div class="form-body">
                                            <input type="hidden" name="id" value="{{$programme->id}}">    
                                            <div class="col-6 ">        
                                                <div class="form-group">
                                                    <label>Department<span class="text-danger">*</span></label>
                                                    <select class="select2 form-control" id="department" name="department_id" required="" >
                                                        <option disabled="" selected="" value="">Select department</option>
                                                        @foreach($departments as $department)
                                                            <option value="{{$department->id}}" {{$department->id == $programme->department_id ? 'selected' : ''}}>{{$department->name}}</option>
                                                        @endforeach
                                                        
                                                    </select>
                                                    
                                                </div>                
                                                <div class="form-group">
                                                    <label for="programmeName">Programme Name<span class="text-danger">*</span></label>
                                                    <input type="text" id="programmeName" class="form-control" placeholder="Enter the programme name" name="name" required value="{{$programme->name}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="code">Programme Code<span class="text-danger">*</span></label>
                                                    <input type="text" id="code" class="form-control" placeholder="Enter the short form of the programme" name="code" value="{{$programme->code}}" required="">
                                                    <small class="text-muted">e.g BSCE / BSEE</small>
                                                </div>
                                               
                                            </div>
                                                
                                            </div>

                                            <div class="form-actions">
                                                <a href="/admin/programmes" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $('#department').select2();
    });
</script>


@endsection