
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>{{session('title')}} | {{ config('app.name')}}</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/chat-application.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/dashboard-analytics.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/advanced-cards.css">

    <!-- END: Custom CSS-->

    <!-- jQuery Library -->
    <script src="/js/core/libraries/jquery.min.js" type="text/javascript"></script>
     <script src="/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
     <style type="text/css">
          #invalid-feedback{
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #fa626b;
         }
         .navbar-dark.navbar-horizontal {
            background: #23A74C;
        }
        .content-wrapper-before{
            background: #23A74C;
        }
     </style>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu 2-columns  " data-open="hover" data-menu="horizontal-menu"  data-col="2-columns">

    <!-- BEGIN: Header-->
    <!-- fixed-top-->
   
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-static-top navbar-light navbar-brand-center">
        <div class="navbar-header">
          
            
        </div>
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto">
                      <li class="nav-item d-none d-md-block">
                        <a class="navbar-brand" href="{{route('student_dashboard')}}" ><img class="brand-logo" style="height: 60px !important;" src="/images/mak_logo.png">
                        <span class="white"> FINAL YEAR PROJECT SYSTEM [ USER MANUAL ]</span></a>
                          </li> 
                          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>       
                    </ul>
                     <ul class="nav navbar-nav float-right">
                        <li class="nav-item mr-2"><a href="{{route('dashboard')}}" class="btn btn-danger round btn-min-width mr-1 mb-1 mt-1">Back to Dashboard</a></li>
                    </ul>
                   
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                
                <li class="nav-item"><a class="nav-link" href="{{route('index.manual')}}"><span> Welcome</span></a>
                    
                </li>
                @role('coordinator')
                <li class="nav-item"><a class="nav-link" href="{{route('manual.coordinator')}}"><span> Coordinator's Manual</span></a>
                </li>
                @endrole

                @role('supervisor|coordinator')
                <li class="nav-item"><a class="nav-link" href="{{route('manual.lecturer', 0)}}"><span> Lecturer's Manual</span></a>
                    
                </li>
                @endrole
                @role('student|coordinator')
                <li class="nav-item"><a class="nav-link" href="{{route('manual.student')}}"><span> Student's Manual</span></a>
                </li>
                @endrole
                <!-- <li class="nav-item"><a class="nav-link" href="index.html"><i class="ft-monitor"></i><span> Projects List</span></a>
                </li> -->
            
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                @yield('content')

            </div>
        </div>
    </div>
    <!-- END: Content-->




    <!-- BEGIN: Vendor JS-->
    <script src="/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script type="text/javascript" src="/vendors/js/ui/jquery.sticky.js"></script>
    <script src="/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
    <script src="/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/js/core/app-menu.js" type="text/javascript"></script>
    <script src="/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/js/scripts/pages/dashboard-analytics.js" type="text/javascript"></script>
    <!-- END: Page JS-->
    <script src="/vendors/js/forms/select/select2.full.min.js"></script>

    <script src="/vendors/js/tables/jquery.dataTables.min.js"></script>
    <script src="/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/js/custom-file-input.js"></script>
    <script src="/js/scripts/cards/card-advanced.js" type="text/javascript"></script>

</body>
<!-- END: Body-->

</html>