@extends('user_manual.base')

@section('content')

<div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Thank you for visiting the {{ config('app.name')}}. </h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                   
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body p-2" >

                                	<p>If you have any queries that are beyond the scope of this manual, please feel free to email us on bensek73@gmail.com or whatsapp us on +256706820009. Thank you so much!</p>

                                	<p><strong>Created: 12/08/2019 - V1</strong></p>
                                	<p><strong>Created: 05/12/2021 - V2</strong></p>
                                	<p><strong>By: Arms Project Makerere University</strong></p>
                                	<p>{{ config('app.name')}} is a web system for final year students and lecturers at the College of Engineering Design Art and Technology at Makerere University. Both users require registeration and approval to start using the system.


                                	</p>
                                	<p>The system allows students to submit their final project proposals and wait for review and feedback from the lecturers/coordinatiors. After approval, the student goes ahead and submits presentations and reports on the system.</p>

                                	<p>The Cordinator/Lecturers receive the proposals from the students and reviews and either approves or rejects the proposal with a reason. For approved projects, the lecturers receives presentations and reports for review.</p>

                             <!--  <div class="row">
                                	<div class="col-xl-6 col-lg-6 col-6">
                                		<a href="{{route('manual.student')}}">
                            <div class="card bg-gradient-directional-success">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="media-body text-white text-left align-self-bottom mt-3">
                                                <span class="d-block mb-1 font-medium-1">Student's Manual</span>
                                                <h1 class="text-white mb-0"></h1>
                                            </div>
                                            <div class="align-self-top">
                                                <i class="icon-earphones-alt icon-opacity text-white font-large-4 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                    

                        <div class="col-xl-6 col-lg-6 col-6">

                                		<a href="{{route('manual.lecturer')}}">
                            <div class="card bg-gradient-directional-danger">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="media-body text-white text-left align-self-bottom mt-3">
                                                <span class="d-block mb-1 font-medium-1">Lecturer's Manual</span>
                                                <h1 class="text-white mb-0"></h1>
                                            </div>
                                            <div class="align-self-top">
                                                <i class="icon-speedometer icon-opacity text-white font-large-4 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>

                    </div> -->





                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection