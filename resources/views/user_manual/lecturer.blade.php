@extends('user_manual.base')

@section('content')

<div class="card">
    <div class="card-header">
        <h4 id="basic-forms" class="card-title">Lecturer/Supervisor Portal</h4>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-content collapse show" aria-expanded="true">
        <div class="card-body">

        	<div id="accordion3" class="card-accordion">
                                <div class="card collapse-icon accordion-icon-rotate">
                                    <div class="card">
                                        <div class="card-header" id="headingGOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC1" aria-expanded="false" aria-controls="accordionC1">
                                                    1. Registration and Login
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="accordionC1" class="collapse" aria-labelledby="headingGOne" data-parent="#accordion3" style="">
                                            <div class="card-body">
                                                <p>For lecturers to access the system they must create accounts and use a valid email.</p>

                                                <p>a) Go to <a href="{{route('register_lecturer')}}" target="_blank"> the link</a> to register. Fill in the form correctly and submit.</p>

                                                <img src="/images/manual/reg2.png" height="600">

                                                <p>b) An email link will be sent to your email. Click on it to verify your account. </p>

                                                <p>c) After email verification, wait for your account to be approved by the coordinator. This usually takes a few hours. You will receive an email notification to start using the system.</p>

                                                <p>c) Go to <a href="{{route('login')}}" target="_blank"> the link</a> to login if you already have an account.</p>
                                                <img src="/images/manual/login.png" height="600">


                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingGTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC2" aria-expanded="false" aria-controls="accordionC2">
                                                    2. Lecturer Dashboard and Features
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC2" class="collapse" aria-labelledby="headingGTwo" data-parent="#accordion3" style="">
                                            <div class="card-body">
                                                <p>After login the lecturer can access many features.</p>
                                                <h5>a. Proposals</h5>
                                                <p>View all proposals that you supervisor under the proposals menu</p>
                                                <h5>b. Presentations</h5>
                                                <p>Receive mid-term,final and supplementary presentations under the presentations tab from your students</p>
                                                <h5>c. Reports</h5>
                                                <p>Receive and review mid-term and final reports documents from your student</p>
                                                <h5>d. Panels</h5>
                                                <p>View the Panel you have been allocated to under Panels</p>

                                                <h5>e. Grades</h5>
                                                <p>Add grades of a particular student</p>
                                                 <img src="/images/manual/dashboard2.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingGThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC3" aria-expanded="false" aria-controls="accordionC3">
                                                    3. Projects List
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC3" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                            <div class="card-body">
                                                <p>Under the Projects List menu, you can view listed project ideas by lecturers and also Add A new project for students to consider.</p>
                                                  <img src="/images/manual/projects2.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                  
                                     <div class="card">
                                        <div class="card-header" id="headingGThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC4" aria-expanded="false" aria-controls="accordionC4">
                                                    4. Account Management and Logout
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC4" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                            <div class="card-body">
                                                <p>Click/Hover to the user icon to view account details and be able to edit profile information.</p>
                                                  <img src="/images/manual/account2.png" height="600">

                                                  <br><br>

                                            </div>
                                        </div>
                                    </div>
                                     
                                </div>
                            </div>
           
        </div>
    </div>
</div>
@endsection