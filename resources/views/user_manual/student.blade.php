@extends('user_manual.base')

@section('content')

<div class="card">
    <div class="card-header">
        <h4 id="basic-forms" class="card-title">Student Portal</h4>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-content collapse show" aria-expanded="true">
        <div class="card-body">

        	<div id="accordion3" class="card-accordion">
                                <div class="card collapse-icon accordion-icon-rotate">
                                    <div class="card">
                                        <div class="card-header" id="headingGOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC1" aria-expanded="false" aria-controls="accordionC1">
                                                    1. Registration and Login
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="accordionC1" class="collapse" aria-labelledby="headingGOne" data-parent="#accordion3" style="">
                                            <div class="card-body">
                                                <p>For students to access the system they must create accounts and use a valid email.</p>

                                                <p>a) Go to <a href="{{route('register_student')}}" target="_blank"> the link</a> to register. Fill in the form correctly and submit.</p>

                                                <img src="/images/manual/reg1.png" height="600">

                                                <p>b) An email link will be sent to your email. Click on it to verify your account. </p>
                                                <p>c) Go to <a href="{{route('login')}}" target="_blank"> the link</a> to login if you already have an account.</p>
                                                <img src="/images/manual/login.png" height="600">


                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingGTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC2" aria-expanded="false" aria-controls="accordionC2">
                                                    2. Student's Dashboard
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC2" class="collapse" aria-labelledby="headingGTwo" data-parent="#accordion3" style="">
                                            <div class="card-body">
                                                <p>After login/registration, you will be redirected to your dashboard. That shows the projects categories, projects list and notice announcements from the Project Coordinators</p>
                                                 <img src="/images/manual/dashboard1.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingGThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC3" aria-expanded="false" aria-controls="accordionC3">
                                                    3. Projects List
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC3" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                            <div class="card-body">
                                                <p>Via the Projects List tab, you can view all projects that you can consider to apply for. You can also search for any project by name or category. By tapping on one you can see the description and skills required to apply for the project.</p>
                                                  <img src="/images/manual/projects1.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="card">
                                        <div class="card-header" id="headingGFour">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC4" aria-expanded="false" aria-controls="accordionC4">
                                                    4. Submission of Proposal
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC4" class="collapse" aria-labelledby="headingGFour" data-parent="#accordion3">
                                            <div class="card-body">
                                                <p>There are two ways of submitting the proposal you want to work on as your final year project. <br>
                                                	1.Developing your own personal project idea.<br>
                                                	2. Applying for a project idea by a lecturer.
                                                </p>

                                                <h4><strong>1. Developing your own personal project idea.</strong></h4>
                                                Click on the Submit Proposal tab in the menu and fill in the form. If you are a single student you can check the box below your bio data.
                                                  <img src="/images/manual/proposal1.png" height="600">
                                                  <br><br>
                                                  <h4><strong>2. Applying for a project idea of a lecturer.</strong></h4>
                                                  <p>Go to Projects List and browse for the project you would like and then click apply. </p>
                                                   <img src="/images/manual/apply.png" height="600">

                                            </div>
                                        </div>
                                    </div>
                                     <div class="card">
                                        <div class="card-header" id="headingGThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC5" aria-expanded="false" aria-controls="accordionC5">
                                                    5. Account Management and Password Resetting
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC5" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                            <div class="card-body">
                                                <p>Via the Account Tab, you can view all your proposals and profile information. You can as well edit this info.</p>
                                                  <img src="/images/manual/account1.png" height="600">

                                                  <br><br>

                                                 <h5>Edit profile and Reset password.</h5>
                                                 <p>By clicking the Edit profile button above, you can change the profile info as well as reset your password from here
                                                </p>
                                                  <img src="/images/manual/edit1.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="card">
                                        <div class="card-header" id="headingGThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC6" aria-expanded="false" aria-controls="accordionC6">
                                                    6. Proposal Tracking and Supervisors
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC6" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                            <div class="card-body">
                                                <p>To access more info about your details go to your account (as described in 5 above) and click on the link <a>see details</a> under the proposal. The details show the status of the proposal and whether it is under review or approved. From this section you will also be able to see the supervisors assigned to you.</p>
                                                  <img src="/images/manual/proposal2.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                      <div class="card">
                                        <div class="card-header" id="headingGThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC7" aria-expanded="false" aria-controls="accordionC7">
                                                    7. Submit Presentations and Reports
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC7" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                            <div class="card-body">
                                                <p>After the proposal has been approved, you are able to submit mid-term or final presentations slides and also submit their reports. </p>
                                                  <img src="/images/manual/presentations1.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="card">
                                        <div class="card-header" id="headingGThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC8" aria-expanded="false" aria-controls="accordionC8">
                                                    8. Logout
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="accordionC8" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                            <div class="card-body">
                                                
                                                  <img src="/images/manual/logout.png" height="600">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
           
        </div>
    </div>
</div>
@endsection