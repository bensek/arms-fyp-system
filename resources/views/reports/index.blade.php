@extends('layouts.master')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">{{session('title')}}</h3>
        </div>
        <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
                <div class="breadcrumb-wrapper mr-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active">{{session('title')}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                                 <h4 class="card-title">{{$current_year->description}}</h4>

                            <div class="heading-elements">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    @foreach($years as $year)

                                    <a href="{{route('list_reports', ['type' => $type, 'year_id'=> $year->id])}}" class="btn btn-outline-info btn-year text-white btn-md @if($current_year->id == $year->id) active @endif">{{$year->name}}</a>
                                    @endforeach
                                </div>
                              

                            </div>
                            </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">


                                <div class="table-responsive">
                                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <table id="datatable" class="table table-bordered">
                                                     <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Students</th>
                                                    <th>Programme</th>
                                                    <th>Date Submitted</th>
                                                    <th>Document</th>
                                                    <th>Notes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($reports as $r)
                                                <tr>
                                                    <td>{{$r->title}}
                                                    </td>
                                                    <td>{{$r->user->fullnames()}}</td>
                                                    <td>{{$r->user->programme->name}}</td>
                                                    <td>{{$r->created_at->isoFormat('dddd Do MMMM YYYY')}}</td>
                                                    <td>
                                                    <form action="{{route('download')}}" method="GET">
                                                        @csrf
                                                        <input type="hidden" name="path" value='{{$r->document}}'>
                                                        <input type="submit" class="btn btn-info btn-sm" value="Download">
                                                    </form>
                                                    </td>
                                                    <td>{{$r->notes}}</td>
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </section>
        <!--/ Zero configuration table -->

    </div>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            $("#datatable").DataTable({
                columnDefs: [{ orderable: false, targets: [4] }],
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf'
                ]
            });
        });

        $("select#year-select").change(function(){
            var year = $(this).children("option:selected").val();
            var programme_id = $('#programme-id').val()
            console.log('programme_id -> '+ programme_id)

            // $.ajax({
            //     url:"/get_proposals_by_year/"+programme_id+"/"+year,
            //     success:function(data){
            //         //console.log(data)
            //         // $('#datatable').empty();
            //         // $.each(data, function(i, item) {
            //         //     $('<tr>').html("<th>" + i++ + "</th><th>" + data[i][1]  + "</th><th>" + data[i][2]  + "</th><th>" + data[i][3]  + "</th>").appendTo('#datatable');
            //         // });

            //     }
            // });
        });

        function deleteProgramme(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You will not be able to recover the supervisor's details!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#6610f2',
                cancelButtonColor: '#607D8B',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false
            }).then((result) => {
                if(result.value){
                    $.ajax({
                        url: '/supervisors/delete/'+id,
                        type: "DELETE",
                        data:{ _token: "{{csrf_token()}}" }
                    }).done(function() {

                        swal({
                            title: "Deleted",
                            text: "Supervisor has been successfully deleted",
                            type: "success",
                            confirmButtonColor: '#6610f2',

                        }).then(function() {
                            location.href = '/supervisors';
                        });
                    });

                }


            });

        }

    </script>


@endsection
