@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">List</h4>
                                <!-- <div class="heading-elements">
                                   <a href="#" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-plus font-medium-3"></i>New</a>
                                </div> -->
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                            <table id="datatable" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td>Date</td>
                                                    <td>Programme</td>
                                                    <td>Names and Registration Numbers</td>
                                                    <td>Email Addresses of students</td>
                                                    <td>Project Title</td>
                                                    <td>Problem Statement</td>
                                                    <td>MAIN SUPERVISOR</td>
                                                    <td>CO-SUPERVISOR</td>
                                                    <td>DOCUMENT</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($proposals as $pr)
                                                    <tr>
                                                        <th>{{$pr->created_at}}</th>
                                                        <th>{{$pr->programme->name}}</th>
                                                        <th>{{$pr->name1}} ({{$pr->reg_no1}}) & {{$pr->name2}}({{$pr->reg_no2}})</th>
                                                        <th>{{$pr->email1}} / {{$pr->email2}}</th>
                                                        <th><a class="text-primary" href="/proposals/{{$pr->programme_id}}/{{$pr->id}}"><u>{{$pr->title}}</u></a></th>
                                                        <th>{{$pr->problem_statement}}</th>
                                                        <td>{{$pr->supervisor_name}}</td>
                                                        <td>{{$pr->cosupervisor_name}}</td>
                                                          <th>
                                                                <form action="{{route('download')}}" method="GET">
                                                                @csrf
                                                                <input type="hidden" name="path" value='{{$pr->document}}'>
                                                                <input type="submit" class="btn btn-info btn-sm" value="Download">
                                                                </form>
                                                        </th>
                                                       
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
                jQuery(document).ready(function(){
                   
                    $("#datatable").DataTable({
                        columnDefs: [{ orderable: false, targets: [7] }],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf'
                        ]
                    });
                });

                function deleteProgramme(id){
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You will not be able to recover the supervisor's details!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#6610f2',
                        cancelButtonColor: '#607D8B',
                        confirmButtonText: 'Yes, delete it!',
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value){
                            $.ajax({
                                url: '/supervisors/delete/'+id,
                                type: "DELETE",
                                data:{ _token: "{{csrf_token()}}" }
                            }).done(function() {

                                swal({
                                    title: "Deleted",
                                    text: "Supervisor has been successfully deleted",
                                    type: "success",
                                    confirmButtonColor: '#6610f2',

                                }).then(function() {
                                    location.href = '/supervisors';
                                });
                            });

                        }


                    });

                }

            </script>


@endsection
