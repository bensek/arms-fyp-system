@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                </ol>
            </div>
        </div>
    </div>

</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">List</h4>
                                <div class="heading-elements">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        @foreach($years as $year)

                                            <a href="{{route('get_proposals', ['status'=> $status, 'year_id'=> $year->id])}}" class="btn btn-outline-info btn-year text-white btn-md @if($current_year->id == $year->id) active @endif">{{$year->name}}</a>
                                        @endforeach
                                    </div>


                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                            <table id="datatable" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>STUDENT ONE</td>
                                                    <td>STUDENT TWO</td>
                                                    <td>TITLE</td>
                                                    <td>PROGRAMME</td>
                                                    <td>MAIN SUPERVISOR</td>
                                                    <td>CO-SUPERVISOR</td>
                                                    <td>DOCUMENT</td>
                                                    <th>LECTURER</th>
                                                    <th>COMMENTS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $counter = 1 @endphp
                                                @foreach($proposals as $pr)
                                                    <tr>
                                                        <th>{{$counter++}}</th>
                                                        <th>{{$pr->name1}}<br>{{$pr->reg_no1}}<br>{{$pr->email1}}</th>
                                                        <th>{{$pr->name2}}<br>{{$pr->reg_no2}}<br>{{$pr->email2}}</th>
                                                         <td><a class="text-primary " href="{{route('show_proposal', ['programme_id' => $pr->programme_id, 'id' => $pr->id])}}"><u>{{$pr->title}}</u></a></td>
                                                        <th>{{$pr->programme->name}}</th>
                                                        <th></th>
                                                        <th></th>
                                                          <th>
                                                                <form action="{{route('download')}}" method="GET">
                                                                @csrf
                                                                <input type="hidden" name="path" value='{{$pr->document}}'>
                                                                <input type="submit" class="btn btn-info btn-sm" value="Download">
                                                                </form>
                                                        </th>
                                                        <th>{{$pr->lecturer_name}}</th>
                                                        <th>{{$pr->comments}}</th>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
                jQuery(document).ready(function(){
                    $("#datatable").DataTable({
                        columnDefs: [{ orderable: false, targets: [7] }],
                    });
                });

                function deleteProgramme(id){
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You will not be able to recover the supervisor's details!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#6610f2',
                        cancelButtonColor: '#607D8B',
                        confirmButtonText: 'Yes, delete it!',
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value){
                            $.ajax({
                                url: '/supervisors/delete/'+id,
                                type: "DELETE",
                                data:{ _token: "{{csrf_token()}}" }
                            }).done(function() {

                                swal({
                                    title: "Deleted",
                                    text: "Supervisor has been successfully deleted",
                                    type: "success",
                                    confirmButtonColor: '#6610f2',

                                }).then(function() {
                                    location.href = '/supervisors';
                                });
                            });

                        }


                    });

                }

            </script>


@endsection
