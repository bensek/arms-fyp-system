@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{$proposal->programme->code}} Proposals</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">{{$proposal->programme->code}} Proposals</a>
                    </li>
                     <li class="breadcrumb-item active">Project
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title pb-2">Proposal #{{$proposal->id}}</h4>
                                <div class="heading-elements row">
                                   <a href="#" class="btn btn-purple btn-min-width box-shadow mr-1 mb-1 white" data-toggle="modal" data-target="#statusModal"><i class="la la-plus font-medium-3"></i>Change Status</a>
                                   <a href="#" class="btn btn-warning btn-min-width box-shadow mr-1 mb-1 white" data-toggle="modal" data-target="#supervisorModal"><i class="la la-user font-medium-3"></i>Assign Supervisors</a>
                                   @if($proposal->status->name == 'Rejected')
                                    <a href="#" class="btn btn-danger btn-min-width box-shadow mr-1 mb-1 white" data-toggle="modal" data-target="#confirmDelete"><i class="la la-trash font-medium-3"></i>Move to Trash</a>
                                    @endif

                                   <!-- <a href="/supervisors/new" class="btn btn-primary btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-download font-medium-3"></i>Download Document</a>
                                     -->

                                </div>
                            </div>

                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                	@if(Session::has('message'))
                                	<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        {{ Session::get('message') }}
                                    </div>
									@endif

                                    <div class="px-2 mb-4">
                                        <h4 class="text-primary"><strong>Title: {{$proposal->title}}</strong></h4>

                                                <hr>
                                                <h6 class="text-bold-600 mt-2"> Students:
                                                    <span class="">{{$proposal->name1}}, {{$proposal->name2}}</span>
                                                </h6>
                                                <h6 class="text-bold-600 mt-2"> Status
                                                    <span class="badge badge-pill badge-{{$proposal->status->class}} text-white">{{$proposal->status->name}}</span>
                                                </h6>
                                                <h6 class="text-bold-600 mt-2"> Submitted On:
                                                    <span class="text-danger">{{$proposal->dateTime}}</span>
                                                </h6>


                                                <!-- TABS -->
                                        <ul class="nav nav-tabs pt-2">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" href="#tabProblemStatement" aria-expanded="true"><i class="ft-info"></i> Problem Statement</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" href="#tabStudentsInfo" aria-expanded="false"><i class="ft-users"></i> Students Info</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="baseIcon-tab3" data-toggle="tab" aria-controls="tabIcon3" href="#tabSupervisors" aria-expanded="false"><i class="ft-compass"></i>Supervisors</a>
                                            </li>
                                             <li class="nav-item">
                                                <a class="nav-link" id="baseIcon-tab3" data-toggle="tab" aria-controls="tabIcon3" href="#tabPresentations" aria-expanded="false"><i class="ft-monitor"></i>Presentations</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="baseIcon-tab3" data-toggle="tab" aria-controls="tabIcon3" href="#tabReports" aria-expanded="false"><i class="ft-file-text"></i>Reports</a>
                                            </li>
                                        </ul>
                                         <div class="tab-content  pt-1">
                                            <div role="tabpanel" class="tab-pane active" id="tabProblemStatement" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                                                <p class="text-justify">{{$proposal->problem_statement}}</p>
                                                 <form action="{{route('download')}}" method="GET">
                                                @csrf
                                                <input type="hidden" name="path" value='{{$proposal->document}}'>
                                                <button type="submit" class="btn btn-outline-primary btn-min-width mr-1 mb-1">
                                                                <i class="la la-download"></i> Download Attachment
                                                </button>
                                                </form>
                                            </div>
                                            <div class="tab-pane" id="tabStudentsInfo" aria-labelledby="baseIcon-tab2">
                                                <p class="text-bold-600"> Student One</p>
                                                <h6 class="mt-1"> Name:
                                            <span class="text-dark">{{$proposal->name1}}</span>
                                        </h6>
                                        <h6 class="mt-1"> Email:
                                            <span class="text-dark">{{$proposal->email1}}</span>
                                        </h6>
                                        <h6 class="mt-1"> Registration No:
                                            <span class="text-dark">{{$proposal->reg_no1}}</span>
                                        </h6>
                                        @if($proposal->name2 != "")
                                        <hr>
                                         <p class="text-bold-600"> Student Two</p>
                                                <h6 class=" mt-1"> Name:
                                            <span class="text-dark">{{$proposal->name2}}</span>
                                        </h6>
                                        <h6 class=" mt-1"> Email:
                                            <span class="text-dark">{{$proposal->email2}}</span>
                                        </h6>
                                        <h6 class=" mt-1"> Registration No:
                                            <span class="text-dark">{{$proposal->reg_no2}}</span>
                                        </h6>
                                        @endif



                                            </div>
                                            <div class="tab-pane" id="tabSupervisors" aria-labelledby="baseIcon-tab3">

                                                @if($proposal->supervisor_name == "" && $proposal->cosupervisor_name == "")
                                                    <p class="p-4 text-center text-muted">No Supervisors Assigned</p>
                                                @else
                                                <p class="text-bold-600">Main Supervisor</p>
                                                <h6 class="mt-1"> Name:
                                            <span class="text-dark">{{$proposal->supervisor_name}}</span>
                                        </h6>
                                        <h6 class="mt-1"> Email:
                                            <span class="text-dark">{{$proposal->supervisor_email}}</span>
                                        </h6>
                                        <hr>
                                            <p class="text-bold-600">Co-Supervisor</p>
                                                <h6 class="mt-1"> Name:
                                            <span class="text-dark">{{$proposal->cosupervisor_name}}</span>
                                        </h6>
                                        <h6 class="mt-1"> Email:
                                            <span class="text-dark">{{$proposal->cosupervisor_email}}</span>
                                        </h6>




                                                @endif

                                            </div>

                                              <div role="tabpanel" class="tab-pane" id="tabPresentations" aria-expanded="true" aria-labelledby="baseIcon-tab4">
                                               <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Type</th>
                                                    <th>Date Submitted</th>
                                                    <th>Slides</th>
                                                    <th>Notes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($presentations as $pr)
                                                <tr>
                                                    <td>{{$pr->title}}
                                                    </td>
                                                    <td>
                                                        <span class="text-danger">
                                                            @if($pr->type == 'mid_term')
                                                                Mid term presentation
                                                            @endif
                                                            @if($pr->type == 'final')
                                                                Final presentation
                                                            @endif
                                                        </span>
                                                    </td>
                                                    <td>{{$pr->created_at->isoFormat('dddd Do MMMM YYYY')}}</td>
                                                    <td>
                                                        <div class="row">
                                                             <form action="{{route('download')}}" method="GET">
                                                        @csrf
                                                        <input type="hidden" name="path" value='{{$pr->document}}'>
                                                        <input type="submit" class="btn btn-info btn-sm" value="Download">
                                                    </form>
                                                        </div>
                                                      
                                                        
                                                    </td>
                                                    <td>{{$pr->notes}}</td>
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                            </div>

                                             <div role="tabpanel" class="tab-pane" id="tabReports" aria-expanded="true" aria-labelledby="baseIcon-tab4">
                                               <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Type</th>
                                                    <th>Date Submitted</th>
                                                    <th>Document</th>
                                                    <th>Notes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($reports as $r)
                                                <tr>
                                                    <td>{{$r->title}}
                                                    </td>
                                                    <td>
                                                        <span class="text-danger">
                                                            @if($r->type == 'project_proposal')
                                                                Project Proposal Report
                                                            @endif
                                                            @if($r->type == 'project_report')
                                                                Final Project Report
                                                            @endif
                                                        </span>
                                                    </td>
                                                    <td>{{$r->created_at->isoFormat('dddd Do MMMM YYYY')}}</td>
                                                    <td>
                                                        <div class="row">
                                                             <form action="{{route('download')}}" method="GET">
                                                        @csrf
                                                        <input type="hidden" name="path" value='{{$r->document}}'>
                                                        <input type="submit" class="btn btn-info btn-sm" value="Download">
                                                    </form>
                                                        </div>
                                                      
                                                        
                                                    </td>
                                                    <td>{{$r->notes}}</td>
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                            </div>
                                        </div>
                                    </div>





                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>


            <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel">Change Proposal Status</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
            </div>
            <form action="{{route('change_status')}}" method="post">
                @csrf
                <div class="modal-body">

                    <input type="hidden" name="id" value="{{$proposal->id}}">

                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Current Status</label>
                            <input type="text" class="form-control" readonly="" value="{{$proposal->status->name}}">
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">New Status</label>
                            <select class="select2 form-control" id="department" name="status_id" required="" >
                                <option disabled="" selected="" value="">Choose</option>
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}">{{$status->name}}</option>
                                @endforeach

                            </select>
                        </div>
                         <div class="form-group">
                            <label for="inputText3" class="col-form-label">Comment</label>
                            <textarea type="text" rows="2" name="comment" class="form-control" placeholder="Optional"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="supervisorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel">Allocate Supervisors</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
            </div>
            <form action="{{route('set_supervisors')}}" method="post">
                @csrf
                <div class="modal-body">

                    <input type="hidden" name="proposal_id" value="{{$proposal->id}}">
                     <div class="row">
                         <div class="form-group col-6">

                            <label for="inputText3" class="col-form-label">Main Supervisor<span class="text-danger">*</span></label><br>
                             <select class="select2 form-control" id="supervisor" name="supervisor_id" required="">
                                <option disabled="" selected="" value="">Select</option>
                                @foreach($supervisors as $supervisor)
                                    <option value="{{$supervisor->id}}">{{$supervisor->fullnames()}}</option>
                                @endforeach

                            </select>
                        </div>
                          <div class="form-group col-6">
                            <label for="inputText3" class="col-form-label">Email</label>
                            <input type="text" class="form-control" required="" id="supervisor_email" name="supervisor_email">
                        </div>
                    </div>

                     <div class="row">
                         <div class="form-group col-6">

                            <label for="inputText3" class="col-form-label">Co-Supervisor</label><br>
                              <select class="select2 form-control" id="cosupervisor" name="cosupervisor_id">
                                <option disabled="" selected="" value="">Select</option>
                                @foreach($supervisors as $supervisor)
                                    <option value="{{$supervisor->id}}">{{$supervisor->fullnames()}}</option>
                                @endforeach
                            </select>
                        </div>
                          <div class="form-group col-6">
                            <label for="inputText3" class="col-form-label">Email</label>
                            <input type="text" class="form-control" required="" id="cosupervisor_email" name="cosupervisor_email">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                    <button class="btn btn-primary" type="submit">Allocate</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel1" aria-modal="true" style="padding-right: 17px;">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="basicModalLabel1">Move Proposal to Trash</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>This proposal will nolonger be visible among your proposals. Are you sure you want to continue? </p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Cancel</button>
                                                                      
                                                             <a href="{{route('trash_proposal', $proposal->id)}}" class="btn btn-danger"> Move To Trash</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // $('#supervisor').select2();
        // $('#cosupervisor').select2();

        $("select#supervisor").change(function(){
            var supervisor = $(this).children("option:selected").val();
            console.log("Student 2 -> " + supervisor);

            $.ajax({
                url:"/get_supervisor_by_id/"+supervisor,
                success:function(data){
                    console.log(data)
                    $("#supervisor_email").val(data)
                }
            });
        });

         $("select#cosupervisor").change(function(){
            var supervisor = $(this).children("option:selected").val();
            console.log("Student 2 -> " + supervisor);

            $.ajax({
                url:"/get_supervisor_by_id/"+supervisor,
                success:function(data){
                    console.log(data)
                    $("#cosupervisor_email").val(data)
                }
            });


        });
    });


</script>

@endsection
