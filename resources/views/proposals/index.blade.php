@extends('layouts.master')

@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-4 col-12 mb-2">
        <h3 class="content-header-title">{{session('title')}}</h3>
    </div>
    <div class="content-header-right col-md-8 col-12">
        <div class="breadcrumbs-top float-md-right">
            <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item active">{{session('title')}}
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{$current_year->description}}</h4>



                                <div class="heading-elements">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        @foreach($years as $year)

                                                <a href="{{route('list_proposals', ['programme_id'=>$programme->id, 'year_id'=> $year->id])}}" class="btn btn-outline-info btn-year text-white btn-md @if($current_year->id == $year->id) active @endif">{{$year->name}}</a>
                                        @endforeach
                                    </div>
                                  

                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">


                                    <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                            <table id="datatable" class="table table-bordered">
                                            <thead>
                                                <tr role="row">
                                                    <th style="width: 5%">#</th>
                                                    <th style="width: 10%">STUDENT ONE</th>
                                                    <th style="width: 10%">STUDENT TWO</th>
                                                    <th style="width: 40%">TITLE</th>
                                                    <th style="width: 10%">STATUS</th>
                                                    <th style="width: 10%">MAIN SUPERVISOR</th>
                                                    <th style="width: 10%">CO-SUPERVISOR</th>
                                                    <th style="width: 5%">DOCUMENT</th>
                                                    <th>LECTURER</th>
                                                    <th>COMMENTS</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $counter = 1 @endphp
                                                @foreach($proposals as $pr)
                                                    <tr>
                                                        <td>{{$counter++}}</td>

                                                        <td>{{$pr->name1}},<br>{{$pr->reg_no1}},<br>{{$pr->email1}}</td>
                                                        <td>{{$pr->name2}},<br>{{$pr->reg_no2}},<br>{{$pr->email2}}</td>
                                                        <td><a class="text-primary " href="{{route('show_proposal', ['programme_id' => $pr->programme_id, 'id' => $pr->id])}}"><u>{{$pr->title}}</u></a></td>
                                                        <td>
                                                            <span class="badge badge-pill badge-{{$pr->status->class}} text-white">{{$pr->status->name}}</span></td>
                                                        <td>{{$pr->supervisor_name}}</td>
                                                        <td>{{$pr->cosupervisor_name}}</td>
                                                          <td>
                                                                <form action="{{route('download')}}" method="GET">
                                                                @csrf
                                                                <input type="hidden" name="path" value='{{$pr->document}}'>
                                                                <input type="submit" class="btn btn-info btn-sm" value="Download">
                                                                </form>
                                                        </td>
                                                        <td>{{$pr->lecturer_name}}</td>
                                                        <td>{{$pr->comments}}</td>
                                                    </tr>
                                                    @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
                jQuery(document).ready(function(){
                    $("#datatable").DataTable({
                        columnDefs: [{ orderable: false, targets: [7] }],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf'
                        ]
                    });
                });

                $("select#year-select").change(function(){
                    var year = $(this).children("option:selected").val();
                    var programme_id = $('#programme-id').val()
                    console.log('programme_id -> '+ programme_id)

                    // $.ajax({
                    //     url:"/get_proposals_by_year/"+programme_id+"/"+year,
                    //     success:function(data){
                    //         //console.log(data)
                    //         // $('#datatable').empty();
                    //         // $.each(data, function(i, item) {
                    //         //     $('<tr>').html("<th>" + i++ + "</th><th>" + data[i][1]  + "</th><th>" + data[i][2]  + "</th><th>" + data[i][3]  + "</th>").appendTo('#datatable');
                    //         // });

                    //     }
                    // });
                });

                function deleteProgramme(id){
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You will not be able to recover the supervisor's details!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#6610f2',
                        cancelButtonColor: '#607D8B',
                        confirmButtonText: 'Yes, delete it!',
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value){
                            $.ajax({
                                url: '/supervisors/delete/'+id,
                                type: "DELETE",
                                data:{ _token: "{{csrf_token()}}" }
                            }).done(function() {

                                swal({
                                    title: "Deleted",
                                    text: "Supervisor has been successfully deleted",
                                    type: "success",
                                    confirmButtonColor: '#6610f2',

                                }).then(function() {
                                    location.href = '/supervisors';
                                });
                            });

                        }


                    });

                }

            </script>


@endsection
