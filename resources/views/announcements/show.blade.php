@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/announcements">Announcements</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2><strong>{{$an->title}}</strong></h2>
                                    <div class="heading-elements">
                                      
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">

                                        <p>{!! $an->description !!} </p>
                                      
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){});
</script>


@endsection