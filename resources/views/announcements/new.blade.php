@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/announcements">Announcements</a>
                                </li>
                                <li class="breadcrumb-item active">{{session('title')}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{session('title')}}</h4>
                                    <div class="heading-elements">
                                      
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form needs-validation" method="post" action="/announcements">
                                            @csrf
                                            <div class="form-body">
                                                <input type="hidden" name="id" value="{{$an->id}}">                                             
                                                <div class="form-group row col-8">
                                                    <label for="schoolName">Subject<span class="text-danger">*</span></label>
                                                    <input type="text" id="schoolName" class="form-control" placeholder="Add subject" name="title" required value="{{$an->title}}">
                                                </div>
                                                <div class="form-group row col-8">
                                                    <label for="description">Body<span class="text-danger"></span></label><br>
                                                    
                                                    <textarea rows="100" id="description" class="form-control summernote" placeholder="Add body" name="description" required>{{$an->description}}</textarea>
                                                </div>                                                   
                                                
                                                
                                            </div>

                                            <div class="form-actions">
                                                <a href="/announcements" class="btn btn-danger mr-1 white">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
         $('#description').summernote({
            height:300
        });
    });
</script>


@endsection