@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Announcements
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">List of announcements</h4>
                                    <div class="heading-elements">
                                       <a href="/announcements/new" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-plus font-medium-3"></i>New</a> 
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                    	@if(Session::has('message'))
                                    	<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            {{ Session::get('message') }}
                                        </div>
										@endif
                                        
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                            	<div class="row">
                                            		<div class="col-sm-12">

                                            	<table id="datatable" class="table table-bordered">
                                                <thead>
                                                	<tr>
	                                                	<td>#</td>
	                                                	<td>TITLE</td>
	                                                	<td>DATE ADDED</td>
	                                                	<td>ACTIONS</td>
                                                	</tr>
                                                </thead>
                                                <tbody>
                                                	@php $counter = 1 @endphp
                                                	@foreach($ans as $a)
	                                                <tr>
	                                                	<th>{{$counter++}}</th>
	                                                	<th><a href="/announcements/show/{{$a->id}}" class="text-info"><u>{{$a->title}}</u></a></th>
	                                                	<th>{{$a->date}}</th>
	                                                	<th>
	                                                		<a href="/announcements/edit/{{$a->id}}" class="btn btn-icon btn-info btn-sm"><i class="ft-edit"></i></a>
	                                                		<a href="#" onclick="deleteSchool({{$a->id}})" class="btn btn-icon btn-danger btn-sm"><i class="ft-trash-2"></i>
	                                                		</a>
	                                                		

	                                                	</th>

	                                                	
	                                                </tr>
	                                                @endforeach 
                                        		</tbody>
                                               
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->

            </div>

            <script type="text/javascript">
            	jQuery(document).ready(function(){
					$("#datatable").DataTable({
						columnDefs: [{ orderable: false, targets: [3] }],
					});
            	});

            	function deleteSchool(id){
            		Swal.fire({
				        title: 'Are you sure?',
				        text: 'You will not be able to recover this announcement!',
				        type: 'warning',
				        showCancelButton: true,
				        confirmButtonColor: '#6610f2',
				        cancelButtonColor: '#607D8B',
				        confirmButtonText: 'Yes, delete it!',
				        closeOnConfirm: false
				    }).then((result) => {
				    	if(result.value){
				    		$.ajax({
					            url: '/announcements/delete/'+id,
					            type: "DELETE",
					            data:{ _token: "{{csrf_token()}}" }
					        }).done(function() {

					            swal({
					                title: "Deleted", 
					                text: "Announcement has been successfully deleted", 
					                type: "success",
					                confirmButtonColor: '#6610f2',

					            }).then(function() {
					                location.href = '/announcements';
					            });
					        });

				    	}

				       
				    });

            	}
            	
            </script>


@endsection