@extends('layouts.master')

@section('content')

<div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">{{session('title')}}</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Account
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

<div class="content-body">
      <section id="configuration">
                    <div class="row">
                        <div class="col-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title primary">Profile Details</h4>
                                     
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <ul style="list-style-type:none;">
                                            <li>
                                                <i class="ft-user"></i><strong class="pl-1">First Name:</strong>
                                                {{$user->fname}}
                                            </li>
                                            <li class="mt-1">
                                                <i class="ft-user"></i><strong class="pl-1">Last Name:</strong>
                                                {{$user->lname}}
                                            </li>
                                            <li class="mt-1">
                                                <i class="ft-user-plus"></i><strong class="pl-1">Other Names:</strong>
                                                {{$user->other_name}}
                                            </li>
                                            <li class="mt-1">
                                                <i class="ft-mail"></i><strong class="pl-1">Email:</strong>
                                                {{$user->email}}
                                            </li>
                                            <li class="mt-1">
                                                <i class="ft-type"></i><strong class="pl-1">Title:</strong>
                                                {{$user->staff_bio->title}}
                                            </li>
                                            <li class="mt-1">
                                                <i class="ft-map-pin"></i> <strong class="pl-1">Room:</strong>
                                                {{$user->staff_bio->office}}
                                            </li>
                                           
                                        </ul>

                                   <!--      <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" value="" id="checkbox" name="is_individual_project">
                            <label class="form-check-label primary" for="checkbox">
                                Click to change your password
                            </label>
                        </div> -->

                                       
                                        
                                       
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title primary">Edit Account</h4>
                                     
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                     <h6 class="text-white">{{ session()->get('message') }}</h6>
                </div>
                   
                @endif
                @if($errors->any())
               
                <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    @foreach($errors->all() as $error)
                     <h6 class="text-white">{{$error}}</h6>
                    @endforeach
                </div>
                @endif
                                            <form class="form-horizontal" method="POST" action="{{route('update_account')}}">
                    @csrf
                    <input type="hidden" name="role" value="3">
                    <div class="row">
                        <div class="col-6">
                            <label>First Name</label>

                            <fieldset class="form-group position-relative">
                                <input type="text" class="form-control" id="user-name" value="{{$user->fname}}"  autocomplete="off" name="first_name">
                             
                                 @error('first_name')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                        <div class="col-6">
                            <label>Last Name</label>
                             <fieldset class="form-group position-relative">
                                <input type="text" class="form-control" id="user-name" value="{{$user->lname}}"  name="last_name">
                                
                                 @error('last_name')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                       
                        
                    </div>

                    <div class="row">
                         <div class="col-6">
                            <label>Other Name</label>
                            <fieldset class="form-group position-relative">
                                <input type="text" class="form-control" id="user-name" value="{{$user->other_name}}" autocomplete="off" name="other_name">
                               

                            </fieldset>
                        </div>
                        <div class="col-6">
                             <label>Email Address</label>
                            <fieldset class="form-group position-relative">
                                <input type="email" class="form-control" id="user-email" value="{{$user->email}}" name="email">
                                
                                @error('email')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>  
                        </div>

                        
                    </div>
                    <div class="row">
                     
                        <div class="col-6">
                            <label>Title</label>
                            <fieldset class="form-group position-relative">
                                <input type="text" class="form-control" id="user-name" value="{{$user->staff_bio->title}}" autocomplete="off" name="title">
                               
                                @error('title')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                        <div class="col-6">
                            <label>Office Location</label>
                             <fieldset class="form-group position-relative">
                                <input type="text" class="form-control" id="user-name" value="{{$user->staff_bio->office}}"  name="office">
                                
                                @error('office')
                                    <span id="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                    </div>
                     <div class="text-right col-12">
                        
                                           <button type="submit" class="btn btn-success ">
                             Update Account
                        </button>
                                        </div>
                   


                        
                    
                </form>
                          


                                       
                                        
                                       
                                    </div>
                                </div>
                            </div>



                            <!-- PASSWORD CARD -->


                                <div class="card" id="password-container">
        <div class="card-header">
            <h4 class="card-title text-primary" id="basic-layout-form">Change Password</h4>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                
                <form method="POST" action="{{ route('change_password') }}">
                @csrf
                <div class="row">
                <div class="col-4">
                                            <label>Old Password</label>

                                            <fieldset class="form-group ">
                                                 <input id="password" type="password" class="form-control" name="oldpassword" required autocomplete="new-password">
                                                
                                                
                                            </fieldset>
                                        </div>
                                    <div class="col-4">
                                            <label>New Password</label>

                                            <fieldset class="form-group">
                                                 <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                                                
                                            </fieldset>
                                        </div>
                                         <div class="col-4">
                                            <label>Confirm Password</label>

                                            <fieldset class="form-group">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                                
                                                  
                                            </fieldset>
                                        </div>
                                    </div>

                                         <div class="text-right col-12">
                        
                                            <button type="submit" class="btn btn-success">
                                                Change Password
                                            </button>
                                        </div>
                </form>
                
            </div>
        </div>

    </div>






                        </div>
                    </div>
                </section>          

</div>

<script type="text/javascript">
    // jQuery(document).ready(function(){
    //     $('#checkbox').change(function() {
    //         if(this.checked) {
    //             $('#password-container').show();
    //         }else{
    //             $('#password-container').hide();
    //         }
    //     });       
    // });
</script>
@endsection