@extends('layouts.master')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Welcome {{Auth::user()->fname}}</h3>
        </div>

    </div>
<div class="content-body">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-12">
            <div class="row">
                <div class="col-6">
                    <a href="{{route('supervisor_proposals')}}">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1">Proposals</span>
                                        <h1 class="success mb-0">{{$proposals_count}}</h1>
                                    </div>
                                    <div class="align-self-top">
                                        <i class="icon-layers icon-opacity success font-large-4"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>

                </div>
                <div class="col-6">
                    <a href="{{route('list_projects',0)}}">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1">Projects</span>
                                        <h1 class="warning mb-0">{{$projects_count}}</h1>
                                    </div>
                                    <div class="align-self-top">
                                        <i class="icon-screen-desktop icon-opacity warning font-large-4"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>

                </div>

                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-danger">Recent Proposals</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">

                            </div>
                        </div>
                        <div class="card-content pb-4">
                            @if(count($proposals) > 0)
                                @foreach($proposals as $proposal)
                                    <div class="card">
                                        <div class="row px-2">
                                            <div class="col-1">

                                                <img src="/images/project_icon.svg" style="height: 48px; background: #F4F5FA; padding: 14px; border-radius: 5px">

                                            </div>


                                            <div class="col-11">
                                                <h4><a class="text-muted" href="{{route('show_proposal_details', ['programme_id'=> $proposal->programme_id, 'id'=>$proposal->id])}}"><strong>{{$proposal->title}}</strong></a></h4>
                                                @if($proposal->is_individual_project)
                                                    <span class="text-primary">{{$proposal->name1}}</span><br>
                                                @else
                                                    <span class="text-primary">{{$proposal->name1}} | {{$proposal->name2}}</span><br>
                                                @endif

                                                <span class="font-small-2">{{$proposal->dateTime}}</span>
                                            </div>


                                        </div>

                                    </div>
                                @endforeach
                            @else
                                <div class="text-center" style="height: 200px"> <span class="mt-4">No proposals available</span></div>
                            @endif

                        </div>
                    </div>

                </div>
            </div>

        </div>
{{--        <div class="col-xl-4 col-lg-4 col-12">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">--}}
{{--                    <h4 class="card-title text-danger">Notice Board</h4>--}}
{{--                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>--}}
{{--                    <div class="heading-elements">--}}
{{--                        <ul class="list-inline mb-0">--}}
{{--                            <li><a href="{{route('list_announcements')}}" class="btn btn-glow btn-round btn-bg-gradient-x-red-pink btn-md">More</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="card-content pb-4">--}}

{{--                    <ul class="list-group list-group-flush">--}}
{{--                        @if(count($announcements) > 0)--}}
{{--                            @foreach($announcements as $an)--}}
{{--                                <li class="list-group-item">--}}
{{--                                    <a href="{{ route('show_announcement', ['id'=>$an->id]) }}">--}}
{{--                                        <span class="font-medium-1 text-muted">{{$an->title}} </span> <br>--}}
{{--                                        <small class="text-primary"><em>{{$an->date}}</em></small>--}}

{{--                                    </a>--}}


{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        @else--}}
{{--                            <div class="text-center" style="height: 200px"> <span class="mt-4">No announcements available</span></div>--}}
{{--                        @endif--}}

{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
    </div>

</div>

@endsection
