@extends('layouts.master')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">{{session('title')}}</h3>
        </div>
        <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
                <div class="breadcrumb-wrapper mr-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active">{{session('title')}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                           <div class="card-header">
                            <h4 class="card-title">Student Grades</h4>
                            <div class="heading-elements">
                               <a href="#" data-toggle="modal" data-target="#modalStudents" class="btn btn-success btn-min-width box-shadow mr-1 mb-1 white"><i class="la la-plus font-medium-3"></i>Grade</a>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">


                                <div class="table-responsive">
                                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <table id="datatable" class="table table-bordered">
                                                     <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Type</th>
                                                    <th>Submitted By</th>
                                                    <th>Proposal</th>
                                                    <th>Date Submitted</th>

                                                    <th>SLIDES</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </section>
        <!--/ Zero configuration table -->


<div class="modal fade text-left show" id="modalStudents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title text-text-bold-600"></label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('start_grading')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <label><h4>Select the student you want to grade</h4> </label>
                        <div class="form-group">
                             <select class="select2 form-control mt-2" id="student2" name="student_id" 
                             style="width:450px" required="">
                                <option disabled="" selected="" value="">Select</option>
                                @foreach($students as $student)
                                    @if(Auth()->id() != $student->id)
                                        <option value="{{$student->id}}">{{$student->fullnames()}}</option>
                                    @endif
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-secondary " data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-success " value="Continue">
                    </div>
                </form>
            </div>
        </div>
    </div>


    </div>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            // $("#student2").select2();
          
        });
    </script>


@endsection
