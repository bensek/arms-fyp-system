### Final Year Submission and Review System

This system allows students at CEDAT to submit their project proposals and have them reviewed and approved by project coordinators.

It is built in Laravel, a PHP web framework and MySQL.

### Steps to setup up locally

##### Step 1

Clone the project to a folder on your computer


##### Step 2

Install XAMPP on your computer, it will add PHP, Apache Server and MySQL. You can download XAMMP from [here.](https://www.apachefriends.org/download.html)

##### Step 3

Install Composer. This package allows you install laravel and other php libraries. You can install composer from their [website.](https://getcomposer.org/doc/00-intro.md)

After it has installed, open a terminal in the root of the folder where the project has been cloned.

Run the command
```
composer update
```
This command installs all the necessary packages for the project found in package.json

Now run the command that starts the server 
```
php artisan serve
```
But ensure that Apache and MySQL are running in XAMPP

##### Step 4

Add a .env file to the root folder of the project and add the following code. 

```
APP_NAME="CEDAT FYP SYSTEM"
APP_ENV=local
APP_KEY=base64:ZBpKJRwTz2yGDdtJ3EWKpuTgWAGKRYSKBm8ocfQVWR4=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=fypsystem
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.googlemail.com
MAIL_PORT=465
MAIL_USERNAME=mbcug20@gmail.com
MAIL_PASSWORD=mbcug@2020
MAIL_ENCRYPTION=ssl
MAIL_FROM_ADDRESS="bensek73@gmail.com"
MAIL_FROM_NAME="Cedat FYP System"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

```

##### Step 5

Configure MYSQL. This is done in the .env file you created in the above step. 

Add the port on which MySQL is running e.g 3306.
Add the name of the database you will create manually e.g fypsystem and add the username and root to access the MySQL.

##### Step 6

Run the migrations to create the necessary tables.
In your terminal run the command

```
php artisan migrate
```

