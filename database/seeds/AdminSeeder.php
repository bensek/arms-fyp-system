<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'admin@gmail.com')->first();
        if($user == null){
            $user = new User;
        }
        
        $user->fname = "Super";
        $user->lname = "Administrator";
        $user->other_name = "";
        $user->email = "admin@gmail.com";
        //$user->department_id = $request->department;
        $user->password = Hash::make("Admin@256!");
        $user->verified = 1;
        $user->is_email_verified = 1;

        // Attach role
        $role = Role::where('name', 'admin')->firstOrFail();
        $user->assignRole($role);


        $user->save();
    }
}
