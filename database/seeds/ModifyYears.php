<?php

use Illuminate\Database\Seeder;
use App\Proposal;
use App\Project;
use App\Presentation;
use App\Report;
use App\User;

class ModifyYears extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = User::where('academic_year_id', 3)->get();
    	foreach($users as $user){
    		$user->academic_year_id = 1;
    		$user->save();
    	}

    	$projects = Project::all();
    	foreach($projects as $p){
    		$p->academic_year_id = 1;
    		$p->save();
    	}

    	$presentations = Presentation::where('academic_year_id', 3)->get();
    	foreach($presentations as $p){
    		$p->academic_year_id = 1;
    		$p->save();
    	}


        
    }
}
