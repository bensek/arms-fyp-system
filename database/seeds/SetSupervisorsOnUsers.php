<?php

use Illuminate\Database\Seeder;
use App\Proposal;
use App\User;

class SetSupervisorsOnUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proposals = Proposal::get();
        foreach($proposals as $p){
        	if($p->std_id1){
        		$user = User::findOrFail($p->std_id1);
        		$user->supervisor_id = $p->supervisor_id;
        		$user->cosupervisor_id = $p->cosupervisor_id;
        		$user->save();
        	}
        	if($p->std_id2){
        		$user = User::findOrFail($p->std_id2);
        		$user->supervisor_id = $p->supervisor_id;
        		$user->cosupervisor_id = $p->cosupervisor_id;
        		$user->save();
        	}
        }
    }
}
