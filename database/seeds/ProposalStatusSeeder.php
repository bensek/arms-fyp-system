<?php

use Illuminate\Database\Seeder;
use App\ProposalStatus;
class ProposalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = new ProposalStatus;
        $a->name = "Pending";
        $a->description = "Not yet reviewed on";
        $a->class = "warning";
        $a->save();

        $b = new ProposalStatus;
        $b->name = "Under Review";
        $b->description = "Started to review";
        $b->class = "info";
        $b->save();

        $c = new ProposalStatus;
        $c->name = "Approved";
        $c->description = "Successfully accepted proposal";
        $c->class = "success";
        $c->save();

        $d = new ProposalStatus;
        $d->name = "Rejected";
        $d->description = "Proposal does not meet requirements";
        $d->class = "danger";
        $d->save();
    }
}
