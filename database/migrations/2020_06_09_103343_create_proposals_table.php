<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('programme_id');
            $table->foreign('programme_id')->references('id')->on('programmes')->onUpdate('cascade');
            $table->unsignedBigInteger('std_id1');
            $table->foreign('std_id1')->references('id')->on('users')->onUpdate('cascade');
            $table->unsignedBigInteger('std_id2');
            $table->foreign('std_id2')->references('id')->on('users')->onUpdate('cascade'); 
            $table->string('name1');
            $table->string('name2');
            $table->string('reg_no1');
            $table->string('reg_no2');
            $table->string('email1');
            $table->string('email2');
            $table->text('title');
            $table->text('problem_statement');
            $table->boolean('lecturer')->default(false);
            $table->string('lecturer_name')->nullable();
            $table->string('document');
            $table->string('comments')->nullable();
            $table->string('dateTime')->nullable();
            $table->unsignedBigInteger('proposal_status_id');
            $table->foreign('proposal_status_id')->references('id')->on('proposal_statuses')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
