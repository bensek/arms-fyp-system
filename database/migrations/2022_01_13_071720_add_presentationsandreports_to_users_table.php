<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPresentationsandreportsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('partner_id')->nullable();
            $table->unsignedBigInteger('proposal_id')->nullable();
            $table->unsignedBigInteger('presentation1_id')->nullable();
            $table->unsignedBigInteger('presentation2_id')->nullable();
            $table->unsignedBigInteger('report1_id')->nullable();
            $table->unsignedBigInteger('report2_id')->nullable();


            $table->foreign('partner_id')->references('id')->on('users');
            $table->foreign('proposal_id')->references('id')->on('proposals');
            $table->foreign('presentation1_id')->references('id')->on('presentations');
            $table->foreign('presentation2_id')->references('id')->on('presentations');
            $table->foreign('report1_id')->references('id')->on('reports');
            $table->foreign('report2_id')->references('id')->on('reports');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
