<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/emails', 'HomeController@emails');
Route::get('/', function () {
    return redirect('/login');
});

// Auth::routes(['verify' => true]);
Route::get('/register/student', 'Auth\RegisterController@student_form')->name('register_student');
Route::get('/register/supervisor', 'Auth\RegisterController@supervisor_form')->name('register_lecturer');
Route::post('/register_student', 'Auth\RegisterController@register_student');
Route::post('/register_supervisor', 'Auth\RegisterController@register_supervisor');

Route::get('account/verify/{token}', 'UserController@verifyAccount')->name('user.verify'); 

Auth::routes();


// Route::group(['middleware' => ['auth', 'is_verify_email']], function(){
Route::group(['middleware' => ['auth']], function(){

	// GENERAL
	Route::get('/download', 'HomeController@download')->name('download');
	Route::post('/change_password', 'UserController@change_password')->name('change_password');
    Route::get('/dashboard', 'DashboardController@get_user_dashboard')->name('dashboard');//->middleware('is_verify_email');
	Route::get('/requires_approval', 'UserController@requires_approval')->name('requires_approval');

	// Coordinator Routes
	Route::get('/home', 'DashboardController@co_dashboard')->name('coordinator_dashboard');
	Route::get('/account', 'UserController@account')->name('account');
	Route::post('/account', 'UserController@update_account')->name('update_account');

	// PRESENTATIONS
	Route::get('/presentations/{type}/{year_id}', 'PresentationController@index')->name('list_presentations')->middleware('approved');

	// REPORTS
	Route::get('/reports/{type}/{year_id}', 'ReportController@index')->name('list_reports')->middleware('approved');

	// PANELS
	Route::get('/panels/{type}', 'PanelController@index')->name('list_panels')->middleware('approved');
	Route::get('/panels/{type}/new', 'PanelController@new')->name('new_panels');
	Route::post('/panels', 'PanelController@store')->name('store_panels');
	Route::get('/panels/{type}/edit/{id}', 'PanelController@edit')->name('edit_panels');
	Route::post('/panels/update', 'PanelController@update')->name('update_panels');
	Route::delete('/panels/delete/{id}', 'PanelController@delete')->name('delete_panels');
	Route::get('/panels/{type}/show/{id}', 'PanelController@show')->name('show_panels');
	Route::get('/panels/allocate/{id}', 'PanelController@allocate')->name('allocate_panels');
	Route::post('/panels/allocate', 'PanelController@store_allocated_students')->name('allocate_students');

	
	// ANNOUNCEMENTS
	Route::get('/announcements', 'AnnouncementController@index')->name('list_announcements');
	Route::get('/announcements/new', 'AnnouncementController@new')->name('new_announcement');
	Route::post('/announcements', 'AnnouncementController@store')->name('store_announcement');
	Route::get('/announcements/edit/{id}', 'AnnouncementController@edit')->name('edit_announcement');
	Route::get('/announcements/show/{id}', 'AnnouncementController@show')->name('show_announcement');
	Route::delete('/announcements/delete/{id}', 'AnnouncementController@delete')->name('delete_announcement');

	// STUDENTS
	Route::get('/students', 'StudentController@all')->name('list_all_students');
	Route::get('/students/{id}/{name}/{year_id}', 'StudentController@index')->name('list_students');
	Route::get('/students/show/{id}', 'StudentController@show')->name('show_student');

	// RESET PASSWORD
	Route::get('/users/resetpassword', 'UserController@reset_password')->name('reset_password');
	Route::post('/users/resetpassword', 'UserController@save_password')->name('reset_password_new');

	// GRADES
	Route::get('/grades/{id}/{year_id}', 'GradeController@index')->name('list_grades')->middleware('approved');
	Route::get('/grades/{id}/{year_id}/new', 'GradeController@new')->name('grade_student');
	Route::post('/grades', 'GradeController@store')->name('store_grade');
	Route::get('/grades/add', 'GradeController@add')->name('add_grade');
	Route::get('/grades/send_email', 'GradeController@send_email')->name('send_email');


	// SUPERVISORS
	Route::get('/supervisors', 'SupervisorController@index')->name('list_supervisors');
	Route::get('/supervisors/approve', 'SupervisorController@approve')->name('list_approvals');
	Route::get('/supervisors/approving/{id}', 'SupervisorController@approve_supervisor')->name('approve_supervisor');
	Route::get('/supervisors/new', 'SupervisorController@new')->name('new_supervisor');
	Route::post('/supervisors/store', 'SupervisorController@store')->name('store_supervisor');
	Route::get('/supervisors/show/{id}', 'SupervisorController@show')->name('show_supervisor');
    Route::get('/supervisors/edit/{id}', 'SupervisorController@edit')->name('edit_supervisor');
    Route::post('/supervisors/update', 'SupervisorController@update')->name('update_supervisor');
    Route::delete('/supervisors/delete/{id}', 'SupervisorController@delete')->name('delete_supervisor');

	// IMPORT EXCEL SHEETS
	Route::get('/supervisors/import', 'SupervisorController@fileImportExcel')->name('import_supervisors');
	Route::post('/supervisors/import', 'SupervisorController@fileImport')->name('import_supervisors');

	// PROPOSALS
	Route::get('/proposals/{programme_id}/{year_id}', 'ProposalController@index')->name('list_proposals');
	Route::get('/proposals/show/{programme_id}/{id}', 'ProposalController@show')->name('show_proposal');
	Route::post('/proposal/change_status', 'ProposalController@change_status')->name('change_status');
	Route::get('/get_proposals/{status}/{year_id}', 'ProposalController@pending_proposals')->name('get_proposals');
	Route::get('/underreview_proposals', 'ProposalController@underreview_proposals')->name('underreview_proposals');
	Route::get('/approved_proposals', 'ProposalController@approved_proposals')->name('approved_proposals');
	Route::post('/get_proposals_by_year', 'ProposalController@get_proposals_by_year')->name('get_proposals_by_year');
	Route::post('/proposal/set_supervisors', 'ProposalController@set_supervisors')->name('set_supervisors');
	Route::get('/proposal/move_to_trash/{id}', 'ProposalController@move_to_trash')->name('trash_proposal');


	// CATEGORIES
	Route::get('/categories', 'CategoryController@index')->name('list_categories');
	Route::get('/categories/new', 'CategoryController@new')->name('new_category');
	Route::post('/categories/store', 'CategoryController@store')->name('store_category');
	Route::get('/categories/edit/{id}', 'CategoryController@edit');
	Route::delete('/categories/delete/{id}', 'CategoryController@delete');

	// PROJECTS
	Route::get('/projects/{year_id}', 'ProjectController@index')->name('list_projects');
	Route::get('/projects/{year_id}/new', 'ProjectController@new')->name('new_project');
	Route::post('/projects/store', 'ProjectController@store')->name('store_project');
	Route::get('/projects/{year_id}/edit/{id}', 'ProjectController@edit')->name('edit_project');
    Route::get('/projects/{year_id}/show/{id}', 'ProjectController@show')->name('show_project');
    Route::delete('/projects/delete/{id}', 'ProjectController@delete');
	Route::post('/projects/comment', 'ProjectController@storeComment')->name('comment_project');
	Route::get('/projects/comment/delete/{id}', 'ProjectController@deleteComment')->name('delete_comment');
	Route::get('/projects/comment/edit/{id}', 'ProjectController@editComment')->name('edit_comment');
	Route::post('/projects/comment/update', 'ProjectController@updateComment')->name('update_comment');
	
	// ACADEMIC YEARS
	Route::get('/academic_years', 'AcademicYearController@index')->name('list_academic_years');
	Route::get('/academic_years/new', 'AcademicYearController@new')->name('new_academic_year');
	Route::post('/academic_years', 'AcademicYearController@store')->name('store_academic_year');
	Route::get('/academic_years/edit/{id}', 'AcademicYearController@edit')->name('edit_academic_year');
	Route::delete('/academic_years/delete/{id}', 'AcademicYearController@delete')->name('delete_academic_year');


// Admin Routes

	Route::prefix('admin')->group(function(){
		Route::get('/dashboard', 'DashboardController@admin_dashboard')->name('admin_dashboard');

		Route::get('/schools', 'SchoolController@index')->name('list_schools');
		Route::get('/schools/new', 'SchoolController@new')->name('new_schools');
		Route::post('/schools', 'SchoolController@store')->name('store_schools');
		Route::get('/schools/edit/{id}', 'SchoolController@edit')->name('edit_schools');
		Route::delete('/schools/delete/{id}', 'SchoolController@delete')->name('delete_school');

		Route::get('/departments', 'DepartmentController@index')->name('list_departments');
		Route::get('/departments/new', 'DepartmentController@new')->name('new_departments');
		Route::post('/departments', 'DepartmentController@store')->name('store_departments');
		Route::get('/departments/edit/{id}', 'DepartmentController@edit')->name('edit_departments');
		Route::delete('/departments/delete/{id}', 'DepartmentController@delete')->name('delete_departments');

		Route::get('/programmes', 'ProgrammeController@index')->name('list_programmes');
		Route::get('/programmes/new', 'ProgrammeController@new')->name('new_programmes');
		Route::post('/programmes', 'ProgrammeController@store')->name('store_programmes');
		Route::get('/programmes/edit/{id}', 'ProgrammeController@edit')->name('edit_programmes');
		Route::delete('/programmes/delete/{id}', 'ProgrammeController@delete');

		Route::get('/coordinators', 'SupervisorController@index')->name('list_coordinators');
		Route::get('/coordinators/new', 'SupervisorController@new')->name('new_coordinator');
		Route::post('/coordinators/store', 'SupervisorController@store')->name('store_coordinator');
		Route::get('/coordinators/show/{id}', 'SupervisorController@show')->name('show_coordinator');

		Route::get('/logs', 'LogController@index')->name('list_logs');
		Route::get('/logs/show/{id}', 'LogController@show')->name('show_logs');
		
	});

	// Student Routes

	Route::group(['prefix' => 'student'], function(){
		Route::get('/home', 'StudentPortalController@dashboard')->name('student_dashboard');
		Route::get('/submit_proposal/{id}','StudentPortalController@submit_proposal')->name('submit_proposal');
		Route::post('/store_proposal','StudentPortalController@store_proposal')->name('store_proposal');
		Route::get('/account/proposal/{id}','StudentPortalController@show_proposal')->name('proposal_details');
Route::post('/delete_proposal/{id}','StudentPortalController@delete_proposal')->name('delete_proposal');
		Route::get('/account', 'StudentPortalController@get_profile')->name('get_profile');
		Route::get('/account/edit', 'StudentPortalController@edit_profile')->name('edit_student_profile');
		Route::post('/account', 'StudentPortalController@update_student')->name('update_student_profile');
		Route::get('/announcements', 'StudentPortalController@get_announcements')->name('get_announcements');
		Route::get('/announcements/{id}', 'StudentPortalController@display_announcement')->name('display_announcement');

		// Projects
		Route::get('/projects', 'StudentPortalController@get_projects')->name('get_projects');
		Route::get('/projects/{id}', 'StudentPortalController@display_project')->name('display_project');
		Route::get('/categories/{id}/projects', 'StudentPortalController@get_projects_by_category')->name('category_projects');


		// Presentations
		Route::post('/submit_presentation', 'StudentPortalController@submit_presentation')->name('submit_presentation');
		Route::post('/presentations/delete', 'StudentPortalController@delete_presentation')->name('delete_presentation');


		// Reports
		Route::post('/upload_report', 'StudentPortalController@upload_report')->name('upload_report');
		Route::post('/reports/delete', 'StudentPortalController@delete_report')->name('delete_report');
	});


	// Supervisor Routes
    Route::get('/supervisor/dashboard', 'SupervisorPortalController@dashboard')->name('supervisor_dashboard');
    Route::get('/supervisor/proposals', 'SupervisorPortalController@get_proposals')->name('supervisor_proposals')->middleware('approved');
    Route::get('/supervisor/proposals/{id}', 'SupervisorPortalController@show_proposal')->name('show_proposal_details');
	Route::get('/presentations', 'SupervisorPortalController@get_presentations')->name('supervisor_presentations')->middleware('approved');

	Route::get('/grades_sus', 'SupervisorPortalController@get_grades')->name('supervisor_grades')->middleware('approved');
	Route::post('/start_grading', 'GradeController@start_grading')->name('start_grading');
	
	Route::get('/grades/new/{id}', 'GradeController@new')->name('new_grade');

//    Route::get('/projects', 'ProjectController@index')->name('supervisor_projects');

//	Route::group(['prefix' => 'supervisor'], function(){
//		Route::get('/dashboard', 'SupervisorPortalController@dashboard')->name('supervisor_dashboard');
//        Route::get('/proposals', 'ProjectController@index')->name('supervisor_proposals');
//        Route::get('/projects', 'ProjectController@index')->name('supervisor_projects');
//
//
//    });

	// AJAX Routes
	Route::get('/get_student_by_id/{id}', 'UserController@get_student_by_id');
	Route::get('/get_supervisor_by_id/{id}', 'UserController@get_supervisor_by_id');
	Route::get('/get_proposal/{id}', 'ProposalController@get_proposal_ajax');

});


Route::group(['middleware' => 'auth'], function(){
// USER MANUAL

Route::get('/user-manual', 'UserManualController@index')->name('index.manual');
Route::get('/user-manual/lecturer', 'UserManualController@lecturer')->name('manual.lecturer');
Route::get('/user-manual/student', 'UserManualController@student')->name('manual.student');
Route::get('/user-manual/coordinator', 'UserManualController@coordinator')->name('manual.coordinator');


});
