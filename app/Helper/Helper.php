<?php
namespace App\Helper;

class Helper
{

   	public function formatDate($string)
   	{
   		$date = Carbon::createFromFormat('Y-m-d', $string);
   		$output = $date->locale('en')->isoFormat('dddd Do MMMM YYYY');
   		return $output;
   	}
}