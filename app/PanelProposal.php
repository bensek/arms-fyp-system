<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanelProposal extends Model
{
    protected $table = "panels_proposals";

    public function proposal(){
        return $this->belongsTo(Proposal::class);
    }
}
