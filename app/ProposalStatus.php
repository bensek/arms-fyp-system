<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalStatus extends Model
{
    public function proposals()
    {
    	return $this->hasMany('App\Proposal');
    }
}
