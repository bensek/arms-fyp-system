<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    protected $appends = ['lecturer_ids'];


    public function lecturers()
    {
       return $this->belongsToMany(
            User::class,
            'panels_lecturers',
            'panel_id',
            'user_id');
    }

    public function getLecturerIdsAttribute(){
        return $this->lecturers()->pluck('user_id');
    }
}
