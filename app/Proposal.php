<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    public function status()
    {
    	return $this->belongsTo('App\ProposalStatus', 'proposal_status_id');
    }

    public function programme()
    {
    	return $this->belongsTo('App\Programme');
    }
}
