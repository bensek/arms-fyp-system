<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanelLecturer extends Model
{
    protected $table = "panels_lecturers";

    public function panel(){
    	return $this->belongsTo(Panel::class, 'panel_id');
    }
}
