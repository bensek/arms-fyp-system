<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
class VerifyAllEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command verifies all emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::get();
        foreach ($users as $user) {
            $user->is_email_verified = 1;
            $user->verified = 1;
            $user->save();
            # code...
        }
    }
}
