<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicYear extends Model
{
	protected $appends = ['description'];

	public function getDescriptionAttribute(){
		return 'Academic Year ' . $this->name;
	}

    public function panelLecturers(){
    	return User::query()->whereHas('panels_lecturers', function($q){
    		$q->where('user_id', $this->id);
    	})->get();
    }
}
