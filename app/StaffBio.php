<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffBio extends Model
{
    protected $table = 'staff_bio';

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function department()
    {
    	return $this->belongsTo('App\Department');

    }
}
