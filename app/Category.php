<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function projects()
    {
    	return $this->belongsToMany(Project::class, 'project_category');
    }

    // public function projects()
    // {
    //    //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
    //    return $this->belongsToMany(
    //         Resource::class,
    //         'project_category',
    //         'category_id',
    //         'project_id');
    // }
}
