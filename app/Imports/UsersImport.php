<?php

namespace App\Imports;
use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToModel;
use Auth;
use Spatie\Permission\Models\Role;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $department_id = Auth::user()->department_id;

        $user = new User;
        $user->fname = $row[0];
        $user->lname = $row[1];
        $user->other_name = $row[2];
        $user->email = $row[3];
        $user->password = Hash::make($row[4]);
        $user->department_id = $department_id;

         // Attach role
        $role = Role::where('name', 'supervisor')->firstOrFail();
        $user->assignRole($role);

        return $user;
    }
}
