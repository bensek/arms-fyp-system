<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BioData extends Model
{
    protected $table = 'bio_data';

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
