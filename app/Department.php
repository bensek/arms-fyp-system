<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function school()
    {
    	return $this->belongsTo('App\School');
    }

    public function programmes()
    {
    	return $this->hasMany('App\Programme');
    }

    public function supervisors()
    {
    	return $this->hasMany('App\StaffBio');
    }

}
