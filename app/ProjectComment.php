<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectComment extends Model
{
    protected $table = "projects_comments";

    public function user(){
        return $this->belongsTo(User::class);
    }
}
