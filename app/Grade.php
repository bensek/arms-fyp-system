<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Grade extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	protected $appends = ['total'];
    public function student(){
        return $this->belongsTo(User::class, 'student_id');
    }

    public function getTotalAttribute(){
    	$avg = ($this->proposal_report + $this->midterm_presentation +  
               $this->final_report + $this->final_presentation)/4 ;

    	return round($avg);
    }

    // public function audits()
}
