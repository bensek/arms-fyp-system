<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function categories()
    {
    	return $this->belongsToMany(Category::class, 'project_category');
    }
}
