<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use Session;

class SchoolController extends Controller
{
    public function index()
    {
    	session(['title' => 'Schools']);
    	$schools = School::get();
    	return view('schools.index')->with(['schools'=>$schools]);
    }

    public function new()
    {
    	session(['title' => 'New School']);
    	$school = new School;
    	return view('schools.new')->with(['school'=>$school]);
    }

    public function store(Request $request)
    {	
    	$is_new = false;

    	if($request->id){
    		$school = School::find($request->id);
    	}else{
    		$is_new = true;
    		$school = new School;
    	}

    	$school->name = $request->name;
    	$school->save();

    	if($is_new){
	    	if($school->save()){
	    		Session::flash('message', 'School has been saved'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect('/admin/schools');
	    	}else{
	    		Session::flash('message', 'School has not been saved'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect('/admin/schools');
	    	}
	    }else{
	    	if($school->save()){
	    		Session::flash('message', 'School has been updated'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect('/admin/schools');
	    	}else{
	    		Session::flash('message', 'School has not been updated'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect('/admin/schools');
	    	}
	    }

    }

    public function edit($id)
    {
    	session(['title' => 'Edit School']);
    	$school = School::find($id);
    	return view('schools.new')->with(['school'=>$school]);
    }

    public function delete($id)
    {
    	$school = School::find($id);
    	$school->delete();
    }

}
