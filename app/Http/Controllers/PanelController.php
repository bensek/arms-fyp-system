<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Panel;
use App\User;
use App\AcademicYear;
use App\PanelLecturer;
use Auth;
use Session;
use App\Proposal;
use App\PanelProposal;

class PanelController extends Controller
{
    public function index($type)
    {
        if($type == 'mid_term'){
            session(['title' => 'Mid-Term Presentations Panels']);
        }

        if($type == 'final'){
            session(['title' => 'Final Presentations Panels']);
        }

        if($type == 'supplementary'){
            session(['title' => 'Supplementary Presentations Panels']);
        }
        if(Auth::user()->hasRole('supervisor')){
            $panels = Panel::where('type', $type)->whereHas('lecturers', function($q){
                $q->where('user_id', Auth::id());
            })->get();
        }else{
             $panels = Panel::where('type', $type)->get();
        }

        return view('panels.index')->with(['panels'=>$panels, 'type'=>$type]);
    }

    public function unallocatedLecturers($type){

        $user = Auth::user();
        $year = AcademicYear::where('is_current', 1)->first();
        // Return supervisors who have not yet been allocated to that year. 
        $unallocated = array();
        $supervisors = User::role(['supervisor'])->where('department_id', $user->department_id)->get();
        
        foreach($supervisors as $sup){
            $allocated = false;

            $local = PanelLecturer::where('user_id', $sup->id)->get();
            //return $local;
            if(count($local) > 0){
                // Lecturer exists and check the panel year
                foreach($local as $pl){
                    if($pl->panel->academic_year_id == $year->id && $pl->panel->type == $type){
                    // Already added dont add
                        $allocated = true;
                    }
                }
            }
            if(!$allocated){
                array_push($unallocated, $sup);
            }
        }

        return $unallocated;
    }

    public function new($type)
    {
        session(['title' => 'New Panel']);

        return view('panels.new')->with(['supervisors'=>$this->unallocatedLecturers($type), 'type'=>$type]);
    }

    public function store(Request $request)
    {
        $year = AcademicYear::where('is_current', 1)->first();

        $panel = new Panel;
        $panel->name = $request->name;
        $panel->type = $request->type;
        $panel->department_id = Auth::user()->department_id;
        $panel->academic_year_id = $year->id;
        $panel->chair_id = $request->chair_id;
        $panel->location = $request->location;
        $panel->save();

        foreach($request->supervisors as $id){
            $pl = new PanelLecturer;
            $pl->panel_id = $panel->id;
            $pl->user_id = $id;
            $pl->save();
        }

        Session::flash('message', 'Panel has been created'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('list_panels', $request->type);
    }

    public function edit($type, $id){
        session(['title' => 'Update Panel']);

        $panel = Panel::with('lecturers')->findOrFail($id);
        $lecturers = $panel->lecturers->pluck('id')->toArray();
        //$lecturers = PanelLecturer::where('panel_id', $id)->get();
        $supervisors = User::role(['supervisor'])->where('department_id', Auth::user()->department_id)->get();

        return view('panels.edit')->with(['panel'=>$panel, 'lecturers' => $lecturers, 'supervisors' => $supervisors, 'type']);
    }

    public function update(Request $request){

        $panel = Panel::findOrFail($request->panel_id);
        $panel->name = $request->name;
        $panel->type = $request->type;
        $panel->save();

        $lecturers = PanelLecturer::where('panel_id', $panel->id)->get();
        foreach($lecturers as $l){
            $l->delete();
        }

        foreach($request->supervisors as $id){
            $pl = new PanelLecturer;
            $pl->panel_id = $panel->id;
            $pl->user_id = $id;
            $pl->save();
        }

        Session::flash('message', 'Panel has been updated'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('list_panels', $request->type);
    }

    public function delete($id)
    {
        $panel = Panel::find($id);

        $lecturers = PanelLecturer::where('panel_id', $id)->get();
        foreach($lecturers as $l){
            $l->delete();
        }

        //students allocated
        $stds = PanelProposal::where('panel_id', $id)->get();
        foreach ($stds as $s) {
            $s->delete();
        }

        $panel->delete();

    }

    public function show($type, $id)
    {
        
        $panel = Panel::with('lecturers')->findOrFail($id);
        $type = null;
        if($panel->type=='mid_term'){
            $type = "Mid-Term Presentations";
        }elseif ($panel->type=='final') {
             $type = "Final Presentations";
        }else{
            $type = "Supplementary Presentations";
        }

        session(['title' => $panel->name . " [ " .$type . " ]"]);
        $students = PanelProposal::where('panel_id', $panel->id)->get();
        return view('panels.show')->with(['panel' => $panel, 'students'=>$students]);
    }

    public function allocate($panel_id)
    {
        session(['title' => 'Allocate Students']);
        $panel = Panel::findOrFail($panel_id);
        $year = AcademicYear::where('is_current', 1)->first();
        // $panel_times = 

        $proposals = Proposal::where('academic_year_id', $year->id)->get();

        return view('panels.allocate')->with(['time' => $this->panelTimes($panel_id), 'proposals'=>$proposals, 'panel' => $panel]);
    }

    public function panelTimes($panel_id){
        $time = ['8:30 - 8:45', 
                 '8:45 - 9:00', 
                 '9:00 - 9:15', 
                 '9:15 - 9:30', 
                 '9:30 - 9:45', 
                 '9:45 - 10:00',
                 '10:00 - 10:15', 
                 '10:15 - 10:30', 
                 '11:00 - 11:15', 
                 '11:15 - 11:30', 
                 '11:30 - 11:45', 
                 '11:45 - 12:00',
                 '12:00 - 12:15', 
                 '12:15 - 12:30', 
                 '12:30 - 13:00'
                ];

        $available_times = array();
        $panel = Panel::findOrFail($panel_id);

        foreach($time as $t){
            $taken = PanelProposal::where('panel_id', $panel_id)->where('time', $t)->count();
            if($taken == 0){
                array_push($available_times, $t);
            }
        }

        return $available_times;
    }

    public function store_allocated_students(Request $request){
        $a = new PanelProposal;
        $a->time = $request->time;
        $a->proposal_id = $request->proposal_id;
        $a->panel_id = $request->panel_id;
        $a->save();

        $panel = Panel::findOrFail($a->panel_id);

        Session::flash('message', 'Students have been added to this panel'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('show_panels', [$panel->type, $a->panel_id]);

    }

   
}
