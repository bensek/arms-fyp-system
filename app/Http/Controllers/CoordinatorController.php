<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CoordinatorController extends Controller
{
   public function index()
   {
   	session(['title'=>'Project Coordinators']);
   	return view('coordinators.index');
   }
}
