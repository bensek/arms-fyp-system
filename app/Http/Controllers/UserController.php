<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Redirect;
use Hash;
use App\UserVerify;
use Session;

class UserController extends Controller
{
    public function get_student_by_id($id)
    {
    	$user = User::find($id);
    	$bio_data = $user->bio_data;
    	return response()->json([
    		'student' => $user,
    		'bio_data' => $bio_data,
    		'fullname' => $user->fullnames()
    	]);
    }

    public function get_supervisor_by_id($id)
    {
        $user = User::find($id);
        $info = $user->staff_bio;
        return response()->json($user->email);
    }

    public function account()
    {
        session(['title'=> 'Account']);
        return view('profile.account')->with(['user'=> Auth::user()]);
    }

    public function update_account(Request $request)
    {
       $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'title' => 'required|string|max:255',
            'office' => 'required'
        ]);


        $user = Auth::user();
        $user->fname = $request->first_name;
        $user->lname = $request->last_name;
        $user->other_name = $request->other_name;
        $user->email = $request->email;
        $user->save();

        $staff = $user->staff_bio;
        $staff->title = $request->title;
        $staff->office = $request->office;
        $staff->save();

        return Redirect::back()->with('message', 'Your account info has been updated');
    }


    public function change_password(Request $request)
    {
        $oldpassword = $request->oldpassword;
        $user = Auth::user();
        if(!(Hash::check($oldpassword, $user->password))){
            return Redirect::back()->withErrors(['Old password is incorrect']);
        }

        $request->validate([
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user->password = Hash::make($request->password);
        $user->save();

        return Redirect::back()->with('message', 'Your password has been changed');
    }

    public function requires_approval(){
        session(['title'=>'Account Requires Approval']);

        return view('auth.approve');
    }

    public function verifyAccount($token)
    {
        $verifyUser = UserVerify::where('token', $token)->first();
  
        $message = 'Sorry your email cannot be identified.';
  
        if(!is_null($verifyUser) ){
            $user = $verifyUser->user;
              
            if(!$user->is_email_verified) {
                $verifyUser->user->is_email_verified = 1;
                $verifyUser->user->save();
                $message = "Your e-mail is verified. You can now login.";
            } else {
                $message = "Your e-mail is already verified. You can now login.";
            }
        }
  
      return redirect()->route('login')->with('message', $message);
    }

    public function reset_password(){
        Session(['title' => 'Reset Password']);

        return view('students.resetpassword')->with(['random'=>$this->generateRandomCode(15)]);
    }

    public function save_password(Request $request){

        $user = User::where('email', $request->email)->first();

        if($user == null){
             Session::flash('message', 'User with email does not exist!!!');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }

        $user->password = Hash::make($request->password);
        $user->save();

        Session::flash('message', 'Password has been reset successfully');
        Session::flash('alert-class', 'alert-success');

        return redirect()->back();
    }

    function generateRandomCode($length = 10) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
