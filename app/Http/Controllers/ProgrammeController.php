<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Programme;
use App\Department;
use Session;

class ProgrammeController extends Controller
{
    
    public function index()
    {
    	session(['title' => 'Programmes']);
    	$programmes = Programme::get();
    	return view('programmes.index')->with(['programmes'=>$programmes]);
    }

    public function new()
    {
    	session(['title' => 'New Programme']);
    	$programme = new Programme;
    	$departments = Department::get();
    	return view('programmes.new')->with(['programme'=>$programme, 'departments'=>$departments]);
    }

    public function store(Request $request)
    {	
    	$is_new = false;

    	if($request->id){
    		$programme = Programme::find($request->id);
    	}else{
    		$is_new = true;
    		$programme = new Programme;
    	}

    	$programme->name = $request->name;
    	$programme->department_id = $request->department_id;
    	$programme->code = $request->code;
    	$programme->save();

    	if($is_new){
	    	if($programme->save()){
	    		Session::flash('message', 'Programme has been saved'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect('/admin/programmes');
	    	}else{
	    		Session::flash('message', 'Programme has not been saved'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect('/admin/programmes');
	    	}
	    }else{
	    	if($programme->save()){
	    		Session::flash('message', 'Programme has been updated'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect('/admin/programmes');
	    	}else{
	    		Session::flash('message', 'Programme has not been updated'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect('/admin/programmes');
	    	}
	    }

    }

    public function edit($id)
    {
    	session(['title' => 'Edit Programme']);
    	$programme = Programme::find($id);
    	$departments = Department::get();
    	return view('programmes.new')->with(['programme'=>$programme, 'departments'=>$departments]);
     }

    public function delete($id)
    {
    	$programme = Programme::find($id);
    	$programme->delete();
    }
}
