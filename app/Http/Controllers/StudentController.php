<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Programme;
use App\User;
use App\Grade;
use Auth;
use App\AcademicYear;

class StudentController extends Controller
{
    public function all(){
        session(['title'=>'Select Student to Grade']);
        $user = Auth::user();
        $students = User::role('student')->where('department_id', $user->department_id)->get();
        return view('students.index')->with(['users'=>$students]);

    }

    public function index($id, $name, $year_id)
    {
        $year = AcademicYear::find($year_id);
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();

    	$programme = Programme::findOrFail($id);
    	$value = $programme->id;
    	$students = User::role('student')->where('programme_id', $value)->where('academic_year_id', $year_id)->get();

    	session(['title'=>$programme->name.' Students']);
    	return view('students.index')->with(['users'=> $students, 'programme'=>$programme, 'current_year' => $year, 'years' => $years]);
    }

    public function show($id)
    {
    	$user = User::find($id)->first();
    	session(['title'=>$user->fname . " " . $user->lname]);
    	return view('students.show')->with(['user' => $user]);
    }

  
}
