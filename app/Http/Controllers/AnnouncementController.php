<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use Session;
use Auth;
use App\Http\Traits\DateTrait;

class AnnouncementController extends Controller
{

    use DateTrait;

    public function index()
    {
    	session(['title' => 'Announcements']);
    	$ans = Announcement::where('department_id', Auth::user()->department_id)->get();
    	return view('announcements.index')->with(['ans'=>$ans]);
    }

    public function new()
    {
    	session(['title' => 'New Announcement']);
    	$an = new Announcement;
    	return view('announcements.new')->with(['an'=>$an]);
    }

    public function store(Request $request)
    {	
    	$is_new = false;

    	if($request->id){
    		$an = Announcement::find($request->id);
    	}else{
    		$is_new = true;
    		$an = new Announcement;
    	}

    	$an->title = $request->title;
        $an->description = $request->description;
        $an->date = $this->getDateTime();
        $an->department_id = Auth::user()->department_id;
    	$an->save();

    	if($is_new){
	    	if($an->save()){
	    		Session::flash('message', 'Announcement has been saved'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect('announcements');
	    	}else{
	    		Session::flash('message', 'School has not been saved'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect('announcements');
	    	}
	    }else{
	    	if($an->save()){
	    		Session::flash('message', 'Announcement has been updated'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect('announcements');
	    	}else{
	    		Session::flash('message', 'School has not been updated'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect('announcements');
	    	}
	    }

    }

     public function show($id)
    {
        session(['title' => 'Announcement']);
        $an = Announcement::find($id);
        return view('announcements.show')->with(['an'=>$an]);
    }

    public function edit($id)
    {
    	session(['title' => 'Edit Announcement']);
    	$an = Announcement::find($id);
    	return view('announcements.new')->with(['an'=>$an]);
    }

    public function delete($id)
    {
    	$an = Announcement::find($id);
    	$an->delete();
    }
}
