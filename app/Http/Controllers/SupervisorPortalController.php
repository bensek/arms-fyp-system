<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Proposal;
use App\Presentation;
use Auth;
use App\User;
use App\Report;

class SupervisorPortalController extends Controller
{
    public function dashboard()
    {
        $user = Auth::user();
    	session(['title'=> 'Dashboard']);
    	$proposals_count = count(Proposal::where('supervisor_id', $user->id)->where('active', 1)->get());
        $proposals = Proposal::where('supervisor_id', $user->id)->orderBy('created_at', 'desc')->where('active', 1)->take(5)->get();

        $projects_count = count(Project::get());

        return view('supervisor_portal.dashboard')->with([ 'proposals'=>$proposals, 'projects_count'=>$projects_count, 'proposals_count'=> $proposals_count]);
    }

    public function get_proposals(){
        $user = Auth::user();
        $proposals = Proposal::where('supervisor_id', $user->id)->where('active', 1)->get();
        session(['title'=> 'My Proposals']);
        return view('supervisor_portal.proposals')->with(['proposals'=>$proposals]);

    }

    public function show_proposal($id){
        $proposal = Proposal::find($id);
        session(['title'=> 'Proposal #' . $proposal->id]);
        $presentations = Presentation::where('proposal_id', $proposal->id)->get();
        $reports = Report::where('proposal_id', $proposal->id)->get();
        return view('supervisor_portal.show_proposal')->with(['proposal'=>$proposal, 'presentations' => $presentations, 'reports' => $reports]);
    }

    public function get_presentations(){
        $presentations = Presentation::where('supervisor_id', Auth::id())->get();
        session(['title'=>'Presentations']);
        return view('supervisor_portal.presentations')->with(['presentations' => $presentations]);
    }


    public function get_grades(){
        session(['title' => 'Grades']);

        $students = User::role('student')->get();

        return view('supervisor_portal.grades')->with(['students'=>$students]);
    }
}
