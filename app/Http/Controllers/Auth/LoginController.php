<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    protected function authenticated(Request $request, $user)
    {

    if ($user->hasRole('student')) {// do your magic here
        return redirect()->route('student_dashboard');
    }
    if ($user->hasRole('supervisor')) {// do your magic here
        return redirect()->route('supervisor_dashboard');
    }

    if ($user->hasRole('coordinator')) {
        return redirect()->route('dashboard');
    }

    if ($user->hasRole('admin')) {
        return redirect()->route('admin_dashboard');
    }

     return redirect('/');
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        session(['title'=> "Login"]);
        return view('auth.login');
    }

   


}
