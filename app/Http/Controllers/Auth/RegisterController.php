<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\BioData;
use App\StaffBio;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Auth;
use App\Mail\EmailVerification;
use App\UserVerify;
use Mail;
use Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    // /**
    //  * Create a new user instance after a valid registration.
    //  *
    //  * @param  array  $data
    //  * @return \App\User
    //  */
    // protected function create(array $data)
    // {
    //     $role = $data['role'];

    //     // Student Registration , Role = 3
    //     if ($role == 3){


    //         $bio = new BioData;
    //         $bio->user_id = $user->id;
    //         $bio->programme_id = $data['programme_id'];
    //         $bio->registration_no = $data['registration_no'];
    //         $bio->student_no = $date['student_no'];
    //         $bio->save();

    //         return $user;
    //     }

    // }

    protected function student_form(){
        session(['title'=>'Student Registration']);
        $years = \App\AcademicYear::all();
        return view('auth.student_form')->with(['programmes'=> \App\Programme::get(),
            'years' => $years
    ]);
    }

    protected function supervisor_form(){
        session(['title'=>'Supervisor Registration']);
        return view('auth.supervisor_form')->with(['departments'=> \App\Department::get()]);
    }

    protected function register_student(Request $request){
        $request->validate([
            'programme' => 'required|integer',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'registration_number' => 'required|string|max:255',
            'student_number' => 'required|string|max:255',
            'password' => 'required|string|min:8|confirmed',
            'academic_year' => 'required'
        ]);

        $user = new User;
        $user->fname = $request->first_name;
        $user->lname = $request->last_name;
        $user->other_name = $request->other_name;
        $user->email = $request->email;
        $user->programme_id = $request->programme;
        $user->password = Hash::make($request->password);
        $user->academic_year_id = $request->academic_year;
        $user->verified = 1;
        //Set the department
        $user->department_id = \App\Programme::find($request->programme)->department_id;
        // $user->sendEmailVerificationNotification();

        $user->save();

        $bio = new BioData;
        $bio->user_id = $user->id;
        $bio->registration_no = $request->registration_number;
        $bio->student_no = $request->student_number;
        $bio->save();


        $role = Role::where('name', 'student')->firstOrFail();
        $user->assignRole($role);

        
        // Send notification to verify email

        // $token = Str::random(64);

        // UserVerify::create([
        //       'user_id' => $user->id, 
        //       'token' => $token
        // ]);
  
        // $details = [
        //     'token' => $token, 
        // ];

        // Mail::to($user->email)->send(new EmailVerification($details));

        return redirect()->route('login')->with('message', 'Account has been created successfully. Login to continue');

        // return redirect()->route('login')
        //             ->with('message', 'You account has been successfully registered. Check your email to verify this account');
    }

    protected function register_supervisor(Request $request){
        $request->validate([
            'department' => 'required|integer',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'title' => 'required|string|max:255',
            'password' => 'required|string|min:8|confirmed',
            'office' => 'required'
        ]);

        $user = new User;
        $user->fname = $request->first_name;
        $user->lname = $request->last_name;
        $user->other_name = $request->other_name;
        $user->email = $request->email;
        $user->department_id = $request->department;
        $user->password = Hash::make($request->password);
        $user->verified = 0;

        // $user->sendEmailVerificationNotification();


        $user->save();

        // Save the academic information of supervisor

        $staff = new StaffBio;
        $staff->user_id = $user->id;
        $staff->title = $request->title;
        $staff->office = $request->office;
        $staff->save();

        // Attach role
        $role = Role::where('name', 'supervisor')->firstOrFail();
        $user->assignRole($role);



        // Send notification to verify email

        // $token = Str::random(64);

        // UserVerify::create([
        //       'user_id' => $user->id, 
        //       'token' => $token
        // ]);
  
        // $details = [
        //     'token' => $token, 
        // ];

        // Mail::to($user->email)->send(new EmailVerification($details));

        // return redirect()->route('login')
        //             ->with('message', 'You account has been successfully registered. Check your email to verify this account');
        return redirect()->route('login')->with('message', 'Account has been created successfully. Wait for Coordinator to approve your account.');
    }
}
