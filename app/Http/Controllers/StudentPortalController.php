<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Programme;
use Auth;
use App\Proposal;
use App\ProposalStatus;
use App\Project;
use App\Http\Traits\DateTrait;
use App\Notice;
use Redirect;
use Hash;
use App\Announcement;
use App\Category;
use App\Presentation;
use Str;
use Session;
use Storage;
use Notification;
use App\User;
use App\Notifications\ProposalNotification;
use App\Report;
use App\ProjectComment;
use App\PanelProposal;

class StudentPortalController extends Controller
{
    use DateTrait;
    public function dashboard()
    {
    	session(['title' => 'Home']);
        $user= Auth::user();
        $categories = Category::where('department_id', $user->department_id)->get();
        $projects = Project::orderBy('created_at', 'desc')->take(8)->get();
        // Return My Proposal
        $proposal = Proposal::where('std_id1', $user->id)->orWhere('std_id2', $user->id)->orderBy('created_at', 'desc')->first();
     

        // Get announcements

        $announcements = Announcement::orderBy('created_at', 'desc')->take(8)->get();

        // Get Academic Year
        $year = \App\AcademicYear::where('is_current',1)->where('department_id', $user->department_id)->first();

    	return view('student_portal.dashboard')->with([
            'proposal' => $proposal,
            'categories' => $categories,
            'announcements' => $announcements,
            'projects' => $projects,
            'year' => $year
        ]);
    }

    public function get_projects(){
        Session(['title' => 'Projects List']);
        $user = Auth::user();
          // Get Academic Year
        $year = \App\AcademicYear::where('is_current',1)->where('department_id', $user->department_id)->first();

        $projects = Project::get();
        return view('student_portal.projects')->with(['projects'=>$projects,'year' => $year]);
    }

    public function get_projects_by_category($id){
        Session(['title' => 'Projects List']);
                $user = Auth::user();

        $category = Category::findOrFail($id);
        // Get Academic Year
        $year = \App\AcademicYear::where('is_current',1)->where('department_id', $user->department_id)->first();

        $projects = $category->projects;
        return view('student_portal.projects')->with(['projects'=>$projects, 'year' => $year]);
    }

    public function display_project($id)
    {
        $project = Project::find($id);
        $comments = ProjectComment::with('user')->where('project_id', $project->id)->orderBy('created_at', 'desc')->get();
        session(['title' => $project->title]);
        return view('student_portal.project_details')->with([
            'project'=> $project,
            'comments' => $comments,
            ]

        );
    }

    public function submit_proposal($id)
    {
        if($id == 0){
            $project = new Project;
        }else{
            $project = Project::findOrFail($id);
        }
    	session(['title'=>'Submit Proposal']);
        $proposal = new Proposal;
        $proposal->status = "Pending";
        $student = Auth::user();
        // Get student programme_id
        $std_programme_id = $student->programme_id;
        // Get other programmes of the department
        $programmes = Programme::where('department_id', $student->department_id)->get();

        // Return students in that programme
        $students = $student->programme->students;

        // Get Academic Year
        // $year = \App\AcademicYear::where('is_current',1)->where('department_id', $student->department_id)->first();
        $years = \App\AcademicYear::where('department_id', $student->department_id)->orderBy('created_at', 'desc')->get();

    	return view('student_portal.submit_proposal')->with([
            'project' => $project,
            'proposal' => $proposal,
            'programmes' => $programmes,
            'std_programme_id' => $std_programme_id,
            'students' => $students,
            'user' => Auth::user(),
            'years' => $years
        ]);
    }

    public function store_proposal(Request $request)
    {
        $is_new = false;
        if($request->id){
            $proposal = Proposal::find($request->id);
        }else{
            $proposal = new Proposal;
            $status = ProposalStatus::where('name', 'Pending')->first();
            $proposal->proposal_status_id = $status->id;
        }

        $programme = Programme::find($request->programme_id)->first();

        if($request->is_individual_project == true){
            $proposal->is_individual_project = true;
        }else{
            $proposal->is_individual_project = false;
        }

        $supervisor = null;
        if($request->project_id != 0){
            $project = Project::findOrFail($request->project_id);
            $supervisor = User::find($project->supervisor_id);
            $proposal->project_id = $project->id;
            $proposal->supervisor_id = $project->supervisor_id;
            $proposal->supervisor_name = $supervisor->staff_bio->title .". " . $supervisor->fullnames();
            $proposal->supervisor_email = $supervisor->email;
        }

        $proposal->programme_id = $request->programme_id;
        $proposal->academic_year_id = $request->year_id;
        $proposal->std_id1 = Auth::id();
        $proposal->std_id2 = $request->std_id2;
        $proposal->name1 = $request->name1;
        $proposal->name2 =$request->name2;
        $proposal->email1 = $request->email1;
        $proposal->email2 = $request->email2;
        $proposal->reg_no1 = $request->reg_no1;
        $proposal->reg_no2 = $request->reg_no2;
        $proposal->title = $request->title;
        $proposal->dateTime = $this->getDateTime();
        $proposal->problem_statement = $request->problem_statement;
        $proposal->lecturer = $request->lecturer;
        $proposal->lecturer_name = $request->lecturer_name;
        if($request->hasFile('proposal_doc')){
        $uniqueFileName = $programme->code . "_" . $request->title . ".". $request->file('proposal_doc')->getClientOriginalExtension();

        $proposal->document = $request->file('proposal_doc')->storeAs('/proposals/'. $programme->code, $uniqueFileName, 'public');
        }
        $proposal->comments = $request->comments;

        $proposal->save();

        // Update Users
        // $user = Auth::user();
        // $user->proposal_id = $proposal->id;
        // if(!$proposal->is_individual_project){
        //     $partner = User::find($proposal->std_id2);
        //     $user->partner_id = $partner->id;
        //     $user->save();
            
        //     $partner->partner_id = $user->id;
        //     $partner->proposal_id = $proposal->id;
        //     $partner->save();
        // }else{
        //     $user->partner_id = null;
        //     $user->save();
        // }

        // Notify lecturer
        if($supervisor != null){
            $details = [
                'subject' => 'New Project Proposal',
                'greeting' => 'Dear ' . $supervisor->fname . ',',
                'body' => 'Your project entitled ' . $project->title . ' has been applied for by the student '. $request->name1 . '.', 
                'actionText' => 'View the proposal',
                'actionURL' => route('show_proposal', [$programme->id, $proposal->id]), 
            ];

            //Notification::send($supervisor, new ProposalNotification($details));
        }

        return redirect()->route('student_dashboard')->with('message', 'Proposal has been submitted successfully');
    }


    public function show_proposal($id)
    {
        $proposal = Proposal::find($id);

        session(['title'=>$proposal->title]);
        $presentations = Presentation::where('proposal_id', $proposal->id)->get();
        $reports = Report::where('proposal_id', $proposal->id)->get();

        $notices = Notice::where('proposal_id', $id)->orderBy('created_at', 'desc')->get();

        return view('student_portal.show_proposal')->with([
            'proposal' => $proposal,
            'notices' => $notices,
            'presentations' => $presentations,
            'reports' => $reports,
        ]);

    }

    public function delete_proposal($id){
        $proposal = Proposal::find($id);
        $user = Auth::user();
        // if($proposal->status == 'approved'){
        //     return redirect()->back()->withErrors('Proposal can not be delete');
        // }

        $presentations = Presentation::where('proposal_id', $proposal->id)->get();
        foreach($presentations as $p){
            
            if($user->presentation1_id == $p->id){
                $user->presentation1_id = null;
                $user->save();
            }
            if($user->presentation2_id == $p->id){
                $user->presentation2_id = null;
                $user->save();
            }
            $p->delete();
        }
        $reports = Report::where('proposal_id', $proposal->id)->get();
        foreach ($reports as $r) {
            if($user->report1_id == $r->id){
                $user->report1_id = null;
                $user->save();
            }
            if($user->report2_id == $r->id){
                $user->report2_id = null;
                $user->save();
            }

            $r->delete();
        }

        $panels = PanelProposal::where('proposal_id', $proposal->id)->get();
        foreach($panels as $p){
            $p->delete();
        }

        if($user->proposal_id == $proposal->id){
            $user->proposal_id = null;
            $user->save();
        }



        $proposal->delete();

        return redirect()->route('get_profile')->with('message', 'Proposal deleted successfully');

    }

    public function get_profile()
    {
        session(['title'=> 'Account']);
        $user = Auth::user();
        // Get users proposals
        $proposals = Proposal::where('std_id1', $user->id)->orWhere('std_id2', $user->id)->orderBy('created_at', 'desc')->get();
        return view('student_portal.profile')->with([
            'user' => $user,
            'proposals' => $proposals
        ]);
    }

    public function edit_profile()
    {
        session(['title'=>'Edit Profile']);
        return view('student_portal.edit_profile')->with(['user'=>Auth::user()]);
    }

    public function update_student(Request $request)
    {
       $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'registration_number' => 'required|string|max:255',
            'student_number' => 'required|string|max:255',
        ]);


        $user = Auth::user();
        $user->fname = $request->first_name;
        $user->lname = $request->last_name;
        $user->other_name = $request->other_name;
        $user->email = $request->email;
        $user->save();

        $bio = $user->bio_data;
        $bio->registration_no = $request->registration_number;
        $bio->student_no = $request->student_number;
        $bio->save();

        return redirect()->route('get_profile'); 
    }

    
    public function get_announcements()
    {
        session(['title'=>'Announcements']);
        $all = Announcement::where('department_id', Auth::user()->department_id)->orderBy('created_at', 'desc')->get();
        $selected = $all->get(0)->id;
        return view('student_portal.list_announcements')->with([
            'announcements' => $all,
            'selected' => $selected
        ]);

    }

    public function display_announcement($id)
    {
        session(['title'=>'Announcements']);
        $all = Announcement::where('department_id', Auth::user()->department_id)->orderBy('created_at', 'desc')->get();

        return view('student_portal.list_announcements')->with([
            'announcements' => $all,
            'selected' => $id,
        ]);
    }

    // Presentations
    public function submit_presentation(Request $request){

        // Check if student already has the particular presentation
        $user = Auth::user();
        $type = $request->type;
        $is_final = false;
        if($type == 'final'){
            $is_final = true;
        }

        if(!$is_final && $user->presentation1_id != null){
            // Student already has a midterm presentation

            return Redirect::back()->withErrors(['You already submitted a mid-term presentation']);
        }

        if($is_final && $user->presentation2_id != null){
            // Student already has a final presentation

            return Redirect::back()->withErrors(['You already submitted a final presentation']);
        }

        // Save Presentation
        $proposal = Proposal::find($request->proposal_id);

        $presentation = new Presentation;
        $presentation->type = $request->type;
        $presentation->title = $request->title;
        $presentation->notes = $request->notes;
        if($request->hasFile('document')){
        $uniqueFileName = $presentation->title . "_User_" . Auth::id() . ".". $request->file('document')->getClientOriginalExtension();

        $presentation->document = $request->file('document')->storeAs('/presentations', $uniqueFileName, 'public');
        }
        $presentation->proposal_id = $proposal->id;
        $presentation->department_id = $proposal->programme->department_id;
        $presentation->user_id = Auth::id();
        $presentation->academic_year_id = $proposal->academic_year_id;
        $presentation->save();

        // Attach presentation to student
        if(!$is_final){
            $user->presentation1_id = $presentation->id;
            $user->save();
        }

        if($is_final){
            $user->presentation2_id = $presentation->id;
            $user->save();
        }

        // Check if proposal has a supervisor and send email notifications
        if($proposal->supervisor_id != null){
            $supervisor = User::find($proposal->supervisor_id);

            // Attach supervisor to presentations
            $presentation->supervisor_id = $supervisor->id;
            $presentation->save();

            // Send Notification
             $details = [
                'subject' => 'Student Presentation',
                'greeting' => 'Dear ' . $supervisor->fname . ',',
                'body' => Auth::user()->fullnames() . ' has submitted their presentation for the project ' . $proposal->title .'.',
                'actionText' => 'View presentation',
                'actionURL' => route('supervisor_presentations'), 
            ];

            //Notification::send($supervisor, new ProposalNotification($details));
        }

        Session::flash('message', 'Presentation has been submitted successfully');
        return redirect()->route('proposal_details', $proposal->id);
    }

    public function delete_presentation(Request $request){

        $presentation = Presentation::findOrFail($request->id);
        $type = $presentation->type;
        $user = Auth::user();

        // Update User Object
        if($type != 'final'){
            $user->presentation1_id = null;
        }else{
            $user->presentation2_id = null;
        }
        $user->save();
        $presentation->delete();


        return redirect()->back();
    }


    // Reports
    public function upload_report(Request $request){
         // Check if student already has the particular report
        $user = Auth::user();
        $type = $request->type;
        $is_final = false;
        if($type == 'project_report'){
            $is_final = true;
        }

        if(!$is_final && $user->report1_id != null){
            // Student already has a midterm report

            return Redirect::back()->withErrors(['You already submitted a project proposal report']);
        }

        if($is_final && $user->report2_id != null){
            // Student already has a final report

            return Redirect::back()->withErrors(['You already submitted a final project report']);
        }

        $proposal = Proposal::find($request->proposal_id);

        $report = new Report;
        $report->type = $request->type;
        $report->title = $request->title;
        $report->notes = $request->notes;
        if($request->hasFile('document')){
            $str = Str::random(20);
        $uniqueFileName = $str . ".". $request->file('document')->getClientOriginalExtension();

        $report->document = $request->file('document')->storeAs('/reports', $uniqueFileName, 'public');
        }
        $report->proposal_id = $proposal->id;
        $report->department_id = $proposal->programme->department_id;
        $report->user_id = Auth::id();
        $report->academic_year_id = $proposal->academic_year_id;
        $report->save();

        // Attach report to student
        if(!$is_final){
            $user->report1_id = $report->id;
            $user->save();
        }

        if($is_final){
            $user->report2_id = $report->id;
            $user->save();
        }
        // Check if proposal has a supervisor and send email notifications
        if($proposal->supervisor_id != null){
            $supervisor = User::find($proposal->supervisor_id);

            // Attach supervisor to presentations
            $report->supervisor_id = $supervisor->id;
            $report->save();

            // Send Notification
             $details = [
                'subject' => 'Student Report',
                'greeting' => 'Dear ' . $supervisor->fname . ',',
                'body' => Auth::user()->fullnames() . ' has submitted their report for the project ' . $proposal->title .'.',
                'actionText' => 'View Report',
                'actionURL' => route('supervisor_presentations'), 
            ];

            // Notification::send($supervisor, new ProposalNotification($details));
        }

        Session::flash('message', 'Report has been submitted successfully');

        return redirect()->route('proposal_details', $proposal->id);
    }

    public function delete_report(Request $request){

        $report = Report::findOrFail($request->id);
        $type = $report->type;
        $user = Auth::user();

        // Update User Object
        if($type != 'project_report'){
            $user->report1_id = null;
        }else{
            $user->report2_id = null;
        }
        $user->save();
        $report->delete();


        return redirect()->back();
    }


}
