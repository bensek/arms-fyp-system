<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Carbon\Carbon;
use Session;
use Auth;
use App\Category;
use App\ProjectComment;
use App\Http\Traits\DateTrait;
use App\AcademicYear;

class ProjectController extends Controller
{
    use DateTrait;

    public function index($year_id)
    {
        $user = Auth::user();
    	session(['title' => 'Projects List']);
        $year = AcademicYear::find($year_id);

        if($year_id == 0){
            $projects = Project::where('department_id', $user->department_id)->orderBy('created_at', 'desc')->get();
        }else{
            $projects = Project::where('department_id', $user->department_id)->where('academic_year_id', $year->id)->orderBy('created_at', 'desc')->get();
        }

        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();
        
    	return view('projects.index')->with(['projects'=>$projects, 'years'=>$years, 'current_year' => $year]);
    }

    public function new($year_id)
    {
    	session(['title' => 'New Project']);
    	$project = new Project;
        $categories = Category::where('department_id', Auth::user()->department_id)->get();
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();

    	return view('projects.new')->with([
                'project'=>$project,
                'categories' => $categories,
                'years' => $years,
                'year_id' => $year_id,
        ]);
    }

    public function store(Request $request)
    {
    	$is_new = false;

    	if($request->id){
    		$project = Project::find($request->id);
    	}else{
    		$is_new = true;
    		$project = new Project;
    	}

    	$project->title = $request->title;
    	$project->description = $request->description;
        $project->department_id = Auth::user()->department_id;
        $project->lecturer = $request->lecturer;
        $project->supervisor_id = Auth::id();
        $project->academic_year_id = $request->year_id;
        $project->date_added = $this->getDateTime();
    	$project->save();
        if($request->hasFile('thumbnail')){
            $uniqueFileName = "project_" . $project->id . ".". $request->file('thumbnail')->getClientOriginalExtension();
            $project->thumbnail = $request->file('thumbnail')->storeAs('/projects', $uniqueFileName, 'public');
        }
        $categories = $request->category_ids;
        if($is_new){
            $project->categories()->attach($categories);
        }else{
            $project->categories()->sync($categories);

        }

        $project->save();

    	if($is_new){
	    	if($project->save()){
	    		Session::flash('message', 'Project has been saved');
				Session::flash('alert-class', 'alert-success');
	    		return redirect()->back();
	    	}else{
	    		Session::flash('message', 'Project has not been saved');
				Session::flash('alert-class', 'alert-danger');
	    		return redirect()->back();
	    	}
	    }else{
	    	if($project->save()){
	    		Session::flash('message', 'Project has been updated');
				Session::flash('alert-class', 'alert-success');
	    		return redirect()->route('show_project', ['year_id' => $project->academic_year_id, 'id' => $project->id]);
	    	}else{
	    		Session::flash('message', 'Project has not been updated');
				Session::flash('alert-class', 'alert-danger');
	    		return redirect()->back();
	    	}
	    }
    }

    public function edit($year_id, $id)
    {
    	session(['title' => 'Edit Project']);
    	$project = Project::find($id);

        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();
        $categories = Category::where('department_id', Auth::user()->department_id)->get();
        foreach($project->categories as $cat)
        {
            $categoriesIds[] = $cat->id;
        }     
        return view('projects.edit')->with([
            'project'=>$project,
            'categories' => $categories,
            'categoriesIds' => $categoriesIds,
            'year_id' => $year_id,
            'years' => $years,
        ]);
    }

    public function show($year_id, $id)
    {
        session(['title' => 'Show Project']);
        $project = Project::find($id);
        $comments = ProjectComment::where('project_id', $project->id)->orderBy('created_at','desc')->get();
        return view('projects.show')->with([
            'project'=>$project,
            'comments' => $comments,
            'year_id' => $year_id,
        ]);
    }

    public function delete($id)
    {
    	$project = Project::find($id);
    	$project->delete();
    }


    // COMMENTS CODE

    public function storeComment(Request $request){
        $project = Project::findOrFail($request->project_id);

        $comment = new ProjectComment;
        $comment->project_id = $project->id;
        $comment->user_id = Auth::id();
        $comment->text = $request->text;

        $comment->save();

        return redirect()->back();
    }

    public function deleteComment($id){
        $comment = ProjectComment::findOrFail($id);

        $comment->delete();

        return redirect()->back();
    }

    public function editComment($id){
        $comment = ProjectComment::findOrFail($id);

        if(Auth::user()->hasRole('student')){
            return view('student_portal.edit_comment')->with(['comment'=>$comment]);
        }else{
            return view('projects.edit_comment')->with(['comment'=>$comment]);
        }
    }

    public function updateComment(Request $request){
        $comment = ProjectComment::findOrFail($request->id);
        $comment->text = $request->text;
        $comment->save();

        if(Auth::user()->hasRole('student')){
            return redirect()->route('display_project', $comment->project_id);
        }else{
            return redirect()->route('show_project', $comment->project_id);

        }
    }

}
