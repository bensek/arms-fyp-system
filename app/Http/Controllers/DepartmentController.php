<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\School;
use Session;

class DepartmentController extends Controller
{
    public function index()
    {
    	session(['title' => 'Departments']);
    	$departments = Department::get();
    	return view('departments.index')->with(['departments'=>$departments]);
    }

    public function new()
    {
    	session(['title' => 'New Department']);
    	$department = new Department;
    	$schools = School::get();
    	return view('departments.new')->with(['department'=>$department, 'schools'=>$schools]);
    }

    public function store(Request $request)
    {	
    	$is_new = false;

    	if($request->id){
    		$department = Department::find($request->id);
    	}else{
    		$is_new = true;
    		$department = new Department;
    	}

    	$department->name = $request->name;
    	$department->school_id = $request->school_id;
    	$department->head_of_department = $request->head_of_department;
    	$department->save();

    	if($is_new){
	    	if($department->save()){
	    		Session::flash('message', 'Department has been saved'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect()->route('list_departments');
	    	}else{
	    		Session::flash('message', 'Department has not been saved'); 
				Session::flash('alert-class', 'alert-danger'); 
                return redirect()->route('list_departments');
	    	}
	    }else{
	    	if($department->save()){
	    		Session::flash('message', 'Department has been updated'); 
				Session::flash('alert-class', 'alert-success'); 
                return redirect()->route('list_departments');
	    	}else{
	    		Session::flash('message', 'Department has not been updated'); 
				Session::flash('alert-class', 'alert-danger'); 
                return redirect()->route('list_departments');
	    	}
	    }

    }

    public function edit($id)
    {
    	session(['title' => 'Edit Department']);
    	$department = Department::find($id);
    	$schools = School::get();
    	return view('departments.new')->with(['department'=>$department, 'schools'=>$schools]);
     }

    public function delete($id)
    {
    	$department = Department::find($id);
    	$department->delete();
    }
}
