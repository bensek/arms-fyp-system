<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AcademicYear;
use Session;
use Auth;

class AcademicYearController extends Controller
{
    public function index()
    {
    	session(['title' => 'Academic Years']);
    	$years = AcademicYear::where('department_id', Auth::user()->department_id)->get();
    	return view('academic_years.index')->with(['years'=>$years]);
    }

    public function new()
    {
    	session(['title' => 'New Academic Year']);
    	$year = new AcademicYear;
    	return view('academic_years.new')->with(['year'=>$year]);
    }

    public function store(Request $request)
    {
    	$is_new = false;

    	if($request->id){
    		$year = AcademicYear::find($request->id);
    	}else{
    		$is_new = true;
    		$year = new AcademicYear;
    	}

        if($request->is_current == 1){
            // Disable any current year
            $current = AcademicYear::where('is_current', 1)->get();
            if(count($current) > 0){
                foreach ($current as $c) {
                    if($c->id != $year->id) {
                        $c->is_current = 0;
                        $c->save();
                    }
                }
            }
        }

    	$year->name = $request->name;
    	$year->start_date = $request->start_date;
    	$year->end_date = $request->end_date;
    	$year->is_current = $request->is_current;
        $year->deadline = $request->deadline;
        $year->late_deadline = $request->late_deadline;
    	$year->department_id = Auth::user()->department_id;
    	$year->save();

    	if($is_new){
	    	if($year->save()){
	    		Session::flash('message', 'Academic Year has been saved');
				Session::flash('alert-class', 'alert-success');
	    		return redirect('academic_years');
	    	}else{
	    		Session::flash('message', 'Academic Year has not been saved');
				Session::flash('alert-class', 'alert-danger');
	    		return redirect('academic_years');
	    	}
	    }else{
	    	if($year->save()){
	    		Session::flash('message', 'Academic Year has been updated');
				Session::flash('alert-class', 'alert-success');
	    		return redirect('academic_years');
	    	}else{
	    		Session::flash('message', 'Academic Year has not been updated');
				Session::flash('alert-class', 'alert-danger');
	    		return redirect('academic_years');
	    	}
	    }
    }

    public function edit($id)
    {
    	session(['title' => 'Edit Academic Year']);
    	$year = AcademicYear::find($id);
    	return view('academic_years.new')->with(['year'=>$year]);
    }

    public function delete($id)
    {
    	$year = AcademicYear::find($id);
    	$year->delete();
    }
}
