<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Mail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

     public function download(Request $request){
        return Storage::disk('public')->download($request->path);
    }

    public function emails()
    {
        $to_name = "Ben Rapha"; // $user->name;
        $to_email = "bensek73@gmail.com"; // $user->email;
        $data = array('name'=> $to_name, "body" => "Test mail");
        
        Mail::send('emails.test', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Test Email');
            $message->from('bensek73@gmail.com','ARMS');
        });
    }
}
