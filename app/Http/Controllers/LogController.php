<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;

class LogController extends Controller
{
    public function index(){
    	Session(['title' => 'Grade Logs']);
    	$grades = Grade::orderBy('updated_at', 'desc')->get();
    	return view('logs.index')->with(['grades'=> $grades]);
    }

    public function show($id){
    	Session(['title' => 'Grade Logs']);
    	$grade = Grade::findOrFail($id);

    	return view('logs.show')->with(['grade'=> $grade]);
    }
}
