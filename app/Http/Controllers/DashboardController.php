<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProposalStatus;
use App\Proposal;
use App\Project;
use Auth;
use App\Announcement;
use App\Presentation;
use App\Report;
use App\AcademicYear;

class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth']);
                // $this->middleware(['auth','verified']);

    }

    public function admin_dashboard()
    {
    	session(['title'=> 'Dashboard']);
       	return view('dashboards.admin');
    }

    public function get_user_dashboard()
    {
        $user = Auth::user();

        if ($user->hasRole('student')) {// do your magic here
            return redirect()->route('student_dashboard');
        }
        if ($user->hasRole('supervisor')) {// do your magic here
            return redirect()->route('supervisor_dashboard');
        }

        if ($user->hasRole('coordinator')) {
            return redirect()->route('coordinator_dashboard');
        }

        if ($user->hasRole('admin')) {
            return redirect()->route('admin_dashboard');
        }
    }

    public function co_dashboard()
    {

        session(['title'=> 'Dashboard']);
        $year = AcademicYear::where('is_current',1)->first();

        $status_p = ProposalStatus::where('name', 'Pending')->first();
        $proposals_p = Proposal::where('proposal_status_id', $status_p->id)->where('active', 1)->get();
        $pending_count = count($proposals_p);
        $status_u = ProposalStatus::where('name', 'Under Review')->first();
        $proposals_u = Proposal::where('proposal_status_id', $status_u->id)->where('active', 1)->get();
        $ureview_count = count($proposals_u);
        $status_a = ProposalStatus::where('name', 'Approved')->first();
        $proposals_a = Proposal::where('proposal_status_id', $status_a->id)->where('active', 1)->get();
        $approved_count = count($proposals_a);

        $user_department = Auth::user()->department_id;
        $project_count = count(Project::all()); //where('department_id', Auth::user()->department_id)->get());
        $announcements = Announcement::orderBy('created_at', 'desc')->take(5)->get();
        $proposals = Proposal::orderBy('created_at', 'desc')->take(5)->get();

        $user = Auth::user();
        // New Stats
        $presentations_mid = Presentation::where('department_id', $user->department_id)->where('type', 'mid_term')->count();
        $presentations_final = Presentation::where('department_id', $user->department_id)->where('type', 'final')->count();
        $reports_proposal = Report::where('department_id', $user->department_id)->where('type', 'project_proposal')->count();
        $reports_final = Report::where('department_id', $user->department_id)->where('type', 'project_report')->count();


        return view('dashboards.coordinator')->with([
            'pending' => $pending_count,
            'under_review' => $ureview_count,
            'approved' => $approved_count,
            'projects' => $project_count,
            'announcements' => $announcements,
            'proposals'=> $proposals,
            'presentations_mid' => $presentations_mid,
            'presentations_final' => $presentations_final,
            'reports_proposal' => $reports_proposal,
            'reports_final' => $reports_final,
            'active_year' => $year
        ]);
    }

}
