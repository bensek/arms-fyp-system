<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Session;
use Auth;

class CategoryController extends Controller
{
    public function index()
    {
    	session(['title' => 'Categories']);
    	$categories = Category::get();
    	return view('categories.index')->with(['categories'=>$categories]);
    }

    public function new()
    {
    	session(['title' => 'New Category']);
    	$cat = new Category;
    	return view('categories.new')->with(['category'=>$cat]);
    }

    public function store(Request $request)
    {	
    	$is_new = false;

    	if($request->id){
    		$category = Category::find($request->id);
    	}else{
    		$is_new = true;
    		$category = new Category;
    	}

    	$category->name = $request->name;
    	$category->description = $request->description;
        $category->department_id = Auth::user()->department_id;
    	$category->save();

    	if($is_new){
	    	if($category->save()){
	    		Session::flash('message', 'Category has been saved'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect()->route('list_categories');
	    	}else{
	    		Session::flash('message', 'Category has not been saved'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect()->route('list_categories');
	    	}
	    }else{
	    	if($category->save()){
	    		Session::flash('message', 'Category has been updated'); 
				Session::flash('alert-class', 'alert-success'); 
	    		return redirect()->route('list_categories');
	    	}else{
	    		Session::flash('message', 'Category has not been updated'); 
				Session::flash('alert-class', 'alert-danger'); 
	    		return redirect()->route('list_categories');
	    	}
	    }

    }

    public function edit($id)
    {
    	session(['title' => 'Edit Category']);
    	$category = Category::find($id);
    	return view('categories.new')->with(['category'=>$category]);
    }

    public function delete($id)
    {
    	$category = Category::find($id);
    	$category->delete();
    }

}
