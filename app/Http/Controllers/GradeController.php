<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Grade;
use Session;
use App\Programme;
use Auth;
use App\AcademicYear;
use App\Mail\EmailForGrade;
use Str;
use Mail;

class GradeController extends Controller
{
    // public function index(){
    //     session(['title'=>'Grades']);
    //     $students = User::role('student')->get();
    //     $programmes = Programme::where('department_id', Auth::user()->department_id)->get();

    //     return view('grades.index')->with(['grades'=>Grade::get(), 'programmes'=>$programmes]);
    // }
    public function index($programme_id, $year_id){
      $year = AcademicYear::find($year_id);
      $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();

      $programme = Programme::findOrFail($programme_id);

      $user = Auth::user();
      if($user->hasRole('supervisor')){
        $users = User::where('programme_id', $programme_id)->where('supervisor_id', $user->id)->role('student')->where('academic_year_id', $year_id)->get();
      }else{
        $users = User::where('programme_id', $programme_id)->role('student')->where('academic_year_id', $year_id)->get();
      }

      Session(['title' => $programme->name . ' Grades - ' . $year->name]);


      return view('grades.index')->with(['students'=>$users, 'programme'=>$programme, 'current_year' => $year, 'years' => $years]);
    }

    public function add(){
      return view('grades.new');
    }

    public function new($id, $year_id)
    {
        session(['title' => 'Grade Student']);
        $student = User::findOrFail($id);
        $grade = Grade::find($student->grade_id);
        if($grade == null){
            $grade = new Grade;
        }
        // $g = Grade::first();
        //return response()->json($grade->audits);
        return view('grades.new')->with(['student'=>$student, 'grade' => $grade, 'audits' => $grade->audits, 'year_id'=>$year_id]);
    }

    public function store(Request $request)
    {
      $grade = Grade::find($request->grade_id);
      if(is_null($grade)){
        $grade = new Grade;
      }
      $grade->student_id = $request->student_id;
      $grade->proposal_report = $request->proposal_report;
      $grade->final_report = $request->final_report;

      if(Auth::user()->hasRole(['coordinator', 'admin'])){
          $grade->midterm_presentation = $request->midterm_presentation;
          $grade->final_presentation = $request->final_presentation;
      }
       $grade->save();

       // Update user grade id
       $user = User::findOrFail($request->student_id);
       $user->grade_id = $grade->id;
       $user->save();

       // Delete all user grades
       $oldgrades = Grade::where('student_id', $user->id)->where('id', '!=', $grade->id)->get();
       foreach($oldgrades as $old){
            $old->delete();
       }


        Session::flash('message', 'Student has been graded');
        Session::flash('alert-class', 'alert-success');
        
       return redirect()->back(); //route('list_grades', $user->programme_id);
    }
    public function start_grading(Request $request){

        return redirect()->route('new_grade', $request->student_id);
    }

    // public function new($id){
    //     $student = User::findOrFail($id);
    //     session(['title'=> 'Grade Student']);

    //     return view('grades.new')->with(['student'=>$student]);
    // }

    public function send_email(){
      // Send Email
       $code = Str::random(6);
        
        $details = [
            'code' => $code, 
        ];

        //Mail::to(Auth::user()->email)->send(new EmailForGrade($details));

        //return view('emails.emailforgrade');
    }
}
