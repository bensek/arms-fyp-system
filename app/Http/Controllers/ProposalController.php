<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Programme;
use App\Proposal;
use App\ProposalStatus;
use Session;
use App\Notice;
use App\Http\Traits\DateTrait;
use App\AcademicYear;
use App\User;
use Mail;
use App\Presentation;
use App\Report;

class ProposalController extends Controller
{
    use DateTrait;

    public function index($programme_id, $year_id)
    {
        $year = AcademicYear::find($year_id);
        $programme = Programme::findOrFail($programme_id);
        $value = $programme->id;
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();
        // Get proposals of recent year added
        $proposals = Proposal::where('programme_id', $value)->where('academic_year_id', $year_id)->where('active', 1)->get();
        session(['title'=>$programme->code.' Proposals']);
        return view('proposals.index')->with(['proposals'=>$proposals, 'programme'=>$programme, 'years' =>$years, 'current_year'=> $year]);
    }

    public function get_proposals_by_year(Request $request)
    {
        $programme_id = $request->programme_id;
        $year = $request->year_id;
        $proposals = Proposal::where('programme_id', $programme_id)->where('academic_year_id', $year)->where('active', 1)->get();
        $programme = Programme::findOrFail($programme_id);
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();
        session(['title'=>$programme->code.' Proposals']);
        return view('proposals.index')->with(['proposals'=>$proposals, 'programme'=>$programme, 'years' =>$years]);
    }

    public function pending_proposals($status, $year_id)
    {
        session(['title'=> $status . ' Proposals']);
        $current_status = ProposalStatus::where('name', $status)->first();
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();
        $current_year = AcademicYear::find($year_id);
        $proposals = Proposal::where('proposal_status_id', $current_status->id)->where('academic_year_id', $year_id)->where('active', 1)->get();

        return view('proposals.proposal_by_status')->with(['status' => $status, 'proposals'=>$proposals, 'years'=> $years, 'current_year'=> $current_year]);
    }

    public function underreview_proposals()
    {
        session(['title'=>'Under Review Proposals']);
        $status = ProposalStatus::where('name', 'Under Review')->first();
        $proposals = Proposal::where('proposal_status_id', $status->id)->where('active', 1)->get();
        return view('proposals.proposal_by_status')->with(['proposals'=>$proposals]);
    }

     public function approved_proposals()
    {
        session(['title'=>'Approved Proposals']);
        $status = ProposalStatus::where('name', 'Approved')->first();
        $proposals = Proposal::where('proposal_status_id', $status->id)->where('active', 1)->get();
        return view('proposals.approved')->with(['proposals'=>$proposals]);
    }

    public function show($programme_id, $id)
    {
        $statuses = ProposalStatus::get();
        $proposal = Proposal::findOrFail($id);
        $supervisors = User::role(['supervisor'])->get();
        $presentations = Presentation::where('proposal_id', $proposal->id)->get();    
        $reports = Report::where('proposal_id', $proposal->id)->get();
        session(['title'=>$proposal->title]);
        return view('proposals.show')->with(['proposal'=>$proposal, 'statuses'=>$statuses, 'supervisors'=>$supervisors,
                                             'presentations' => $presentations, 'reports'=>$reports]);
    }

    public function change_status(Request $request)
    {
        $proposal = Proposal::findOrFail($request->id);
        $old_status = $proposal->status->name;
        $new_status = ProposalStatus::where('id', $request->status_id)->first()->name;

        // Change proposal status
        $proposal->proposal_status_id = $request->status_id;


        // Create notice object
        $notitification = 'Status change from ' . $old_status . ' to ' . $new_status;
        $notice = new Notice;
        $notice->type = 'status';
        $notice->proposal_id = $proposal->id;
        $notice->user_id = $proposal->std_id1;
        $notice->user2_id = $proposal->std_id2;
        $notice->notification = $notitification;
        $notice->comment = $request->comment;
        $notice->date = $this->getDateTime();


        $to_name = $proposal->name1; // $user->name;
        $to_email = $proposal->email1; // $user->email;
        $data = array('name'=> $to_name, "proposal" => $proposal, "old_status" => $old_status, "new_status" => $new_status, "from_name" => Auth::user()->fullnames());

//        Mail::send('emails.status_change', $data, function($message) use ($to_name, $to_email) {
//            $message->to($to_email, $to_name)
//                    ->subject('Project Status');
//            $message->from('fypsystem20@gmail.com','ARMS Project');
//        });

        if($proposal->name2 != ""){
            $to2_name = $proposal->name2; // $user->name;
            $to2_email = $proposal->email2; // $user->email;
            $data = array('name'=> $to2_name, "proposal" => $proposal,"old_status" => $old_status, "new_status" => $new_status, "from_name" => Auth::user()->fullnames());

//            Mail::send('emails.status_change', $data, function($message) use ($to2_name, $to2_email) {
//                $message->to($to2_email, $to2_name)
//                        ->subject('Project Status');
//                $message->from('fypsystem20@gmail.com','ARMS Project');
//            });
        }

        $notice->save();
        $proposal->save();

        // if($proposal->status == 'approved'){
        //     $user1 = User::find($proposal->std_id1);
        //     $user1->proposal_id = $proposal->id;

        //     if(!$proposal->is_individual_project){
        //         $user2 = User::find($proposal->std_id2);
        //         $user2->proposal_id = $proposal->id;
        //         $user2->partner_id = $user1->id;
        //         $user2->save()

        //         $user1->partner_id = $user2->id;
        //         $user1->save();
        //     }else{

        //         $user1->partner_id = null;
        //         $user1->save();
        //     }
        // }


        Session::flash('message', 'Status has been changed');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('show_proposal', ['programme_id'=>$proposal->programme_id, 'id'=>$request->id]);
    }

    public function set_supervisors(Request $request)
    {
        $proposal = Proposal::find($request->proposal_id);
        $supervisor = User::find($request->supervisor_id);
        $proposal->supervisor_id = $supervisor->id;
        $proposal->supervisor_name = $supervisor->staff_bio->title .". " . $supervisor->fullnames();
        $proposal->supervisor_email = $request->supervisor_email;
        if($request->cosupervisor_id){
            $cosupervisor = User::find($request->cosupervisor_id);
            $proposal->cosupervisor_id = $cosupervisor->id;
            $proposal->cosupervisor_name = $cosupervisor->staff_bio->title .". " . $cosupervisor->fullnames();
            $proposal->cosupervisor_email = $request->cosupervisor_email;
        }

        // Attach supervsiors to students as well
        if($proposal->std_id1){
            $user = User::findOrFail($proposal->std_id1);
            $user->supervisor_id = $proposal->supervisor_id;
            $user->cosupervisor_id = $proposal->cosupervisor_id;
            $user->save();
        }
        if($proposal->std_id2){
            $user = User::findOrFail($proposal->std_id2);
            $user->supervisor_id = $proposal->supervisor_id;
            $user->cosupervisor_id = $proposal->cosupervisor_id;
            $user->save();
        }
        $to_name = $proposal->name1; // $user->name;
        $to_email = $proposal->email1; // $user->email;
        $data = array('name'=> $to_name, "proposal" => $proposal, "from_name" => Auth::user()->fullnames());

//        Mail::send('emails.assign_supervisors', $data, function($message) use ($to_name, $to_email) {
//            $message->to($to_email, $to_name)
//                    ->subject('Project Supervisors');
//            $message->from('fypsystem20@gmail.com','ARMS Project');
//        });

        if($proposal->name2 != ""){
            $to2_name = $proposal->name2; // $user->name;
            $to2_email = $proposal->email2; // $user->email;
            $data = array('name'=> $to2_name, "proposal" => $proposal, "from_name" => Auth::user()->fullnames());

//            Mail::send('emails.assign_supervisors', $data, function($message) use ($to2_name, $to2_email) {
//                $message->to($to2_email, $to2_name)
//                        ->subject('Project Supervisors');
//                $message->from('fypsystem20@gmail.com','ARMS Project');
//            });
        }

        $proposal->save();
        Session::flash('message', 'Supervisors have been assigned to this proposal');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('show_proposal', ['programme_id'=>$proposal->programme_id, 'id'=>$request->proposal_id]);
    }

     public function fileImportExcel()
    {
        Session(['title'=>'Import Proposals']);
        return view('proposals.import');
    }

    public function fileImport(Request $request)
    {
        Excel::import(new UsersImport, $request->file('file')->store('temp'));
        Session::flash('message', 'Proposals have been added');
        Session::flash('alert-class', 'alert-success');
        return redirect('supervisors');
    }

    public function get_proposal_ajax($id){
        return response()->json(Proposal::findOrFail($id));
    }

    public function move_to_trash($id){
        $proposal = Proposal::findOrFail($id);
        $proposal->active = 0;
        $proposal->save();

        return redirect()->route('dashboard');
    }
}
