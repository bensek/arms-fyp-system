<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\StaffBio;
use Spatie\Permission\Models\Role;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use Auth;
use Session;
use Notification;
use App\Notifications\ProposalNotification;
use Str;
use App\UserVerify;
use Mail;
use App\Mail\EmailVerification;

class SupervisorController extends Controller
{
    public function index()
    {
    	session(['title'=>'Supervisors']);
    	// $users = User::role(['supervisor','coordinator'])->get();
        $user = Auth::user();
        if($user->hasRole('admin')){
            session(['title'=>'Coordinators']);
            $users = User::role(['coordinator'])->get();
        }
         if($user->hasRole('coordinator')){
            session(['title'=>'Supervisors']);
            $users = User::role(['supervisor'])->get();
        }
    	return view('supervisors.index')->with(['users' => $users]);
    }

    public function approve()
    {
        session(['title'=>'Approve Accounts']);
     
        $users = User::where('verified', 0)->get();
        return view('supervisors.approve')->with(['users' => $users]);
    }

    public function approve_supervisor($id){
        $supervisor = User::find($id);
        $supervisor->verified = 1;
        $supervisor->save();

        // Send Notification
         $details = [
            'subject' => 'Account Approved',
            'greeting' => 'Dear ' . $supervisor->fname . ',',
            'body' => 'Your account has been approved. You now have full access to the Fourth Year Projects system.',
            'actionText' => 'Login',
            'actionURL' => route('login'), 
        ];

        //Notification::send($supervisor, new ProposalNotification($details));

        Session::flash('message', 'User has been approved');
        Session::flash('alert-class', 'alert-success');

        return redirect()->back();
    }


    public function new(){
         $user = Auth::user();
        if($user->hasRole('admin')){
            session(['title'=>'New Coordinator']);
        }
        if($user->hasRole('coordinator')){
            session(['title'=>'New Supervisor']);
        }

        $user_object = new User();

        return view('supervisors.new')->with(['departments'=> \App\Department::get(), 'user' => $user_object]);
    }

    public function show($id)
    {
    	$user = User::find($id)->first();
    	session(['title'=>$user->fname . " " . $user->lname]);
    	return view('supervisors.show')->with(['user' => $user]);
    }

    public function store(Request $request){
        $request->validate([
            'department' => 'required|integer',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'title' => 'required|string|max:255',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = new User;
        $user->fname = $request->first_name;
        $user->lname = $request->last_name;
        $user->other_name = $request->other_name;
        $user->email = $request->email;
        $user->department_id = $request->department;
        $user->password = Hash::make($request->password);
        $user->verified = 1;
        $user->save();

        // Save the academic information of supervisor

        $staff = new StaffBio;
        $staff->user_id = $user->id;
        $staff->title = $request->title;
        $staff->office = $request->office;
        $staff->save();

        // Send notification to verify email
        $token = Str::random(64);

        UserVerify::create([
              'user_id' => $user->id, 
              'token' => $token
        ]);
  
        $details = [
            'token' => $token, 
        ];

        //Mail::to($user->email)->send(new EmailVerification($details));

        //$user->sendEmailVerificationNotification();

        if(Auth::user()->hasRole('coordinator')){
            $role = Role::where('name', 'supervisor')->firstOrFail();
            $user->assignRole($role);
            $user->save();
            return redirect('/supervisors');
        }else{
            $role = Role::where('name', 'coordinator')->firstOrFail();
            $user->assignRole($role);
            $user->save();
            return redirect()->route('list_coordinators');
        }
    }

    public function edit($id)
    {
        session(['title' => 'Edit Supervisor']);
        $user = User::find($id);

        return view('supervisors.edit')->with(['departments'=> \App\Department::get(), 'user' => $user]);
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);

        if($request->password){
            $request->validate([
                'password' => 'min:8|confirmed',
            ]);

            $user->password = Hash::make($request->password);
        }


        $user->fname = $request->first_name;
        $user->lname = $request->last_name;
        $user->other_name = $request->other_name;
        $user->email = $request->email;
        $user->department_id = $request->department;
        $user->save();

        // Save the academic information of supervisor

        $staff = StaffBio::where('user_id', $user->id)->first();
        $staff->user_id = $user->id;
        $staff->title = $request->title;
        $staff->office = $request->office;
        $staff->save();

        if(Auth::user()->hasRole('coordinator')){

            return redirect('/supervisors');
        }else{

            return redirect()->route('list_coordinators');
        }

    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
    }

    public function fileImportExcel()
    {
        Session(['title'=>'Import Supervisors']);
        return view('supervisors.import');
    }

    public function fileImport(Request $request)
    {
        Excel::import(new UsersImport, $request->file('file')->store('temp'));
        Session::flash('message', 'Supervisors have been added');
        Session::flash('alert-class', 'alert-success');
        return redirect('supervisors');
    }
}
