<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Presentation;
use App\AcademicYear;

class PresentationController extends Controller
{
    public function index($type, $year_id){
        if($type == 'mid_term'){
            Session(['title'=> 'Mid Term Presentations']);
        }
        if($type == 'final'){
            Session(['title'=> 'Final Presentations']);
        }
    	$user = Auth::user();
        $year = AcademicYear::find($year_id);
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();

        if($user->hasRole('supervisor')){
        	$presentations = Presentation::where('supervisor_id', $user->id)->where('type', $type)->where('academic_year_id', $year_id)->get();
        }else{
            $presentations = Presentation::where('department_id', $user->department_id)->where('type', $type)->where('academic_year_id', $year_id)->get();
        }

    	return view('presentations.index')->with(['presentations'=>$presentations, 'type'=> $type, 'current_year' => $year, 'years' => $years]);
    }
}
