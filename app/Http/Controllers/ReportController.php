<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Report;
use App\AcademicYear;

class ReportController extends Controller
{
    public function index($type, $year_id){
        if($type == 'project_proposal'){
            Session(['title'=> 'Project Proposal Reports']);
        }
        if($type == 'project_report'){
            Session(['title'=> 'Final Project Reports']);
        }
    	$user = Auth::user();
        $year = AcademicYear::find($year_id);
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();

        if($user->hasRole('supervisor')){
            $reports = Report::where('supervisor_id', $user->id)->where('type', $type)->where('academic_year_id', $year_id)->get();
        }else{
            $reports = Report::where('department_id', $user->department_id)->where('type', $type)->where('academic_year_id', $year_id)->get();
        }

    	return view('reports.index')->with(['reports'=>$reports, 'type' => $type, 'current_year' => $year, 'years' => $years]);
    }

    public function index_final($year_id){
    	Session(['title'=> 'Final Project Reports']);
    	$user = Auth::user();
        $year = AcademicYear::find($year_id);
        $years = AcademicYear::where('department_id', Auth::user()->department_id)->orderBy('name', 'desc' )->get();

        if($user->hasRole('supervisor')){
            $reports = Report::where('supervisor_id', $user->id)->where('type', 'project_report')->where('academic_year_id', $year_id)->get();
        }else{
            $reports = Report::where('department_id', $user->department_id)->where('type', 'project_report')->where('academic_year_id', $year_id)->get();
        }
        
    	return view('reports.index')->with(['reports'=>$reports, 'current_year' => $year, 'years' => $years]);
    }
}
