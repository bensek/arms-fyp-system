<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserManualController extends Controller
{

	public function index(){


		Session(['title' => 'User Manual']);

		return view('user_manual.main');
	}

	public function student(){


		Session(['title' => "Student's Manual"]);

		return view('user_manual.student');
	}

	public function lecturer(){


		Session(['title' => "Lecturer's Manual"]);

		return view('user_manual.lecturer');
	}

	public function coordinator(){


		Session(['title' => "Coordinator's Manual"]);

		return view('user_manual.lecturer');
	}
}
