<?php
namespace App\Http\Traits;

use Carbon\Carbon;

trait DateTrait {

	public function getDateTime()
	{
	
		$date = Carbon::now('Africa/Kampala');
		$day = $date->locale('en')->isoFormat('dddd Do MMMM YYYY');
    	$time = $date->format('H:i');

    	return $day . ', ' . $time;
   	}

   	public function formattedDate($string)
   	{
   		$date = Carbon::createFromFormat('Y-m-d', $string);
   		$output = $date->locale('en')->isoFormat('dddd Do MMMM YYYY');
   		return $output;
   	}
}