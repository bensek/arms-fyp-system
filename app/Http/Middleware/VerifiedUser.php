<?php

namespace App\Http\Middleware;

use Closure;

class VerifiedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->verified == 1){
            return $next($request);
        }
        return redirect()->route('requires_approval');
    }
}
