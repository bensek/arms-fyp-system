<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'other_name', 'email', 'password', 'is_email_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function fullnames()
    {
        return $this->lname . " " . $this->fname . " " . $this->other_name;
    }

    public function bio_data()
    {
        return $this->hasOne('App\BioData');
    }

    public function staff_bio()
    {
        return $this->hasOne('App\StaffBio');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    // public function staffnames()
    // {
    //     return $this->staff_bio->title . ". " . $this->lname . " " . $this->fname . " " . $this->other_name;
    // }

    public function panels()
    {
       return $this->belongsToMany(
            User::class,
            'panels_lecturers',
            'user_id',
            'panel_id');
    }

    public function panels_lecturers(){
        return $this->hasMany('App\PanelLecturer');
    }

    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }

    public function presentation1(){
        return $this->belongsTo(Presentation::class, 'presentation1_id');
    }

    public function presentation2(){
        return $this->belongsTo(Presentation::class, 'presentation2_id');
    }



}
